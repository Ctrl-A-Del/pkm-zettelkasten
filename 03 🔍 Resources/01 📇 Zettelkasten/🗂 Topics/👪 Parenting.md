tags: #index-notes

> This topic is about raising children, how they behave and develop and everything I need to know about it


- [[Constraints in attachment parenting]]
- [[The best parents I know make thirty mistakes each day]]
- [[Red yellow and green - recognizing when you are stressed out and need a break]]
- [[We don't need to be perfect parents]]
- [[What a child wants is something different than what of needs]]
- [[Media consumption isn't bad for children]]
- [[Always consider what your child is capable of]]
- [[Comforting a child]]


