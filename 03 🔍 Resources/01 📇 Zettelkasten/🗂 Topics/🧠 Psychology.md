---
tags: index-notes
---

Behavior science and how people think. Psychology has a huge impact on how we act and into what common traps we fall.

---

- [[Risk aversion - if we don't get a big advantage by taking a risk, we avoid it]]
- [[Constrains are a good thing]]
- [[Mental subtraction can help your practicing gratitude]]
- [[I am wrong much more often than I am right - and why this is a good thing]]
- [[You need to gain twice as much as you gain]]
- [[Brandolini's law]]
- [[Parkinsons Law]]
- [[Willpower is depletable]]