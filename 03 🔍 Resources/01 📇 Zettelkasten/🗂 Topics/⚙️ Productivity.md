---
tags: index-notes
---

Everything related to self-improvement and getting better at the stuff you do.

- [[Pareto principle - the 80-20 rule]]
- [[The Habit Loop]]
- [[Getting better each day]]
- [[Two Minute Rule]]
- [[Deliberate Practice is needed to achieve expertise]]
- [[Don't follow your passion - why working right is more important than finding the right work]]
- [[Make bad habits impossible]]
- [[Parkinsons Law]]
- [[Pomodoro vs Deep Work]]
- [[The goal of Pomodoro]]
- 