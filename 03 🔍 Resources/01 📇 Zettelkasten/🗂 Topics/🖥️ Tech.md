---
tags: index-notes
---

 Best practices and ideas for coding, DevOps and other things

---

- [[Avoid let variable depending on condition]]
- [[Creating a SSL Certificate for Gandi with Nginx Proxy Manager]]
- [[Hosting your own image gallery]]
- [[Static Website]]
- [[Dynamic Website]]
- [[Single Page Application]]
- [[Continuous Deployment]]
- [[Continuous Integration]]
- [[Don't panic, if your service breaks over night]]