tags: #index-notes

Everything that you need to now about React.

- [[Pros and cons of React]]
- [[React state does not update immediately - use useEffect to work with the new value]]
- [[Using styled-components to create scoped styling]]
- [[SOLID Clean code in React]]
- [[Lifting the state up]]


