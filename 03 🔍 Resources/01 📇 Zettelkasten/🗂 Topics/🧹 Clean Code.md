tags: #index-notes

Everything that you need to now about Clean Code.

- [[Clean code - write your code like an author writes a story]]
- [[Names in your code should be meaningful]]
- [[Embrace refactoring - Clean code takes time]]
- [[Functions should be small and do one thing - they should have a single responsibility]]


