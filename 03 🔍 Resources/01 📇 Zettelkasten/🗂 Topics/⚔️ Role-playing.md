Everything related to role playing games and managing your game.

- [[Players want their characters to do awesome things]]
- [[Prepare to throw your work away]]
- [[Keep your scene descriptions short]]
- [[Fronts, Grim Portents and Impending Dooms]]
- [[Secrets and clues are the glue of your game]]
- [[You only have control over a game at the beginning of a session]]