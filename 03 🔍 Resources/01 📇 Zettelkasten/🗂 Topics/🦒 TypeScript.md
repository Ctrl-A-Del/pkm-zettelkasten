tags: #index-notes

Everything that you need to now about TypeScript.

- [[What is TypeScript]]
- [[Interfaces in TypeScript]]
- [[TypeScript uses a structural type system aka duck typing]]
- [[Generic type parameters make your components reusable over a variety of types]]
- [[Defining intersection types]]
- [[Common types in TypeScript]]


