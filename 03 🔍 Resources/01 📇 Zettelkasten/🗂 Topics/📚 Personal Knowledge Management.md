tags: #index-notes

### Key Topics
- [[Capture Everything in an Inbox]]
- [[The problem of starting with a blank page]]
- [[Mind like water]]
- [[Understanding what you read]]
- [[What is a second brain]]
- [[What is a second brain]]
- [[What types of notes belong to a Zettelkasten]]
- [[Writing every idea and every task down helps to declutter you mind]]

---

How to build a second brain, take notes and not forget everything.
