---
sr-due: 2023-08-19
sr-interval: 39
sr-ease: 250
date created: Monday, July 10th 2023, 8:40:15 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[🦒 TypeScript]]
> links:  [[Basic syntax to define a variable with a type in TypeScript]]
> source: TypeScript course by the native web
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

## Basic Types
Common types in TypeScript are
- number
- boolean
- string
- void 
- null 
- undefined

## Arrays
You can define an array like this:

```ts
let primes: number[];
primes = [2, 3, 5];
```
or like this:
```ts 
let primes: Array%3Cnumber%3E;
```

But the latter has problems with JSX/TSX in React. So use the first one.

## Enums
Enums can be used to give a selection of values.
```ts
enum Color {
	Red,
	Green,
	Blue
}
let color: Color = Color.Green;
```

## Unknown
This is a wildcard type that works similar to normal JavaScript, but it still has type safety, so it tries to defer the current type. This can result in not allowing you to call a function that you know is there because TypeScript doesn't know.

```ts
let something: unkonwn;
something = 23;      //this 
something = 'Hello'; // is 
something = true;    // valid
```


## Any
Any behaves in the same way as `unkown`, but without type safety. So, you can call anything and do anything, like in normal JavaScript.
```ts
let something: any;
something = 23;      //this 
something = 'Hello'; // is 
something = true;    // valid
```>)