---
date created: Monday, July 10th 2023, 8:39:27 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
tags: [permanent-note, published]
sr-due: 2023-10-09
sr-interval: 82
sr-ease: 291
---
| Topic                                | Source |
| ------------------------------------ | ------ |
| [[📚 Personal Knowledge Management]] |  [How To Take Smart Notes (3 methods no one's talking about) - YouTube](https://www.youtube.com/watch?v=5O46Rqh5zHE)      |

---

> The Q E C framework can give you an idea how to extract information.

When writing smart notes, and you struggle to find a structure or don't know what to write down, use the Q/E/C framework.

Question
Evidence
Conclusion

Reduce the information given to you into questions and conclusions and give evidence for your conclusion.

- - -
## Links
1. [[The problem of starting with a blank page]]
2. [[What types of notes belong to a Zettelkasten]]
