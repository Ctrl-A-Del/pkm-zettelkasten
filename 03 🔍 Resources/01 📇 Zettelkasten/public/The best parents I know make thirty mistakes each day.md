---
date created: Monday, July 10th 2023, 8:41:04 am
date modified: Friday, July 28th 2023, 9:27:33 am
tags: [permanent-note, published]
sr-due: 2023-09-01
sr-interval: 35
sr-ease: 303
---
| Topic | Source |
| ----- | ------ |
| [[👪 Parenting]]      |  [Elternschaft: Anforderungen vs. Realität. Scheitern an den eigenen Ansprüchen - Das gewünschteste Wunschkind](https://pca.st/episode/dbaaa307-6ce1-4402-afd9-e08e51121817)      |

---

> The best parents I know make thirty mistakes each day.  
> --- Jesper Juul

Parenting is difficult. You can't make it perfect. Nobody does. But most people don't talk about that because they don't want to be considered a failure. But it is absolutely normal to make mistakes - in life and as a parent. Don't beat yourself up about at and try to do your best.

- - -
## Links
1. [[Countering rage with a physical sensation]]
2. [[Failure leads to success]]
3. [[Going into any direction is better than standing still]]
4. [[Failure-success paradox]]
