---
date created: Monday, July 10th 2023, 8:40:01 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---
> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  
> - [[Capture Everything in an Inbox]] 
> - [[Five steps to deal with work]]
>
> source: [[Getting things done]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> To clarify what to do, first decide if the item is actionable. If it is, what is the next action? If it is not, trash it, incubate it, or file it.

This is the next step after you have [[Capture Everything in an Inbox|captured all things in your inbox]]. 

You now want to clarify, what to do about each item. 

There are three important rules when processing your inbox:
1. only process one item at a time 
2. nothing goes back into the inbox 
3. process the top item first 

For each item, you have first to ask yourself the following question:

## Is it Actionable?

### No
 This means it is only information for you. One of the following steps should be done:
 1. **Trash it**
 If the information is not relevant enough, don't let it clutter your space. Throw it away.
 
2. **Incubate it**
Maybe there is something to do with the information, but not yet. In this case, create a reminder to come back to that topic later.

3. **File it**
If the item only has informational value, file it in your reference system, either digitally or physically 

### Yes
**What is the next action?**
This means the next physical action. Formulate the step as clear as possible. Avoid abstract actions like *schedule meeting*. Who is part of the meeting? How do you invite everyone? Formulate it out so that it is easy to do it when you want to tackle the task and don't need to overthink it then.


