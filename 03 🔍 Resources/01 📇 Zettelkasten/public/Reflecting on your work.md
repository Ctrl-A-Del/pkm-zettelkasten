---
sr-due: 2023-08-29
sr-interval: 49
sr-ease: 270
date created: Monday, July 10th 2023, 8:40:30 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  [[Five steps to deal with work]]
> source: [[Getting things done]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Reflection means to review your current lists regularly and check if everything is still in line. 

Reflection is an important step to stay productive. From time to time, you need to take a look at all your lists and see which one you forgot to tick off, which one you forgot completely about and which one you specifically avoided.

To give your reflection some structure, the **weekly review** is a great habit to form.

## Weekly Review
Either at the end of the week or at the beginning of the week, you should take some time (roughly two hours) to reflect on all your tasks, projects and lists.

Your weekly review should consist of these three steps:

### 1. Get Clear
- review all inputs, notes, emails etc. 
- collect loose documents and put them in your inbox 
- process your inbox 

### 2. Get Current
- review next action lists 
- review calendar events 
- review waiting for list, did something happen, do you need to remember someone? 
- review project lists and status of your projects 

### 3. Get Creative
- review some-day/maybe list 


## Bigger Picture Reviews
You should also review your long-term goals and general goals in life every once in a wile.  