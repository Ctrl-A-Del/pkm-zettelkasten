---
sr-due: 2023-10-13
sr-interval: 93
sr-ease: 310
date created: Monday, July 10th 2023, 8:40:51 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[👪 Parenting]]
> links:  [[Don't compare yourself to others]]
> source: [[Liebevolle und verantwortungsvolle Beikosteinführung und -begleitung – So geht es!]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> You should start supplementing food for your baby whenever it shows interest. There is no specific time.

Generally, you should start supplementing meals for your baby whenever it is ready. This means, whenever your child shows interest in food. You should never force it to eat because it has a specific age. 

The WHO recommends breastfeeding a baby generally at least until it is six months old. But even if they start eating meals, they still recommend continuing breastfeeding alongside until the child is 2 years old or doesn't want to drink milk anymore. **But even if it is not the recommendation of the WHO, breastfeeding is perfectly fine longer, if the mother and the child both want to**.

When starting to feed the baby, patience is the most important thing. The child should have time to explore the food. Yes, this also means touching it.

If the child is satisfied, you should stop and not force it to eat more, even if there is still food on the plate. This pressure doesn't help. 

If the child doesn't like a dish, you should try it again occasionally and maybe serve it in a different from (mashed potatoes etc.).
