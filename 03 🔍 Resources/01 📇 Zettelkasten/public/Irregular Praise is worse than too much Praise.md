---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
---

| Topic            | Source |
| ---------------- | ------ |
| [[👪 Parenting]] | [[Unconditional Parenting]]        |

---

If you try to love your child unconditionally, you are probably trying to not shower your child with praise because you do not want to only show affection, when it behaves, but in general. But there is something that is worse than showering your child with praise, and that is praising it very rarely. This creates a strong contrast of giving praise and not giving it, making it more difficult for your kid to get it. 

Praise in general communicates, *"I love you when you do XY"*.  The dangerous clue that the child gets is *"I don't love you when you don't do such and such"*. 

This is basically the opposite of what you are trying to do. And since you are doing this very rarely, the child learns that it has to do something very specific *to be loved*. So few praises are basically worse than praising all the time. 

As a result, you have to be cautious when you are trying to avoid praise in general, because if you accidentally praise occasionally, this might backfire.

- - -
## Links
1. [[Positive Reinforcements lowers the intrinsic motivation]]
