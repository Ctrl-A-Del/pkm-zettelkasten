---
sr-due: 2023-08-20
sr-interval: 37
sr-ease: 278
date created: Friday, July 14th 2023, 4:21:45 pm
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[🖥️  Tech]]
> links: 
> - [[Pros and cons of React]]
> - [[Vue]]
> - [[Riot]]
>
> source: 
- [Svelte vs. React vs. Vue | Best SPA Framework for 2022 - YouTube](https://www.youtube.com/watch?v=LSiV8A9Zqzw)
- [GitHub - feltcoop/why-svelte: Why Svelte is our choice for a large web project in 2020](https://github.com/feltcoop/why-svelte)

> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Svelte compiles your code at build time, creating fast native code in a small bundle size.

Svelte is a JavaScript framework to build user interfaces. It is different from other frameworks like React or Vue because it compiles the code at build time rather than at runtime., generating highly optimized code that runs faster and takes up less space.

Svelte works by converting the code written in its own syntax, known as Svelte components, into efficient JavaScript code that manipulates the DOM. It does this during the build process, which eliminates the need for a virtual DOM like in other frameworks.

One of the key advantages of Svelte is its small bundle size. Since it compiles the code upfront, only necessary code is included in the final bundle, resulting in smaller file sizes and faster load times.

Another benefit of Svelte is its simplicity and ease of use. The syntax is straightforward and easy to understand, making it accessible to both beginners and experienced developers.

Sveltes [documentation](https://svelte.dev/docs/introduction) and [tutorials](https://svelte.dev/tutorial/basics) are excellent and approachable.

| Pros                               | Cons                         |
| ---------------------------------- | ---------------------------- |
| high performance, small bundlesize | smaller dev team & community |
| great documentation                |                              |

