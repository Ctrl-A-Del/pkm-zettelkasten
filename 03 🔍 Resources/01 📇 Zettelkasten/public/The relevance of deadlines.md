---
date created: Monday, July 10th 2023, 8:40:23 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---
> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  [[Parkinsons Law]]
> source: [How I Routinely Study With a Full Time Job when I'm TIRED - YouTube](https://www.youtube.com/watch?v=ifZWcPXDyFc)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> If you are struggling to do all your tasks and are prone to procrastination, consider setting strict deadlines.

A deadline is a simple method to force yourself, to do something **right now**. However, for this to work you need to commit to this deadline and take it seriously