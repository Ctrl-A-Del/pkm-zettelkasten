---
sr-due: 2023-09-25
sr-interval: 77
sr-ease: 310
date created: Monday, July 10th 2023, 8:41:36 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🩺 Health]]
> links: 
> source: [[Atmen - heilt - enntspannt - zentriert]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p>

---

> A healthy breath goes through the nose into the belly.

Natural breathing happens though the nose and into your belly. It should happen silently and slow. Between six and twelve breaths per minute are normal. After every exhale, there is a short break before the next inhale comes automatically.
