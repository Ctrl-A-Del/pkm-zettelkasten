---
sr-due: 2023-09-11
sr-interval: 60
sr-ease: 290
date created: Monday, July 10th 2023, 8:39:44 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[👪 Parenting]]
> links:  [[Willpower is depletable]]
> source: [Grenzen setzen in der bedürfnisorientierten Erziehung - Das gewünschteste Wunschkind](https://pca.st/episode/7df530a4-f085-4d3b-ba6c-5596a9708481)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> If your child doesn't cooperate, there is often a reason for that. Consider the situation and what your child can handle at the moment.

When you want something from your child, **always consider if it is capable of doing it**. Is it old enough? Is the time ok (or maybe it's just too late in the day) is your child tired, hungry? Or **did it just use all of its [[Willpower is depletable|willpower]] to cooperate earlier in the day**? 

Thinking about why your child isn't cooperative does not only give you a better feeling (because you see a reason for that behavior, instead of seeing a tyrant), but it also gives you clues about how to handle that situation and your child's [[What a child wants is something different than what of needs|needs]].  