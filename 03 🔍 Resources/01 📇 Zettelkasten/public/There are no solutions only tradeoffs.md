---
date created: Monday, July 10th 2023, 8:41:11 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---
> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  [[Rejection is a Life Skill]]
> source: [[Essentialism - The Disciplined Pursuit of Less]] 
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Doing one thing will mean that you can't do other things.

There is no solutions to all your problems. You will always have to make trade offs. You don't have time for other projects. You have to choose carefully, on what tasks to focus on because it will always mean that other tasks get dropped. Learning to make the right trade offs is an important skill in your life.