---
date created: Monday, July 10th 2023, 8:39:33 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
tags: [permanent-note, published]
sr-due: 2023-07-23
sr-interval: 4
sr-ease: 282
---
| Topic       | Source |
| ----------- | ------ |
| [[🖥️ Tech]] |        |

---

> A dynamic website can update its content based on a server response.

A dynamic website is a mix between a [[Static Website]] and a [[Single Page Application]]. It consists of multiple pages, that can dynamically change its content. A good example are web shops. You have different pages, but the list of products update without reloading the whole site. This is usually done via templates that update after a server response.
On a dynamic website, the server renders the template and some content might dynamically change on the client.

- - -
## Links
1. [[Static Website]]
2. [[Single Page Application]]
