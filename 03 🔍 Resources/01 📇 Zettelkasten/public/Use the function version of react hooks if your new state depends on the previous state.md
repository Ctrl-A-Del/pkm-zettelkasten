---
date created: Monday, July 10th 2023, 8:41:39 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
tags: [permanent-note, published]
sr-due: 2023-09-13
sr-interval: 52
sr-ease: 253
---

| Topic        | Source |
| ------------ | ------ |
| [[🧩 React]] |   [Top 6 React Hook Mistakes Beginners Make - YouTube](https://www.youtube.com/watch?v=GGo3MVBFr1A&t=160s)      |

---

> Using the function version avoids side effects that happen due to React updating the state asynchronous.

If a state variables' value is dependent on the previous value (e.g. `setCount(count + amount))`, always use the function version of the hook.

```js
setCount(currentCount => {
	return currentCount + amount
})
```

React updates the state asynchronous. An addition, it updates the state variable after the next render, so that multiple `useState`-Hooks could be batched together before this re-render.
This can have weird side effects where the state is not updated properly if the previous `useState` is not done yet. Using the function version avoids this issue.

- - -
## Links
1. [[Pros and cons of React]]
