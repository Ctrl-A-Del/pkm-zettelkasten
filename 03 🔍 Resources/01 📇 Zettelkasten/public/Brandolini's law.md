---
date created: Monday, July 10th 2023, 8:41:28 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
tags:
  - permanent-note
  - published
sr-due: 2023-08-05
sr-interval: 4
sr-ease: 274
---
| Topic             | Source |
| ----------------- | ------ |
| [[🧠 Psychology]] | [Brandolini's law - Wikipedia](https://en.m.wikipedia.org/wiki/Brandolini%27s_law)       |

---

> The amount of energy needed to refute bullshit is an order of magnitude larger than is needed to produce it

Making up some statement is effortless, especially if it sounds believable. However, proving that something is wrong is much more difficult. You really have to do the research and find sources. 

- - -
## Links
1. [[Architects of our own believe]]
