---
date created: Monday, July 10th 2023, 8:40:04 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
tags: [permanent-note, published]
sr-due: 2023-08-17
sr-interval: 29
sr-ease: 290
---
| Topic        | Source |
| ------------ | ------ |
| [[🧩 React]] |[React Testing Library and Jest: The Complete Guide | Udemy](https://www.udemy.com/course/react-testing-library-and-jest/learn/lecture/35701634#overview)        |

---


```js
const mock = jest.fn(); // create a mock function that doesn't do anything. You can also handle stuff in here (e.g. event.preventDefault())

render(<UserForm onUserAdd={ mock }) />);
...

expect(mock).toHaveBeenCalled();
expect(mock).toHaveBeenCalledWith({ name: 'jane', email: 'jane@jane.com' });
```

- - -
## Links
1. [[Set up Jest environment]]
