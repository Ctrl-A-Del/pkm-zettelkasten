---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
---
| Topic        | Source |
| ------------ | ------ |
| [[🖥️  Tech]] | [[The Pragmatic Programmer]]        |

---
Inheritance tax is a term that describes the problem of strong coupling introduced by inheritance. There are alternatives that create less coupling. For example, using interfaces and implementing those instead. However, this would mean that we have a lot of code duplication, and cannot keep our code [[Don't repeat yourself - keep your code DRY|dry]]. To get around this, you can use mix-ins. They are a way to put some additional functionality upon another type or class. You basically create a class of the interface and put the functionality of the mix-in on top. This top-to-bottom approach keeps the code much more decoupled compared to the bottom-up approach of inheritance.


- - -
## Links
1. [[Don't repeat yourself - keep your code DRY]]
2. [[Interface Segregation Principle]]
