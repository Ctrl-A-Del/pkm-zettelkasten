---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
---

| Topic        | Source                                                                                                                                            |
| ------------ | ------------------------------------------------------------------------------------------------------------------------------------------------- |
| [[🖥️ Tech]] | [3 Ways to Close and Restart Explorer.exe in Windows 10](https://www.isumsoft.com/windows-tips/3-ways-to-restart-explorer-exe-in-windows-10.html) |

---

 For me, Windowsdows explorer tends to crash freeze from time to so that I need to restart it. 
 
 The easiest way I know of is to do this via the terminal:
```sh
taskkill /f /im explorer.exe 
start explorer.exe
```

Win + R and type `cmd` to open the terminal, if you cannot access start menu or search.