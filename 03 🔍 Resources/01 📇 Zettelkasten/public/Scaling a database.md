---
date created: Wednesday, July 12th 2023, 9:39:03 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
tags: [permanent-note, published]
sr-due: 2023-09-08
sr-interval: 37
sr-ease: 250
---
| Topic       | Source |
| ----------- | ------ |
| [[🖥️ Tech]] |        |

---

> There are two main approaches to scaling a database: optimizing read time through caching common queries and scaling write time through sharding the database vertically or horizontally.

There are two different approaches to scaling a database: Scaling the read time and scaling the write time. Generally speaking, **a relational database is optimizied towards reading a database quicker, while a noSQL (document based) database is optimizied towards writing new data.**


**Scaling the DB read**
Usually the database is a bottleneck for your service. The database reaches its' limits quicker than the server. To counter this, you should cache the data of common queries, so that only the first query is slow. A good example for this might be getting a list of products from your home page.

**Scaling the DB write**
To scale the writing time, you can *shard* the database
- vertically: each table has a different machine, ok if the load on tables is equal, but if you have one huge table, this won't help (EG tweets table for Twitter )
- horizontally: split the table around different machines, to achieve this you need a function to map each ID to a server, e.g. via modulo 

- - -
## Links
1. [[Redis Cache stores data in RAM to avoid accessing the database]]


