---
date created: Monday, July 10th 2023, 8:39:46 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
tags: [permanent-note, published]
sr-due: 2023-07-23
sr-interval: 4
sr-ease: 282
---
| Topic            | Source |
| ---------------- | ------ |
| [[👪 Parenting]] |   https://pca.st/episode/7df530a4-f085-4d3b-ba6c-5596a9708481      |

---

> Just because your child wants something, you shouldn't just give it to it. Consider the need behind that and evaluate the craving properly.

It's important to always consider your child's needs. However, what a child wants may be something entirely different. Considering your child's needs doesn't mean to always do what they want. 
Imagine your child wanting some sweets. But its need might be something different: lunch. So, you don't need to give your child sweets just because it wants some. Instead, think about the need behind it. Give your child something (healthier) to eat. That way, you don't have to fight over the sweets and have an angry, hungry child. It will be much easier to handle if your child gets what it needs. 

- - -
## Links
1. [[Constraints in attachment parenting]]