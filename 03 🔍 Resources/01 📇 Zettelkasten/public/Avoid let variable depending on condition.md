---
sr-due: 2023-07-29
sr-interval: 29
sr-ease: 270
date created: Monday, July 10th 2023, 8:40:30 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
tags:
  - published
  - permanent-note
sr-due: 2023-11-30
sr-interval: 121
sr-ease: 290
---
| Topic             |
| ----------------- |
| [[🧹 Clean Code]] |
| ----------------- |

---

> Having a `let` value is always a sign to change something. Consider using a function that returns the values into a `const`

```javascript
// problem:

let values;

if (a) {
	values = xyz;
}
else {
	values = abc;
}

// solution

const values = rebuildValues();

rebuildValues() {
	if(a) {
		return xyz;
	}
	else {
		return abc;
	}
};
```

- - -
## Links
1. [[Clean code - write your code like an author writes a story]]
