---
date created: Monday, July 10th 2023, 8:39:59 am
date modified: Wednesday, July 19th 2023, 1:12:20 pm
tags: [permanent-note, published]
sr-due: 2023-07-23
sr-interval: 4
sr-ease: 274
---
| Topic             | Source |
| ----------------- | ------ |
| [[🧹 Clean Code]] | [Clean Code - Udemy](https://www.udemy.com/course/writing-clean-code/learn/lecture/23111168#overview)       |

---

Overusing comments can make your code more difficult to read, even though they describe what's going on. Often, the information provided might be redundant and the additional text is slowing you down.

If you are using proper naming, the names should speak for themselves.  
Use comments to give explanations, that can't be illustrated in code, e.g. for regular expressions.  
They can also be helpful to warn other developers or give a special reason, why you did something a specific way.

- - -
## Links
1. [[Clean code - write your code like an author writes a story]]
