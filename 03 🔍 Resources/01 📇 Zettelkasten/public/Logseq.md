---
sr-due: 2023-10-06
sr-interval: 86
sr-ease: 310
date created: Monday, July 10th 2023, 8:40:18 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🖥️ Tech]]
> links:  [[Logseq journaling]] [[Todos in Logseq]]
> source: 
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> [Logseq](https://logseq.com/) is a privacy-first, open-source knowledge base. It advertises itself as a joyful, open-source outliner that works on top of local plain-text Markdown and Org-mode files. Use it to write, organize and share your thoughts, keep your to-do list, and build your own digital garden. 
 
 
Logseq is a note-taking tool very similar to [Obsidian](https://obsidian.md). It can be used to manage several Markdown files to create a complex knowledge database for yourself. It is free and open-source and comes with many features, such as a journal function, to-do management, reference links, theming and many more out of the box. Not only that, but it doesn't need any additional plugins for any of those features.

**The data is saved locally on your device in plain Markdown format**. This gives you full control over the files and writes them in a format that can be used with any other text editor, not limiting you to Logseq. 

I use it together with Obsidian, another outstanding tool. I prefer Logseq for some specific tasks because they are natively integrated. The outlining, to-do and journaling capabilities out of the box make it perfect to handle your daily to-dos and tasks in a very convenient way. Logseq also has the benefit of being completely free, while Obsidian requires a license for commercial use. This means I can use Logseq at work without worrying about a license at all. 