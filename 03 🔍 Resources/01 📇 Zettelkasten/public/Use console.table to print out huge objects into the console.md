---
sr-due: 2023-09-14
sr-interval: 63
sr-ease: 290
date created: Monday, July 10th 2023, 8:39:37 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[🖥️ Tech]]
> links:  [[Use console.dir for logging html elements]]
> source: [Tips and Tricks for Debugging JavaScript - YouTube](https://youtu.be/_QtUGdaCb1c?t=246)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

When you print out huge arrays or objects into the console that are hard to digest, you can use `console.table()` to print out a nicely formatted table of it.