---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
---

| Topic     | Source                                                                                          |
| --------- | ----------------------------------------------------------------------------------------------- |
| [[React]] | [All 29 Next.js Mistakes Beginners Make - YouTube](https://www.youtube.com/watch?v=5QP0mvrJkiY) |

---

> A client component makes all imported modules client components but not server components nested within it.

A client component makes all imported modules also render as client components. 
However, this is not the case for server components, that are wrapped by a client component as children. They stay a server component that is just passed along.

A common example is a context provider, that handles some state and therefore is a client component. But inside this client component, the rest of the application can stay a server compontent.


- - -
## Links
1. [[Make client component explicit instead of implicit in Next.js]]
