---
sr-due: 2023-07-31
sr-interval: 15
sr-ease: 294
date created: Monday, July 10th 2023, 8:40:01 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[🧹 Clean Code]]
> links:  [[Clean code - write your code like an author writes a story]]
> source: [Clean Code | Udemy](https://www.udemy.com/course/writing-clean-code/learn/lecture/23111102#overview)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

Typing might seem like a good thing to write clean code, but it is actually not important. Types can help you to avoid errors and unexpected behavior, but you can use clean code principles with any language, with or without typing.