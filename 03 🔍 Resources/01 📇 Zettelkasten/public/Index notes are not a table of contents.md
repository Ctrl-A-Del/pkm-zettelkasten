---
sr-due: 2023-09-16
sr-interval: 68
sr-ease: 310
date created: Monday, July 10th 2023, 8:41:05 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  [[What types of notes belong to a Zettelkasten]]
> source: [[06 🗃️ Archiv/03 🗄️ References/How to take smart notes]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Your index note should not contain all notes related to a topic. It should only serve as an entry point.

An index note is part of the [[What is a Zettelkasten|Zettelkasten]] principle. They are your entry point to your topics by listing the most important related notes. 

However, they do not list all notes of that topic. The idea is to just show the most important ones and stumble upon the other notes on the go by the links between your permanent notes. 

Index notes are supposed to be an entry point into a topic and not a table of contents.

Your index notes will still grow over time. But they should not contain too many notes. About 25 notes is a good limit. If you exceed this number, you should consider splitting the topic into two categories and split the links as well, creating a more specified range of topics over time.