---
sr-due: 2023-08-29
sr-interval: 54
sr-ease: 294
date created: Monday, July 10th 2023, 8:39:55 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[🧠 Psychology]]
> links:  [[Nobody is special]]
> source: [[The subtle art of not giving a fuck]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Don't compare yourself to others. Find your own measurements to define your success.


We tend to always measure ourselves against other people. Even if we have great success, this is not enough because someone else is still doing better.

This can make us feel like a failure, even though we are doing great.

You always have to consider, that [[Nobody is special|everybody is average]]. If someone has more success in some field, they are probably worse in another skill that you own.

You have to find measurements that make you happy and stop caring about others and missed chances. 

Our metrics define our success. 

This means we can decide how successful we are, not someone else.