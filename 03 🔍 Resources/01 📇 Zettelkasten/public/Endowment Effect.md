---
sr-due: 2023-08-22
sr-interval: 44
sr-ease: 270
date created: Monday, July 10th 2023, 8:41:23 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🧠 Psychology]]
> links:  [[Confirmation Bias - We like ideas that support our point of view]]
> source: [[Essentialism - The Disciplined Pursuit of Less]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> We overvalue things we own.

The endowment effect is a cognitive bias that describes the tendency of individuals to value items they already own more than identical items they do not own. This bias suggests that people assign a higher value to something simply because they possess it, even if the item's objective worth remains the same.
