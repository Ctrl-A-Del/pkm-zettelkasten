---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
---

| Topic                                | Source                                                                                                            |
| ------------------------------------ | ----------------------------------------------------------------------------------------------------------------- |
| [[📚 Personal Knowledge Management]] | [Most Beginner Obsidian Users Make This Mistake... - YouTube](https://www.youtube.com/watch?v=P-_zK_fjwyE&t=125s) |

---

> My notes are my own understanding of something else. They are just a personal interpretation of information. They can be rough, they can be wrong. And most importantly: They can change.



- - -
## Links
2. [[Writing fleeting notes]]
3. [[I am wrong much more often than I am right - and why this is a good thing]]
