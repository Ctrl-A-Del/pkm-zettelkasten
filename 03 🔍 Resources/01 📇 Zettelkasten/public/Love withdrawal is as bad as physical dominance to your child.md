---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
---
| Topic            | Source |
| ---------------- | ------ |
| [[👪 Parenting]] | [[Unconditional Parenting]]        |

---

Timeouts are a common punishment for parents. This can go from the common "go to your room", to ignoring the child and all other forms of stopping interaction with your kid. All of those punishments are different kinds of love withdrawal.

The problem is, that love withdrawal is as bad as physical dominance, or even worse. It triggers the primal fear in the child of not being cared for by their parents anymore. 

It means total isolation. And humans are not only social creatures, but children also depend on a caretaker.  Not being cared for is basically equivalent to death for their primal instincts. 

So while hitting your child leaves physical wounds, love withdrawal is an emotional wound. Especially if timeouts last longer, they can therefore be seen as worse than hitting your child. 

I don't propagate you should hit your child! I am just drawing a comparison to make you aware that timeouts are a horrible practice, even though they are socially more accepted than spanking.

Making your child not feel loved can have a devastating impact on their self-esteem for the rest of their lives. 

- - -
## Links
1. [[The best parents I know make thirty mistakes each day]]
2. [[Constraints in attachment parenting]]
3. [[You don't want your child to be well-behaved]]
