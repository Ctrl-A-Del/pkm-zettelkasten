---
sr-due: 2023-07-26
sr-interval: 13
sr-ease: 281
date created: Thursday, July 13th 2023, 10:23:41 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🧠 Psychology]]
> links:  [[You need to gain twice as much as you gain]]
> source: [[Thinking fast and slow]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> If we don't expect a significant advantage by taking a risk, we don't like taking risks. We tend to prefer a situation with a certain, but guaranteed payoff to a risky situation with a higher payoff. 

Think about the following situation:
You have the choice between these to options:

1. play a game that gives you a 50% chance to win 85€ or get nothing at all
2. getting 40€ straight away

Most people take the 40€. 

However, the expected outcome of game 1 is 42,5€ (85\*0,5), while the second option only has an outcome of 40. So, **even if the expected outcome is higher, the risky version doesn't yield a significant advantage**. If this difference is not significantly higher, the safe version seems like a much better choice to most people. 

But there are scenarios where the risky option can be more attractive, depending on your reference point: if you have the chance of either getting a save gain or loose and gain, you are likely to go with the save option. However, if you have the option to either lose a bit, or take a chance to lose more or gain something, you might probably take your chances to at least try to avoid loss. 