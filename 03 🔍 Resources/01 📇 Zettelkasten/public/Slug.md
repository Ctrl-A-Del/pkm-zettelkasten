---
date created: Thursday, March 16th 2023, 9:32:58 am
date modified: Sunday, July 16th 2023, 9:12:46 pm
tags: [permanent-note, published]
sr-due: 2023-07-23
sr-interval: 4
sr-ease: 270
---

| Topic       | Source |
| ----------- | ------ |
| [[🖥️ Tech]] |  [Was bedeutet Slug? - Ryte Digital Marketing Wiki](https://de.ryte.com/wiki/Slug)      |

---

> The term slug refers to parts of a URL that consist of one or more words and can be read by search engines as well as understood by users

The term slug refers to parts of a URL that consist of one or more words and can be read by search engines as well as understood by users. Typically, slugs are located at the end of a URL and refer uniquely to a digital resource by referencing it with a specific URL structure.

- - -
## Links
1. [[GraphQL]]
2. [[Gatsby.js]]
