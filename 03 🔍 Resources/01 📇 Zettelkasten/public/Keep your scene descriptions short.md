---
sr-due: 2023-09-14
sr-interval: 64
sr-ease: 327
date created: Monday, July 10th 2023, 8:41:02 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[⚔️ Role-playing]]
> links:  
> - [[You only have control over a game at the beginning of a session]]
> - [[Prepare to throw your work away]]
>
> source: [[Return of the Lazy Dungeon Master]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Only prepare as much, as you need to feel prepared. Keep your scene descriptions short to adapt them quickly and to be okay to [[Prepare to throw your work away|throw them away]].

Since you never know what your players are going to do, scripting out the adventure won't work. Instead, **you can outline a couple of potential scenes that you think might occur**. Only write about a sentence for a scene, to easily adapt it if needed. This also helps you to not be too disappointed, if this scene won't occur. 
The main goal of those notes is to give you the feeling of knowing what will happen and being prepared for it, not to have a script for each possible scene.

