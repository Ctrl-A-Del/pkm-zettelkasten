---
sr-due: 2023-08-16
sr-interval: 35
sr-ease: 250
date created: Monday, July 10th 2023, 8:40:02 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[🦒 TypeScript]]
> links:  
> - [[Interfaces in TypeScript]]
> - [[TypeScript uses a structural type system aka duck typing]]
>
> source: TypeScript course by the native web
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

In TypeScript you can define interfaces for more than just objects. You can also define interfaces for functions. You can even add properties to those functions.

```ts
interface Add {
  (left: number, right: number): number;
  description: string;
}

const add: Add = function (left: number, right: number):  number {
  return left + right;
};

add.description = 'Calculates the sum of two numbers.';
```