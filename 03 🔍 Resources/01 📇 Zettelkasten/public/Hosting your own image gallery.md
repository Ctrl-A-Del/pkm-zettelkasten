---
date created: Monday, July 10th 2023, 8:41:31 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---
> [!INFO]- 
> topic: [[🖥️ Tech]]
> links:  [[Installing PhotoPrism on your Pi]]
> source: https://photoprism.app/
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

![[Screenshot 2021-06-08 at 10.42.12.png]]
> PhotoPrism is a software for organizing a photo gallery. Using machine learning, it indexes and structures the pictures into different albums, allows searching etc.

If you value your privacy, you probably don't like uploading your photos to Google. However, having a nice interface to back up and browse all your pictures is hard to come by if you want them to still belong to you. 

[PhotoPrism](https://photoprism.app/) is a nice solution, that you can self-host on a Raspberry Pi. I use it together with my Nextcloud instance and achieve a completely private interface to browse my photos. The experience is very similar to Google Photos, but with a much better feeling that your data still is yours. 

A complete setup of PhotoPrism on a Raspberry Pi, including synchronization with Nextcloud, can be set up in this way:
1. [[Installing PhotoPrism on your Pi]]
2. [[Use an external SSD for your PhotoPrism instance]]
3. [[Sync Nextcloud with Photoprism]]

To make it accessible outside your local network and add HTTPS capabilities, do the following:

1. Do these steps for your Photoprism server:[[Nextcloud Deutsche Glasfaser]]
2. [[Creating a SSL Certificate for Gandi with Nginx Proxy Manager]]
3. [[Install Nginx Proxy Manager on a Pi 4]]
4. [[Add HTTPS proxy for a service via Nginx Proxy Manager]]

Additional Features:
- [Export from Google Photos](https://docs.photoprism.org/user-guide/use-cases/google/)