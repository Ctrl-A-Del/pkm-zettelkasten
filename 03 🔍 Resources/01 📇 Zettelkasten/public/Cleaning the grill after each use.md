---
date created: Monday, July 10th 2023, 8:41:25 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
tags: [permanent-note, published]
sr-due: 2023-08-28
sr-interval: 32
sr-ease: 296
---
| Topic       | Source |
| ----------- | ------ |
| [[🏡 Life]] | [Gasgrill täglich reinigen?! So geht's richtig! 5 Schritte Anleitung - YouTube](https://www.youtube.com/watch?v=-uFmicDz3C8)       |

---

> Clean your grill after each use.

After each use, you should clean your grill.

Start by heating it up to the maximum to clean it via pyrolysis. This burn everything in the grill. This process may take 20 to 30 minutes. Wait, until no more smoke rises.

Next clean the barbecue grate with a brush made from steel or brass (better for cast iron).

Wait, till the cast iron has cooled down to about 200 °C and apply oil to it, for example with baking spray.

Last but not least, clean the grease tray below your grill by scratching away the crust and wash it out. As an alternative use tin foil, to make this easier

- - -
## Links
1. [[Burn in cast iron]]
