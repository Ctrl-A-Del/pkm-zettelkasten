---
date created: Monday, July 10th 2023, 8:40:25 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
tags: [permanent-note, published]
sr-due: 2023-08-12
sr-interval: 16
sr-ease: 274
---
| Topic                                 | Source |
| ------------------------------------- | ------ |
| [[🧠 Psychology]] [[⚙️ Productivity]] | [[The Art of Work]]       |

---

> Instead of taking a leap into something that hopefully is your passion, take small steps into that direction.

Following your calling doesn't mean to take a leap of faith when you think the time is right. This might be how we imagine it, but realistically it takes time to prepare this big step. 
You can't just walk up to a stranger and propose. You have to get to know each other. 

The same is true for your calling. Go small steps and keep a lookout for signs. Always be open to adjust your direction. 

- - -
## Links
1. [[Going into any direction is better than standing still]]
