---
sr-due: 2023-07-25
sr-interval: 11
sr-ease: 273
date created: Wednesday, July 12th 2023, 8:59:20 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
tags:
  - permanent-note
  - published
sr-due: 2023-10-14
sr-interval: 73
sr-ease: 293
---
| Topic             | Source                                  |
| ----------------- | --------------------------------------- |
| [[🧠 Psychology]] | [[The subtle art of not giving a fuck]] |

---

> We are wrong countless times, and we should admit that. Only then will we be able to find the right answer and grow personally.

I am wrong countless times a day. And this is a good thing. In fact, I am wrong much more often than I am right. And I embrace this. It might sound more fun to be right all the time, but let's face it — this is simply not possible. We are wrong numerous times, and this is totally fine.

Only if we accept that we are wrong, can we learn the right answer. 
This is called personal growth.
The goal is not to be right all the time, but to be a little less wrong each day

Let's celebrate the growth that comes from being wrong.

- - -
## Links
1. [[Nobody is special]]
