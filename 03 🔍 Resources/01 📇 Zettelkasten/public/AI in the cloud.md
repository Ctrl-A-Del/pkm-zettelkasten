---
date created: Monday, July 10th 2023, 8:41:04 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
tags: [permanent-note, published]
sr-due: 2023-10-11
sr-interval: 84
sr-ease: 291
---
| Topic       | Source |
| ----------- | ------ |
| [[🖥️ Tech]] |[Artificial Intelligence on the Cloud - Overview of Cloud Computing | Coursera](https://www.coursera.org/learn/introduction-to-cloud/lecture/rCs7K/artificial-intelligence-on-the-cloud)        |

---

> Ai is a common way to process data on the cloud.

AI can be the method to process data on the cloud, e.g. coming from IoT devices. The AI model can make it easier to create insights from the data created.

- - -
## Links
1. [[Basic definition of Cloud Computing]]
2. [[IoT in the cloud]]
