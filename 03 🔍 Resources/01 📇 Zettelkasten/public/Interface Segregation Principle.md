---
date created: Jul. Fr, 2023 - 13:59
date modified: Jul. Fr, 2023 - 14:02
tags:
  - permanent-note
  - published
sr-due: 2023-08-01
sr-interval: 4
sr-ease: 270
---
| Topic             | Source |
| ----------------- | ------ |
| [[🧹 Clean Code]] |  [Clean Code - Udemy](https://www.udemy.com/course/writing-clean-code/learn/lecture/23111364#overview)      |

---

> Many client-specific interfaces are better than one general purpose interface.

Instead of trying to create on interface that fits all purposes, consider creating several smaller interfaces, that fit each use case. This gives you much more flexibility which functionality you want to implement.

Big general purpose interfaces can lead you into the trap that your specific class needs to implement a function, that it actually doesn't need.

```js
interface Database {
  storeData(data: any);
}

interface RemoteDatabase {
  connect(uri: string);
}

class SQLDatabase implements Database, RemoteDatabase {
  connect(uri: string) {
    // connecting...
  }

  storeData(data: any) {
    // Storing data...
  }
}

class InMemoryDatabase implements Database {
  storeData(data: any) {
    // Storing data...
  }
}

```

- - -
## Links
1. [[Liskov Substitution Principle]]