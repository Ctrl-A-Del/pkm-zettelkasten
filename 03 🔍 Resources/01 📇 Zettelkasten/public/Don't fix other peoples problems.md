---
date created: Monday, July 10th 2023, 8:39:38 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
tags: [permanent-note, published]
sr-due: 2023-08-14
sr-interval: 17
sr-ease: 270
---
| Topic             | Source |
| ----------------- | ------ |
| [[🧠 Psychology]] | [[The subtle art of not giving a fuck]]       |

---

> It is not your job, to fix other people's problems. The only thing you can do, is support them.

While you have to take [[Responsibility of choice|responsibility]] for the actions you take, you don't have to take responsibility for other people. 

There are boundaries between every healthy relationship. You should not blame your partner for your problems. Neither should you accept the blame from your partner.

It is not your job, to fix other people's problems. The only thing you can do, is support them. But everybody needs to fix it on their own. It is impossible to resolve problems from the outside. That's your own job and your own responsibility.

- - -
## Links
1. [[Responsibility of choice]]
