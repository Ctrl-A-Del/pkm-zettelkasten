---
date created: Monday, July 10th 2023, 8:40:01 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
tags: [permanent-note, published]
sr-due: 2023-09-21
sr-interval: 49
sr-ease: 290
---
| Topic        | Source |
| ------------ | ------ |
| [[🧩 React]] | [React Testing Library and Jest: The Complete Guide | Udemy](https://www.udemy.com/course/react-testing-library-and-jest/learn/lecture/35701744#overview)       |

---

> Module mocking allows you to overwrite modules with dummy mocks.

Module mocking describes the process of overwriting some module, that you would normally import into your test. For example, when a component is making trouble during testing due to weird imports, you can decide to exclude it from the test, if you are certain that it shouldn't affect the testing result.

You can then create a mock version of this module, that is imported instead.

```js
jest.mock('../tree/FileIcon', () => {
	return () => {
		return 'File Icon Component' // this replaces the content from '../tree/FileIcon'
	}
});
```

- - -
## Links
1. [[Module mocking is a bad way to test data fetching]] 
