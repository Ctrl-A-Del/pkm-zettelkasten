---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
---

| Topic        | Source                       |
| ------------ | ---------------------------- |
| [[🖥️ Tech]] | https://voiceinput.futo.org/ |

---

> FUTO is a FOSS and completely offline speech-to-text engine for Android.

FUTO uses a completely offline speech-to-text engine and is surprisingly accurate, especially if you are using the slower but better language models. It supports English and multilingual mode. Of course English is the most accurate one, but the multilingual one also does a fantastic job. 

It works completely on its own, which means it doesn't need any Google services like most speech-to-text tools on Android.

It is free and open source. 