---
sr-due: 2023-08-14
sr-interval: 34
sr-ease: 250
date created: Monday, July 10th 2023, 8:41:32 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  [[Planing fallacy - why we plan too optimistic]]
> source: [[Thinking fast and slow]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Optimistic people tend to overestimate their chances and take more risks. However, the average chance of success often catches up and results in an unexpected failure. A pre mortum can help. 

Optimists have more confidence in their decisions and therefore tend to take more risks. They are overconfident and don't consider the average possibility of failure for their decisions. They think that they have a much bigger influence on their success, than they really have. For example, CEOs consider their influence in the success of the company to be about 80%. However, luck and external factors play a much bigger role. 

As a result, optimistic people tend to do think they can succeed, where other people failed before them. 

However, confidence is much higher valued than a realistic uncertainty. This motivates bold forecasts. And lets optimistic people be considered experts, just because they tend to be confident about their attitude. 

As a result, a lot of these people still fail due to the average chance of success. This mistake is called the optimistic bias. 

A solution to this (and the [[Planing fallacy - why we plan too optimistic|planing falacy]]) is a *pre mortum*: imagine the following future for your decision: you are 1 year into the future, and you followed the plan as it now exists. The outcome was a disaster. Take 5-10 minutes to describe this disaster. This encourages doubt and lets you think about negative effects of your choice. It allows you space and time to take a step back from your optimistic point of view and look at a more pessimistic side, to maybe get a realistic view. 