---
sr-due: 2023-08-26
sr-interval: 46
sr-ease: 292
date created: Monday, July 10th 2023, 8:41:11 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🖥️ Tech]]
> links:  
> - [[Big O Notation]]
> - [[Log N Runtimes]]
>
> source: [[Cracking the Coding Interview]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> When dealing with recursive algorithms, pay close attention to your space and time complexity. It can easily become O(N²).

If you have a recursive algorithm that makes multiple calls, e.g. calculating the [[Fibonacci sequence]] like [[Dynamic PROGRAMMING|here]], the runtime can be expressed as a tree with 2^N - 1 nodes. 

![[Pasted image 20220620221511.png]]

This results in O(2^N). 

However, a straight recursion with only one call, like calculating the factorial, only takes O(n). 