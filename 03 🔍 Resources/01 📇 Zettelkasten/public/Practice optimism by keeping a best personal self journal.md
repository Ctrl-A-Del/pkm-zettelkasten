---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
---
| ----------------- | ------------------------ |
| [[🧠 Psychology]] | [[The How of Happiness]] |

---

> Envision your ideal future occasionally through a "best personal self" diary to clarify goals, prioritize actions, and train your optimism.


Activity:
Cultivate optimism by keeping a "best personal self" diary.

Frequency:
This doesn’t need to be a regular habit—set aside 15 minutes occasionally to envision your ideal future.

Content:

Focus on different time frames (e.g., a few months, 10 years).

Explore various areas of life and define what you want to achieve.


Benefits:

Clarifies if your goals are realistic and makes them more achievable.

Helps prioritize what’s important and create a plan of action.

Trains your optimism, improving your perspective on life.


- - -
## Links
1. [[We can never be happy]]
2. [[Bad measurements of happiness]]
