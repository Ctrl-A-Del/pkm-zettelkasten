---
date created: Monday, July 10th 2023, 8:40:59 am
date modified: Tuesday, July 18th 2023, 11:35:01 am
tags: [permanent-note, published]
sr-due: 2023-10-17
sr-interval: 91
sr-ease: 310
---
| Topic               | Source |
| ------------------- | ------ |
| [[⚔️ Role-playing]] | [[Return of the Lazy Dungeon Master]]       |

---

To make your campaign unique, consider defining some truths of your world. Six is a good number because they are easy enough to write down but still define the world. 

- - -
## Links
1. [[Spiral Campaign Development]]