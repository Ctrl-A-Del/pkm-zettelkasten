---
sr-due: 2023-09-29
sr-interval: 81
sr-ease: 310
date created: Monday, July 10th 2023, 9:57:09 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
tags: [permanent-note, published]
---
| Topic       | Source |
| ----------- | ------ |
| [[🖥️ Tech]] | me       |

---

> This is a guide how to create an HTTPS Proxy via Nginx Proxy Manager

1. Go into the admin panel of your Nginx Proxy Manager (e.g. http://192.168.178.155:81)
2. Go to `Hosts/Proxy Hosts`
3. Add new Proxy
4. Enter the desired domain name (with the subdomain. e.g. `subdomain.domain.com`)
5. Enter the forward IP and the port, leave scheme HTTP, because nginx uses HTTPS
6. Go into the SSL Tab and choose the Certificate (ideally from [[Creating a SSL Certificate for Gandi with Nginx Proxy Manager]])
7. Check `Force SSL`

- - -
## Links
1. [[Creating a SSL Certificate for Gandi with Nginx Proxy Manager]]
2. [[Install Nginx Proxy Manager on a Pi 4]]
