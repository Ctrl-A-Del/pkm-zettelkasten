---
sr-due: 2023-09-27
sr-interval: 79
sr-ease: 290
date created: Monday, July 10th 2023, 8:41:37 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🖥️ Tech]]
> links: 
> - [[Static Website]]
> - [[Dynamic Website]]
> source: 
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> A single page application only has one page, which content updates dynamically.

While classic websites are generated on the server and send to the client in a Request-Response manner, Single Page Applications mimic the user experience of an app. The page never reloads. Everything happens on one page. All content is dynamically loaded and replaced. The page that is visible on the client is usually rendered on the client depending on the server responses. You can even develop a single page application, that handles only local data and works without internet access.