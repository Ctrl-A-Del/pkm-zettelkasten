---
sr-due: 2023-10-04
sr-interval: 84
sr-ease: 310
date created: Monday, July 10th 2023, 8:40:54 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🖥️ Tech]]
> links: 
> source: [New in Obsidian: Obsidian Callouts (available now) | Nicole van der Hoeven](https://nicolevanderhoeven.com/blog/20220330-new-in-obsidian-obsidian-callouts/)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p>

---

If you want an additional infobox in obsidian, you can use the callout syntax:

> [!info] Your title
> 
> > The contents of your note here

## List of All Callouts

- abstract
- summary
- tldr
- info
- todo
- tip
- hint
- important
- success
- check
- done
- questions
- help
- faq
- warning
- caution
- attention
- fail
- failure
- missing
- danger
- error
- bug
- quote
- cite

## Types of Callouts

### Abstract, Summary, Tldr

And then another one is abstract, summary, or TLDR.

Now, all of these are the same, so like this is a TLDR, so I can actually edit it here as well. So here in this case, I was using TLDR, but if I do abstract, it's going to look, actually, it looks a little bit different now that I think about it I thought it was going to look the same, but it's also dependent on the theme that you're using.

Yeah, so these three are actually different, even though they're semantically the same. Well, that's interesting. I just literally found that out right now as well.

### Info, Todo

And there's info and todo. So this I believe is the todo. And if I go to info, it looks slightly different, just has a little eye here.

This is what I use for descriptions. What we call it in TTRPG is what we call box text, when players enter a new scene and there's a part that you read out word for word, rather than improvising it. This is what I use for it.

### Tip, Hint, Important

Then you have tips, hint, tip, hint, or important. This one is a tip.

Let's see what important looks like. All right. Yeah, all right. So there's some highlighting going on there as well, and it's marked by an asterisk.

### Success, Check, Done

There's success, check, done. I particularly like, I think it was done. It's kind of like the todo had a checkbox. This one actually has the check.

### Questions, Help, Faq

There's questions, help, or faq.

### Warning, Caution, Attention

There's warning, caution, or attention, which is what I use for errors or gotchas when you're programming or something things that you might fall into that I want to call out, because I did fall into it.

### Fail, Failure, Missing

And there's also fail, failure, and missing. So this is all bad stuff, essentially.

### Danger, Error

There's danger, error. It looks like this with a little lightning symbol.

### Bug

There's bug, which I use quite a bit when I'm reporting bugs or documenting bugs, anyway. This example, which kind of looks like abstract, actually.

### Quote, Cite

Quote, and cite, which is kind of a blockquote, except a little bit fancier.
