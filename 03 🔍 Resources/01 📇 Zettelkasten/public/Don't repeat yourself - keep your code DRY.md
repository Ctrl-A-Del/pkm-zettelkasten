---
sr-due: 2023-08-21
sr-interval: 40
sr-ease: 270
date created: Monday, July 10th 2023, 8:39:25 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🧹 Clean Code]]
> links:  [[Functions should be small and do one thing - they should have a single responsibility]]
> source: [Clean Code | Udemy](https://www.udemy.com/course/writing-clean-code/learn/lecture/23111248#overview)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

When writing the same code in multiple places, this does make your code harder to understand and more difficult to maintain because you have to make changes in several places. **Whenever you write the same code in multiple places, move it into a new function so that you only have to change it there**.