---
sr-due: 2023-09-28
sr-interval: 78
sr-ease: 314
date created: Monday, July 10th 2023, 8:41:06 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  
> - [[Getting better each day - the compounding effect of the one percent rule]]
> - [[Defining goals is difficult]] 
> - [[Five steps of project planning]] 
> - [[Five steps to deal with work]]
>
> source: 
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> The 80/20 principle describes the idea, that 20% of something are responsible for 80% of the outcome. 

The 80/20 principle, or Pareto principle, describes the idea, that **20% of something are responsible for 80% of the outcome**. 

This means that 20% of people drink 80% of the beer that is produced. Or that 20% of all people in the world have 80% of the global wealth.

If you use this principle for your daily life, it means that **you do 80% of a task in 20% of the time**. The **last 20% of the work, however, need 80% of the time**. This happens because it's easy to create a draft, but finalizing something until perfection and getting rid of all the issues takes a lot of time. This realization can help you to manage your time more efficiently. Do you really need the 100% perfect solution? Is this perfection worth the immensely higher amount of time? 

In other words: **==20% of all things you do impact your life by 80%==**. Find out which 20% are most important and consider focusing on those. This gives you a lot more time for other topics. 