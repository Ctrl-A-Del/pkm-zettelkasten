---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Friday, July 28th 2023, 1:14:28 pm
tags:
  - permanent-note
  - published
sr-due: 2023-08-01
sr-interval: 4
sr-ease: 270
---
| Topic             | Source |
| ----------------- | ------ |
| [[🧹 Clean Code]] | [Clean Code - Udemy](https://www.udemy.com/course/writing-clean-code/learn/lecture/23111340#overview)       |

---

Focus your classes to only have one single responsibility.

Don't create a Shop class that handles customers, products, orders, deliveries, …

Instead, create a new class for each responsibility.

Each class should handle a single business case, making it easy to replace and easy to understand.

- - -
## Links
1. [[Functions should be small and do one thing - they should have a single responsibility]]
2. [[SOLID Clean code in React]]