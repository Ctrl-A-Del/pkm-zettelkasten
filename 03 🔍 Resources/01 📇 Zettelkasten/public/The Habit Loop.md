---
sr-due: 2023-08-30
sr-interval: 48
sr-ease: 301
date created: Monday, July 10th 2023, 8:40:21 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  [[Getting better each day - the compounding effect of the one percent rule]]
> source: [[Atomic Habits]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---


A habit consists of 4 steps:

1. **Cue**: some kind of trigger that triggers a behavior. This can be a specific location, an event, or an object you see.
2. **Craving**: you feel the urge to do a specific thing. This is the result of the cue.
3. **Response**: you do the action you wanted
4. **Reward**: the feeling that you did what you wanted
![[Pasted image 20230713103356.png]]

Here is an example:

**Cue**: You walk past a coffee shop on the way to work and smell fresh roasted coffee.

**Craving**: Coffee gives you energy, and you want to feel energized.

**Response**: You buy a cup of coffee.

**Reward**: By the time you reach work, you are raring to go. Buying a cup of coffee becomes associated with your walk to work. 

These four steps can create either good or bad habits. To form a good habit, you should make use of the habit Loop. 

Cue: make it obvious 

Craving: make it attractive 

Response: make it easy 

Reward: make it satisfying 