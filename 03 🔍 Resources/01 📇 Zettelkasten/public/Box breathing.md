---
sr-due: 2023-10-12
sr-interval: 92
sr-ease: 310
date created: Monday, July 10th 2023, 8:41:07 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[🩺 Health]]
> links:  
> - [[Breathing triangle]]
> - [[CO2 tolerance]]
>
> source: [[Atmen - heilt - enntspannt - zentriert]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Box breathing is a breathing technique to calm your breathing.

Box breathing is an alternative to the [[Breathing triangle]]. **You take the same amount of time for inhale - pause - exhale - pause. 5 seconds per step are the average**. Shorter durations, like 3 seconds, are more energizing. Longer periods, like 6 to 8 seconds more relaxing. You can put an extra focus on the exhalation and the pause afterwards to train your [[CO2 tolerance]]