---
date created: Monday, July 10th 2023, 8:40:38 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---
> [!INFO]- 
> topic: [[🩺 Health]]
> links:  [[Measuring the control pause]]
> source: [[Atmen - heilt - enntspannt - zentriert]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p>

---

> Summary

This is a technique to measure your CO2 tolerance. You need a stopwatch to do it.

Sit down for a few minutes to relax. Breath normally. Take a deep breath an fill your lungs. Push the stopwatch and breathe out as long and as slow as you can. Stop the watch when you are down.

Below 20 seconds is considered bad.
20-40 seconds are average.
60-80 seconds are considered good.
