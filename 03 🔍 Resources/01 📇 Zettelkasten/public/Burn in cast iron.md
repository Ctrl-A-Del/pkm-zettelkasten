---
sr-due: 2023-09-06
sr-interval: 62
sr-ease: 310
date created: Monday, July 10th 2023, 8:41:14 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[🏡 Life]]
> links:  [[Burn out the grill]]
> source: 
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Cast iron creates a protection layer, called patina, by applying oil to it when it is heated to about 200 °C.

Every material made from cast iron, barbecue grate or pan, needs to be burned in, to create a patina. This is a protection layer for the cast iron.

To achieve this, heat the cast iron to about 200 °C. Then apply oil to the cast iron, for example, you can use a baking spray, or apply the oil with a silicon brush.

