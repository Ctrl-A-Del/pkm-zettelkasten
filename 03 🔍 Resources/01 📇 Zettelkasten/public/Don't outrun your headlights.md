---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
---
| Topic       | Source                       |
| ----------- | ---------------------------- |
| [[🖥️ Tech]] | [[The Pragmatic Programmer]] | 

---

> Outrunning your headlights means being faster than you can see.


The headlights of a car have only a limited distance that they illuminate.  This means if you drive too fast, you can't see what is coming at you. If you can only see 100 meters with your headlights and drive too fast, then stopping will take more distance than you can see. 

This effect of outrunning your headlights can also happen to you with planning and development. You can't predict the future. So only plan as far ahead as you can see, so probably only one or two steps. The rest just can't be predicted. 

In development, it is a good idea to make your code replaceable. It is the best way to prepare for the future. Don't plan too far ahead, but make it easy to adjust.

- - -
## Links
1. [[Planing fallacy - why we plan too optimistic]]
