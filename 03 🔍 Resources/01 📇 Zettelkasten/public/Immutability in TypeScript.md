---
sr-due: 2023-08-17
sr-interval: 37
sr-ease: 250
date created: Monday, July 10th 2023, 8:41:06 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🦒 TypeScript]]
> links:  [[Interfaces in TypeScript]]
> source: TypeScript course by the native web
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

To make a property in TypeScript immutable, you can make it `readonly`.

```ts
interface Person {
	readonly firstName: string; // this value cannot be changed after initialization
	lastName: string;
}
```
