---
sr-due: 2023-09-19
sr-interval: 69
sr-ease: 290
date created: Monday, July 10th 2023, 8:40:35 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[💵 Finance]]
> links:  [[Planing fallacy - why we plan too optimistic]]
> source: https://actualbudget.com/docs/budgeting/how-it-works/
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Give each dollar a purpose and set a budget for different purposes, so that your remaining budget, that doesn't have a purpose, is zero. Make spending a continuous decision.


This is a budgeting technique, to give *every dollar in your pocket a purpose*. 

**==The goal is to define different budgets for different purposes==.** You can imagine this like having separate envelopes for each purpose. You have an envelope for your rent, one for groceries, one for your hobby, health expenses etc. 

Then you take your monthly budget, and split it up between these envelopes. **The goal it to split your monthly budget into all these envelopes, so that your remaining budget is zero**. 

You want to limit the money you spend on specific purposes. It also allows you to set aside a specific amount to save some money for a bigger purchase. For example, you could put some cash aside each month for a new car. 

If your envelope for a topic is empty, that doesn't necessarily mean that you are not allowed to spend anymore in this category, but you have to take the money from somewhere else! So, you are allowed to go to the movies, even if your *fun money* is gone. But you have to find that 20 bucks somewhere. So maybe this means less money for clothes this month. **==The point is, to make a conscious decision and only spend money that you have**.==