---
sr-due: 2023-09-24
sr-interval: 75
sr-ease: 310
date created: Monday, July 10th 2023, 8:41:25 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[👪 Parenting]]
> links:  [[Erste Hilfe für Kinder]]
> source: [[Ist doch nicht so schlimm! -  Ist es doch. – Über das Trösten]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> We should always treat bad emotions with care and comfort the child, even if we do not think the situation is bad.

Babies cannot interpret their feelings when they are born. They don't understand why they feel what they feel and what this means. The caregiver needs to explain the different feelings to the child.

*"Are you hungry?" "Do you need a new nappie?" "Did you hurt yourself?"*  

Only if we verbalize the child's emotions, it can learn what these feelings mean. Because of this, it is significant, to take every emotion of the child seriously. If we do not acknowledge the situation, the child might learn that it should hide its feelings. 
By reacting and treating the situation with proper caution, the child learns that someone is there for it and how to verbalize emotions.

Sentences like *"That did not hurt." "Boys don't cry"*, *"It isn't that bad"* are wrong because they don't take the emotions serious. If a child cries, there always is a problem, even if we do not think there is. We should comfort the child, take it on our arm and speak to it.
Body and eye contact send out positive emotions and help to calm down.