---
date created: Monday, July 10th 2023, 8:41:01 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---
> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  [[Write down your achievements]]
> source: Berufsoptimierer #045
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> PETES is a framework to write down your achievements in a way that makes it easier to build a story from in your career.

 The letters originally describe German words, but I tried to translate them in a fitting way.

## Problem
- first write down the problem that you had

## Erschwernis (Encumbrance)
- what made this problem difficult?

## Tätigkeit (Task)
- what did you do, to fulfill this task, despite its difficulties?

## Erkenntnis/Erfolg (Experience)
- what did you learn from it?

## Stärke (Strength)
- which of your strengths does this emphasize?