---
sr-due: 2023-08-27
sr-interval: 47
sr-ease: 270
date created: Monday, July 10th 2023, 8:39:53 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[🦒 TypeScript]]
> links:  [[What is TypeScript]]
> source: TypeScript course by the native web
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---


To define a variable with a type in TypeScript, you define the type with a `:` like this:

```ts
let pi: number;
pi = 3.14;
```

TypeScript can defer a type. So if you add two numbers together, the result is also a number.

```ts
const result = 42 + 1; // result has the type number
```

