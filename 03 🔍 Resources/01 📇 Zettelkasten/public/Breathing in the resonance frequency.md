---
sr-due: 2023-09-26
sr-interval: 76
sr-ease: 312
date created: Monday, July 10th 2023, 8:41:00 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[🩺 Health]]
> links:  [[Natural Breathing]]
> source: [[Atmen - heilt - enntspannt - zentriert]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> The healthiest way to breathe is 5 seconds in and t seconds out 

Breathing slow is healthy for you nervel system and your heart. **The optimal frequency is 6 breaths each minute**. Inhalation and exhalation should happen synchronous. So you should inhale 5 seconds and exhale 5 seconds. 
A metronome or a breathing app can help to practice this. 
