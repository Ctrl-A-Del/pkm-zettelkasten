---
sr-due: 2023-10-10
sr-interval: 86
sr-ease: 314
date created: Monday, July 10th 2023, 8:41:29 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🖥️ Tech]]  
> links:  [[What to do after installing GrapheneOS]]  
> source: [Web installer | Install | GrapheneOS](https://grapheneos.org/install/web)  
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> The installation process for GrapheneOS is well explained and requires no specific knowledge.

Installing GrapheneOS is a piece of cake with the web installer and the instructions given on their [website](https://grapheneos.org/install/web). Next to no specific technical knowledge is required for the process.

For my device, there were a few things I needed to pay attention to, but it was all well explained.
