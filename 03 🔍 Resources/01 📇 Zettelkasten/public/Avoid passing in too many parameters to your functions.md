---
date created: Monday, July 10th 2023, 8:41:01 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
tags: [permanent-note, published]
sr-due: 2023-09-20
sr-interval: 50
sr-ease: 294
---
| Topic             | Source |
| ----------------- | ------ |
| [[🧹 Clean Code]] | [Clean Code - Udemy](https://www.udemy.com/course/writing-clean-code/learn/lecture/23111232#overview)       |

---

> Use as few parameters as possible. Consider using objects to reduce the number of parameters.

In many languages, like JavaScript, you pass in the parameters of your functions without explicitly adding the parameter name. This makes it **difficult to understand the parameters, the more a function has.**

So, this `log('message')` is straightforward to grasp, but this `transformData(1, 'userName', false` is very difficult because you don't have any clue what the parameters do and in what order you have to pass them.

One way to make this easier, is by **using objects as parameters**. This way, you can pass one parameter and map the corresponding values, making them much easier to read.

```js
class User {
  constructor(userData) {
    this.name = userData.name;
    this.age = userData.age;
    this.email = userData.email;
  }
}

const user = new User({ name: 'Max', email: 'max@test.com', age: 31 });
```

- - -
## Links
1. [[Clean code - write your code like an author writes a story]]
