---
sr-due: 2023-07-27
sr-interval: 28
sr-ease: 270
date created: Monday, July 10th 2023, 8:39:54 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🖥️ Tech]]
> links:  [[Big O Notation]]
> source: [[Cracking the Coding Interview]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> O(log N) describes an algorithm that halves N after each run, e.g. A binary search. 

If you have a logarithmic runtime, this describes that the number of repetitions is halved after each run. A good example is a binary search. 

A sorted array of elements is halved each time depending on a condition. This is a good indicator for O(log N). 