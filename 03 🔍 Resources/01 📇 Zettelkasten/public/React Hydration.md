---
date created: Thursday, July 13th 2023, 10:08:08 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---
> [!INFO]- 
> topic: [[🧩 React]]
> links:  [[Pros and cons of React]]
> source: [Understanding React Hydration | Gatsby](https://www.gatsbyjs.com/docs/conceptual/react-hydration/)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Hydration in React is the process of adding client-side JavaScript to server-rendered HTML to create a full React application with interactivity and event listeners.

Hydration is the process of **using client-side JavaScript to add application state and interactivity to server-rendered HTML**. It's a feature of [React](https://www.gatsbyjs.com/docs/glossary/react/).

A typical React application relies on client-side rendering. Instead of parsing HTML to create the [DOM](https://www.gatsbyjs.com/docs/glossary#dom), client-side rendering uses JavaScript to create it. A minimal HTML document serves as the application container, and only contains links to the JavaScript and CSS necessary to render the application.

However, you might need [[Server-side rendering improves your SEO and the client loading times|server-side rendering]] for SEO reasons. 

When a visitor requests their first URL from your site, the response contains static HTML along with linked JavaScript, CSS, and images. React then takes over and *hydrates* that HTML. **React adds event listeners to the DOM created during HTML parsing, and turns your site into a full React application**. Subsequent page requests are DOM updates managed by React.


