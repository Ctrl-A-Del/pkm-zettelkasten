---
date created: Jul. Mo, 2023 - 08:40
date modified: Jul. Fr, 2023 - 13:38
tags:
  - permanent-note
  - published
sr-due: 2023-08-01
sr-interval: 4
sr-ease: 286
---
| Topic | Source |
| ----- | ------ |
| [[🧹 Clean Code]]      |  [Clean Code - Udemy](https://www.udemy.com/course/writing-clean-code/learn/lecture/23111232#overview)      |

---

Doing everything in one huge function makes the function unnecessarily complex and difficult to understand. Split the functionality up into different operations. This gives you the opportunity, to name the new functions according to what they are doing, making it much easier to understand what is going on.

- - -
## Links
1. [[Clean code - write your code like an author writes a story]]
2. [[SOLID Clean code in React]]
3. [[Classes should be small and only have a single responsibility]]
