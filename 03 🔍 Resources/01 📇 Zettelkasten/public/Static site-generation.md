---
date created: Monday, June 26th 2023, 10:41:18 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
tags: [permanent-note, published]
sr-due: 2023-10-11
sr-interval: 84
sr-ease: 270
---
| Topic       | Source |
| ----------- | ------ |
| [[🖥️ Tech]] |   [BEST Framework for React JS (Gatsby vs Next.js) - YouTube](https://www.youtube.com/watch?v=c8_kEVCcQvg)      |

---

> Generates static content and deploys it to the web server.

Static side generation is a way to create high-performance websites out of dynamic templates. It allows you to deploy [[Headless Websites]], even though the content is dynamic, and uses a [[CMS]] or [[REST API]] to render the templates. To achieve this, static side generation renders static HTML files during the build process and deploying only those static files on the server.
As a result, the server doesn't need any complex setup, but only needs to host the content. This kind of website is also very fast because the content is already rendered on the server.

This kind of setup is great for websites with a lot of static content, but that still use a CMS, database or API to generate this content, e.g. blogs.

- - -
## Links
1.  [[Gatsby.js]] 
2. [[Next.js]] 
3. [[Server-side rendering improves your SEO and the client loading times]] 
4. [[How to scale static content]]
