---
sr-due: 2023-08-16
sr-interval: 37
sr-ease: 270
date created: Monday, July 10th 2023, 8:40:27 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[🖥️ Tech]]
> links:  
> - [[Static Website]]
> - [[Dynamic Website]]
>
> source: [What is a CDN? | How do CDNs work? | Cloudflare](https://www.cloudflare.com/learning/cdn/what-is-a-cdn/)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> A content delivery network (CDN) refers to a geographically distributed group of servers which work together to provide fast delivery of Internet content.

A content delivery network globally distributes your content across different servers around the world to make them easily accessible to everyone, no matter where they live. They avoid long loading times that might occur geographically and additionally can cache your content, to make it accessible as fast as possible. This ensures a better user experience and also makes you independent from one server.