---
sr-due: 2023-07-27
sr-interval: 13
sr-ease: 270
date created: Monday, July 10th 2023, 9:05:15 am
date modified: Thursday, August 3rd 2023, 3:23:48 pm
tags:
  - permanent-note
  - published
sr-due: 2023-10-19
sr-interval: 77
sr-ease: 290
---
| Topic | Source |
| ----- | ------ |
| [[🖥️ Tech]]      |   [BEST Framework for React JS (Gatsby vs Next.js) - YouTube](https://www.youtube.com/watch?v=c8_kEVCcQvg)      |

---

> [!tldr] Summary  
> Server-side rendering involves the server generating and sending a fully rendered webpage to the user's browser, improving load times and allowing for effective search engine crawling.

Server-side rendering (SSR) involves **the server creating the complete HTML content of a webpage and sending it to the user's browser**. This means that instead of waiting for the client's browser to render the page, the server takes care of it and sends a fully rendered page to the user.

This **allows search engines to effectively crawl and index the webpage's content**. Additionally, SSR **improves the initial load time** of a webpage because users don't have to wait for all the JavaScript files to be downloaded before seeing any meaningful content. This results in a better user experience as users can access relevant information more quickly.

SSR is commonly used in frameworks like React, Angular, and Vue.js, especially when client-side rendering (CSR) alone is not enough. By combining SSR with CSR, developers can achieve faster load times and provide a smoother browsing experience for their users.

- - -
## Links
1. [[Next.js]]
2. [[Static site-generation]]
