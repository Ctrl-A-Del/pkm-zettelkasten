---
sr-due: 2023-08-30
sr-interval: 50
sr-ease: 270
date created: Monday, July 10th 2023, 8:41:12 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[🧠 Psychology]]
> links:  [[Don't compare yourself to others]]
> source: [[The subtle art of not giving a fuck]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> There are numerous values that seem to be a good way to a happier life, but they are not.

You define how happy and successful you are by defining your measurements.

However, there are several values that seem to be good, but are actually awful measurements.

## Pleasure
While pleasure is something that makes you happy presently, it is not something that should define your life. A drug addict has a lot of pleasure, but certainly no happy life.

Pleasure is not the cause of happiness, it is the effect. By defining better values, we can be happier and experience pleasure along the way.

## Material Success
We tend to think that money will make us happy. But money alone doesn't help. You can have all the money in the world, but if you don't have time to enjoy it, it will not make you happy.

## Being Always Right
While being right is fun, it is simply often not true. We are wrong countless times. Probably more often than we are right. So if we aim to always be right, this is a lie and results in unhappiness because we can never achieve this goal.

## Staying Positive
We often tell ourselves that having a positive attitude will make you happy. But not everything has a positive side. You should admit negative events for what they are. Admitting problems enables you to solve them.
