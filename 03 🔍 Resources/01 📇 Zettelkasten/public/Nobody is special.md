---
sr-due: 2023-08-31
sr-interval: 50
sr-ease: 297
date created: Monday, July 10th 2023, 8:39:28 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: 🧠 Psychology
> links:  [[Don't compare yourself to others]]
> source: [[The subtle art of not giving a fuck]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Everybody is pretty average if you take everything into account. If you are great at one specific skill, you are bad or average at most other skills.

Nobody is special. There are numerous personalities that are seemingly great and countless people idolize those people. 
Celebrities, politicians, musicians, there are many public people who get our attention. 

But these people are not perfect. Usually, people only excel at one skill, but are pretty bad or average at the rest. 

This is normal because it requires significant effort to be great at something.

If we take everything into account, everybody is pretty average. Athletes aren't as smart as professors. Professors are not the best runners. Top managers don't have a lot of personal life etc. 

Don't compare your skills to someone, who focuses completely on one skill because if this is not your only focus in your life, you probably will never be as good as this person.

Don't idolize people. They are pretty average. Always remember that.


