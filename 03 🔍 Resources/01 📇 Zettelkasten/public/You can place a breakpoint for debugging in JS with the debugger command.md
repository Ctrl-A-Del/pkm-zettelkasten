---
date created: Monday, July 10th 2023, 8:41:33 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---
> [!INFO]- 
> topic: [[🖥️ Tech]]
> links:  [[Track the time of some of your JS code with console.time]]
> source: [Tips and Tricks for Debugging JavaScript - YouTube](https://youtu.be/_QtUGdaCb1c?t=518)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---


By using the `debugger` command, the browser stops code execution and jumps into the debugger at this code line. You can then use the debugger to jump around in the source code, view current values etc.