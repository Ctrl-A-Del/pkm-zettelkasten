---
date created: Monday, July 10th 2023, 8:40:09 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---
> [!INFO]- 
> topic: [[🧠 Psychology]]
> links:  
> - [[Responsiblity-fault fallacy]]
> - [[Going into any direction is better than standing still]]
> source: [[The subtle art of not giving a fuck]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> We tend to stick to behavior that fits our image of ourselves. However, this keeps us from change and stops us from personal growth and development

**The more something doesn't fit your personality, the more you avoid it.**

We define ourselves by different values and characteristics that make out our personality. Because of this, we usually try to stick to this personality as good as we can in our actions. Likewise, we do actions that fit our image of ourselves.

This can have positive and negative effects.

You avoid taking the hard truth to someone because you consider yourself to be a nice person and don't want to hurt someone.
You avoid a better job because you don't want to take the risk and work more.

Sticking to our personality locks us in acting a specific way and keeps us from change.

However, personalities change and change is a good thing. If something makes you unhappy, you should not hesitate because you think acting this way is part of your identity.

Don't stay at the same place all the time. Please don't hesitate to change and redefine your metrics and your personality.