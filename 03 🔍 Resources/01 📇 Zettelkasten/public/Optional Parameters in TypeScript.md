---
sr-due: 2023-09-02
sr-interval: 52
sr-ease: 270
date created: Monday, July 10th 2023, 8:40:18 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🦒 TypeScript]]
> links:  [[What is TypeScript]]
> source: TypeScript course by the native web
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---
You can define parameters to be optional with a `?`

```ts
// Optional parameters
const getFullName = function (firstName: string, lastName?: string): string {
  if (!lastName) {
    return firstName;
  }

  return `${firstName} ${lastName}`;
};

const fullName = getFullName('Steve', 'Jobs');
```