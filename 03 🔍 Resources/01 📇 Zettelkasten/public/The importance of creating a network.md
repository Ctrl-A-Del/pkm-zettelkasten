---
date created: Monday, July 10th 2023, 8:40:57 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---
> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  [[Write down your achievements]]
> source: [5 Gründe warum du im Job nicht weiter kommst (#249) - Berufsoptimierer - Erfolg in Bewerbung und Karriere](https://pca.st/episode/2177a31e-55cc-4657-93cc-2bf09f599f9e)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Knowing the right people can open up new opportunities. 

For many people creating a network of people, especially professionally is hard work. But it pays off. Knowing the right people in and outside of your company can create many great opportunities, that you wouldn't have heard of otherwise. Get to know the people that bring you forward and do the effort to keep up that relationship. 