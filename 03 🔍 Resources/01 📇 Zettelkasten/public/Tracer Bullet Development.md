---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags: [permanent-note, published]
---
| Topic        | Source |
| ------------ | ------ |
| [[🖥️ Tech]] | [[The Pragmatic Programmer]]       |

---

> Tracer bullet development means creating a working skeleton of your application that shows that all parts are working end to end.

Tracer bullets are bullets in a machine gun that are visible and help the shooter to improve their aim. Tracer bullet development is basically the same thing. It means that you make sure that everything works and that you are hitting your target. Your goal is not a fully fleshed out module for one part of your application, but to create a full end to end skeleton of your application. This framework only has a fraction of the functionality, but already shows that all parts of your code are working as expected. 

This can mean only having one button that communicates with your backend and reads something from your database. From this smallest working version, you can incrementally develop more features, making sure that everything is working from front to back.

There is a difference between tracer code and prototyping. Prototyping is disposable code that you're likely to throw away afterward. It is only a proof of concept. But tracer code is the working framework of your code. It's the skeleton upon which you build the rest of your application.

- - -
## Links
1. [[Knowledge Portfolio]]
