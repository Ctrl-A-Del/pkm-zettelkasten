---
sr-due: 2023-09-01
sr-interval: 51
sr-ease: 253
date created: Monday, July 10th 2023, 8:40:03 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[🧩 React]]
> links:  [[Pros and cons of React]]
> source: [Site Unreachable](https://www.youtube.com/watch?v=GGo3MVBFr1A&t=160s)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Don't use state for every variable that comes up. 

For example, if you have a login form with user and password, you should not write those variables into the state if you actually don't care if they update. If you update the state `onChange` of every letter that is written, you cause unwanted re-renders. Use `useRef` instead. Mapping the reference handles the update of the input and when you send the request, you just access the value via `emailRef.current.value`.