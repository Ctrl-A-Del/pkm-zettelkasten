---
sr-due: 2023-08-20
sr-interval: 41
sr-ease: 270
date created: Monday, July 10th 2023, 8:41:10 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[👪 Parenting]]
> links: 
> source: [[Das gewünschteste Wunschkind aller Zeiten treibt mich in den Wahnsinn]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> It is not automatically bad, if your child spends lots of time with media.

Just because your child spends time on media, this doesn't need to be bad. They can follow their interests and often use the internet to learn. They might explore science or a newHowever,ge. However there still is passive and active media consumption, so you should still keep an eye on the way your child uses media. But it is not bad per se.

