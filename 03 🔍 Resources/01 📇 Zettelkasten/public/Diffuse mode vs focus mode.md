---
sr-due: 2023-07-30
sr-interval: 14
sr-ease: 292
date created: Monday, July 10th 2023, 8:40:51 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[⚙️ Productivity]] 
> links: [Coursera | Online Courses & Credentials From Top Educators. Join for Free | Coursera](https://www.coursera.org/learn/learning-how-to-learn/home/welcome)
> source: 
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> There are two modes in our brain.

Our brain has two different modes to work in. When we focus on something, we try to identify patterns quickly and think in our known ways to analyze the problem at hand.
In diffuse mode, we trigger our creativity and let the mind wander.

