---
date created: Monday, July 10th 2023, 8:41:09 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
tags: [permanent-note, published]
sr-due: 2023-08-17
sr-interval: 29
sr-ease: 314
---
| Topic               | Source |
| ------------------- | ------ |
| [[⚔️ Role-playing]] | [[Return of the Lazy Dungeon Master]]       |

---

> Players want to see their characters succeed and do awesome stuff.

 No matter how much you think about your story as a Dungeon master, **the player characters play the main role**. So give them opportunities to shine. Create scenarios that fit their strength. Celebrate their victories with them. Don't be their enemy, be their buddy. 

- - -
## Links
1. [[You only have control over a game at the beginning of a session]]
