---
sr-due: 2023-09-11
sr-interval: 63
sr-ease: 293
date created: Friday, June 9th 2023, 9:45:35 pm
date modified: Sunday, July 16th 2023, 9:12:46 pm
tags: [permanent-note, published]
---
| Topic        | Source |
| ------------ | ------ |
| [[🧩 React]] | [Udemy](https://www.udemy.com/course/react-the-complete-guide-incl-redux/learn/lecture/25597264#overview)        |
|              |[styled-components](https://styled-components.com/)        |

---

> Styled-components is a react package to style your components in a way that avoids unwanted influence of other components.

Styled-components uses *[tagged template literals](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals)*, which is a default JavaScript feature. 

The syntax is something like this:
```jsx
const Button = styled.button`
	color: white;
`;
```

The `styled` package has functions for all default HTML elements.
It returns a component that adds unique class names to the component and puts the styling in there, avoiding the styling influencing other components.

- - -
## Links
1. [[Pros and cons of React]]
