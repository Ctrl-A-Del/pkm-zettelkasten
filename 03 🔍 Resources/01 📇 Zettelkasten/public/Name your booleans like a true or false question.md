---
sr-due: 2023-08-06
sr-interval: 31
sr-ease: 290
date created: Monday, July 10th 2023, 8:40:54 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🧹 Clean Code]]
> links:  [[Names in your code should be meaningful]]
> source: [Clean Code | Udemy](https://www.udemy.com/course/writing-clean-code/learn/lecture/23111126#overview)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

While your variable names should usually describe the value as clear as possible, your booleans should always be a question, which can be answered with yes or no. When in doubt, starting with `is` is often a good way.
Same goes for functions that compute a boolean.

`isLoggedIn`
`isValid`