---
sr-due: 2023-07-24
sr-interval: 25
sr-ease: 271
date created: Monday, July 10th 2023, 8:39:48 am
date modified: Thursday, July 27th 2023, 8:40:08 am
tags:
  - permanent-note
  - published
sr-due: 2023-10-07
sr-interval: 72
sr-ease: 271
---
| Topic            | Source |
| ---------------- | ------ |
| [[👪 Parenting]] | [[Erziehen ohne Schimpfen]]       |

---

> There will be situations you can not handle as a parent. Failure is okay. Don't beat yourself up about it.

It's okay to make mistakes. It's okay if your child doesn't behave perfectly. They are children. Don't best yourself up if they throw around Legos, scream at each other, or even hit other people. This is actually normal behavior for children. They are learning. As a result, not everything will work perfectly. It's okay to aim for the best you can. But you won't be able to shut this behavior down completely. Therefore, it's important to relax and keep in mind that you got time to fix this. 

We don't lose our face if we fail in these scenarios. This doesn't mean that everything is allowed all the time. It just means that we can relax and stop beating ourselves up.

- - -
## Links
1. [[Failure-success paradox]]
2. [[Failure leads to success]]
3. [[Don't compare yourself to others]]
4. [[The best parents I know make thirty mistakes each day]]
