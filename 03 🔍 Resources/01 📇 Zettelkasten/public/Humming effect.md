---
sr-due: 2023-09-17
sr-interval: 69
sr-ease: 310
date created: Monday, July 10th 2023, 8:41:22 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🩺 Health]]
> links: 
> source: [[Atmen - heilt - enntspannt - zentriert]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p>

---

> NO relaxes your body and lowers blood pressure. It can be produced by humming.

The molecule NO relaxes the body by widening the veins and reducing the blood pressure. It raises the amount of oxygen that is transported in your body with less pumping.

Our body can produce NO in our nose. Every breath we take through our nose adds some NO to our body. This is one of the reasons, why breathing through the nose is healthier than through the mouth.

The amount of NO produced can be increased about 15 times by humming during the exhalation.

Take a normal inhale and during the exhalation start humming and breath out slowly. Sowing this 3 to 5 times should already make you feel more relaxed.
