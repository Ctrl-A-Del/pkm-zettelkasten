---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
---

| Topic             | Source                         |
| ----------------- | ------------------------------ |
| [[🧠 Psychology]] | [[Manson-Everything Is Fcked]] |

---

> Pain is a constant in life, and that is a good thing. Suffering, however, is a choice. 

Without pain, live would be meaningless. Why? Because we *need* a challenge. We *need* to grow. We *need* goals. 

We will always hit walls. The question is, what you do with them. Do you succumb to the pain and suffer and complain, or do you change something?

- - -
## Links
1. [[Failure-success paradox]]
2. [[Failure leads to success]]
3. [[We don't need to be perfect parents]]
4. [[Grit is more important than talent]]
5. [[The Myth of Talent]]
6. [[We can never be happy]]
