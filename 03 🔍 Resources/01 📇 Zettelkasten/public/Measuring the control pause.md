---
sr-due: 2023-09-12
sr-interval: 62
sr-ease: 318
date created: Monday, July 10th 2023, 8:40:10 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🩺 Health]]
> links:  [[Natural Breathing]]
> source: [[Atmen - heilt - enntspannt - zentriert]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p>

---

This is a technique to **==measure your CO2 tolerance==**. You need a stopwatch to do it.
Sit down for a few minutes and relax. Afterward, exhale normally and stop breathing and start the stopwatch. It may help to close your nose with your fingers. Whenever you have a feeling, that this is not doable in a relaxed manner anymore, stop the watch. Follow the urge to breathe in normally. Different signs might show you, that this point is reached. You don't want to overdo it. The next breath should be a normal one.

**A value below 10 seconds is considered low.
A value around 15 seconds is average for a healthy person.
A good CO2 tolerance is reached at 25 seconds.**
