---
date created: Monday, July 10th 2023, 8:40:00 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---
> [!INFO]- 
> topic: [[🧠 Psychology]] 
> links:  
> - [[Pareto principle - the 80-20 rule]]
> - [[Defining goals is difficult]]
> - [[Don't compare yourself to others]]
> - [[Getting better each day - the compounding effect of the one percent rule]]
> - [[The Habit Loop]]
> - [[Willpower is depletable]]
> source: [[Grit - The Power of Passion and Perseverance]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Talent can tell you how easy something is for you. But to be one of the best, you have to strive to improve permanently. 

Of course, it is helpful to be talented in something. But to really be the best, you also have to show perseverance and work harder than the competition.