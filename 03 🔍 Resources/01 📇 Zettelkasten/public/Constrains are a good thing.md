---
date created: Monday, July 10th 2023, 8:40:01 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
tags: [permanent-note, published]
sr-due: 2023-07-23
sr-interval: 4
sr-ease: 278
---
| Topic                                                                 | Source |
| --------------------------------------------------------------------- | ------ |
| [[🧠 Psychology]] [[⚙️ Productivity]] [[🖥️ Tech]] [[⚔️ Role-playing]] | How to be great gm, David Lücke, dev of [[Feathters.js]]       |

---

> Don't see constraints as barriers, consider them to be guidance. 

You shouldn't see constraints as a barrier that limits you. They can give you direction. Especially for creative tasks, it can be a blessing to limit your options. For example, if you write a novel, the genre and time of the story give a ton of constraints about what is possible in that world. 

If you design a system of any kind, try to find as many constraints as possible to get a clear vision of the final result.

The more your imagination is directed, the better it can work. 

- - -
## Links
1. [[Constraints in attachment parenting]]
