---
date created: Monday, July 10th 2023, 8:41:18 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
tags:
  - permanent-note
  - published
sr-due: 2023-08-01
sr-interval: 4
sr-ease: 276
---
| Topic        | Source |
| ------------ | ------ |
| [[🧩 React]] | [React Testing Library and Jest: The Complete Guide - Udemy](https://www.udemy.com/course/react-testing-library-and-jest/learn/lecture/35701760#overview)       |

---

Request handlers only intercept the actual request you are doing (e.g. axios or fetch) and return some mock data instead of sending the request. This allows you to test all code except the actual request, which makes it superior to [[Module mocking is a bad way to test data fetching|module mocking]].

- - -
## Links
1. [[Module mocking is a bad way to test data fetching]]
