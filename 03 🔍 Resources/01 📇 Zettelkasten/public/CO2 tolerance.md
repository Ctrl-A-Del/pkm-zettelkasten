---
sr-due: 2023-08-02
sr-interval: 34
sr-ease: 294
date created: Monday, July 10th 2023, 8:41:42 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[🩺 Health]]
> links:
> - [[Measuring the control pause]]
> - [[Max Exhale Test]]
> - [[Natural Breathing]]
  >
> source: [[Atmen - heilt - enntspannt - zentriert]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p>

---

> The higher your CO2 tolerance, the slower your breathing.

The CO2 level inside your body regulates your breathing. A higher CO2 tolerance helps to breathe slower and calmer. It means that your next breath impulse starts at a higher CO2 level and therefore later.
