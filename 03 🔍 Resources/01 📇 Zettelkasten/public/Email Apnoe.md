---
date created: Monday, July 10th 2023, 8:39:38 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---
> [!INFO]- 
> topic: [[🩺 Health]]
> links:  [[Natural Breathing]]
> source: [[Atmen - heilt - enntspannt - zentriert]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p>

---

> When you have email apnoe, you subconsciously stop breathing when on a computer or phone.

Email apnoe is a breathing phenomenon. Many people actually stop breathing for a few seconds when on a computer and take a deep breath or sigh afterwards to make up for it.

This process happens subconsciously, but is not healthy.
