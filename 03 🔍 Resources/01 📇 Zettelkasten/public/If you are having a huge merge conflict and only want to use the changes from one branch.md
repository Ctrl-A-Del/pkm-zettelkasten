---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
---
| Topic        | Source                                                                                                                                  |
| ------------ | --------------------------------------------------------------------------------------------------------------------------------------- |
| [[🖥️ Tech]] | [github - Git merge with force overwrite - Stack Overflow](https://stackoverflow.com/questions/40517129/git-merge-with-force-overwrite) |

---

```sh
git merge --strategy=ours master
```


- - -
## Links
1. [[Git revert can help you undoing a specific commit]]
2. [[Git clean has the power to undo all untracked changes to your repository]]
