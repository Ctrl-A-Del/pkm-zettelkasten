---
date created: Monday, July 10th 2023, 8:39:46 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---
Hi there 👋

My name is [Sebastian](https://sebastianauner.com), and I am a German software developer, but my fields of interest go far beyond development and technology. I also like to learn about self-improvement, productivity, decision-making and much more.

It seems you have found my second brain. This is a place, where I share my learnings. In here it's messy and unpolished because these notes are mainly written for myself. I share those thoughts to work in public, [with the garage door open](https://notes.andymatuschak.org/z21cgR9K3UcQ5a7yPsj2RUim3oM2TzdBByZu), and therefore inspire and spark new ideas. So if you don't understand something, keep in mind, that these notes are focused on me, and not an audience. Because of this [[My Notes are not a Source of Truth]]. Feel free, to draw inspiration, though. If you spot something wrong, feel free to <a href="mailto:mail@sebastianauner.com">reach out to me</a>.

Enjoy your stay!

## Where to Start
Wherever you want! Search for a topic, click on random notes, explore the graph view, or start with these loose areas of interest. Below are a few of my favorites:

### ⚙️ Productivity
![[⚙️ Productivity]]

### 🧠 Psychology
![[🧠 Psychology]]

### 🖥️ Tech
![[🖥️ Tech]]

### 🧹 Clean Code
![[🧹 Clean Code]]

### 🦒 TypeScript
![[🦒 TypeScript]]