---
sr-due: 2023-07-29
sr-interval: 19
sr-ease: 250
date created: Wednesday, June 28th 2023, 9:44:21 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[🧹 Clean Code]]
> links:  [[Keeping your functions pure]]
> source: [Clean Code | Udemy](https://www.udemy.com/course/writing-clean-code/learn/lecture/23111258#overview)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

A side effect happens, when a function changes something in the state of your application. This could be writing to a database, connecting to an API, setting a variable outside the function's scope etc. 

Side effects are not bad per se, they are needed in your code. But unexpected side effects are. So, you should aim to name your functions in a way, that the side effect is to be expected.