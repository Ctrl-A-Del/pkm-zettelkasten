---
date created: Monday, July 10th 2023, 8:39:59 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---
> [!INFO]- 
> topic: [[👪 Parenting]]
> links:  [[Constraints in attachment parenting]]
> source: [Grenzen setzen in der bedürfnisorientierten Erziehung - Das gewünschteste Wunschkind](https://pca.st/episode/7df530a4-f085-4d3b-ba6c-5596a9708481)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> It's not just important to communicate boundaries, but also **how** you do it.

You need to communicate your limits clearly, but this doesn't mean you always have to say no. It's important how you communicate your constraints. Consider saying "yes, I see want to play with food now, it's okay if you want to play now. But I don't want food flying around. Let's first get you cleaned up, and then you can go playing" You don't just get into a discussion about playing with food but instead understand what your child wants and allow it to play. But you still communicate that you don't want food flying around or having your couch painted in with sauce. 
Always try to express that *you don't want that* and put a focus on your personal limits. 

