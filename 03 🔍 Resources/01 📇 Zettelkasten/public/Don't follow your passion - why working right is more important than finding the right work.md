---
sr-due: 2023-08-05
sr-interval: 30
sr-ease: 293
date created: Monday, July 10th 2023, 8:41:41 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  
> - [[Grit is more important than talent]]
> - [[Building a Bridge instead of taking the Leap]]
> - [[Going into any direction is better than standing still]]
> - [[We don't need a map, we need a shovel]]
> - [[Failure-success paradox]]
> source: [[So good they can't ignore you]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Following your passion is bad advice.
> Finding the right work pales in comparison to working right. 

Following your passion is a modern approach being praised to be happy at your job. However, lots of passions are actually very limited to make a career out of. Many passions include hobbies or sports, where only very few can succeed. It also builds a trap of always chasing for your passion and wondering, if what you are currently doing is the right thing, leaving you confused.

Instead, passion comes with experience and mastery, because it builds confidence and gives you more flexibility when doing your work. Having better terms makes your work more pleasing.
