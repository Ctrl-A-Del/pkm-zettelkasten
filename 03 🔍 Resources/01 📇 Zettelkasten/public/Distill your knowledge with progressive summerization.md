---
date created: Tuesday, July 11th 2023, 10:10:10 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
tags: [permanent-note, published]
sr-due: 2023-08-18
sr-interval: 30
sr-ease: 290
---
| Topic                                | Source |
| ------------------------------------ | ------ |
| [[📚 Personal Knowledge Management]] | [[Building a Second Brain]] Ch. 9       |

---

> Progressive summarization is a technique that involves gradually distilling and condensing information into its most important and concise form, making it easier to understand and remember.

Progressive summarization is a technique to distill the most important information from your notes. You start by marking the most important insights and highlights from a text in bold text. Another step you can then further distill this down and highlight the most important takeaways from the bold text in yellow, and then you can even go one step further and create an executive summary that you would basically you tell if you have a meeting with your CEO and you need to bring your point across very, rapidly. And basically in one sentence, you don't have to do this in one single swipe, but you can have multiple stages whenever you come across the note to do this, I make it easier and easier to learn the content

It reflects the process of highlighting in multiple layers, to get down to the essence of the information.

You start by marking the most important insights and highlights from your source or your summary in **bold** letters. In another step, you can then further distill this down and highlight the most important takeaways from the bold text in ==yellow==. For the most important notes, you can create an *executive summary* in your own words, by writing it down in one sentence or as few bullet points as possible.

You don't need to - and you shouldn't - do this all in one session. Consider doing the first step initially, but do each other step whenever you realise that you revisit the note often.

- - -
## Links
1. [[CODE Framework second brain]]
2. [[Understanding what you read]]
