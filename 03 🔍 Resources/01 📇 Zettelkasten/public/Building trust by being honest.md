---
sr-due: 2023-09-25
sr-interval: 75
sr-ease: 290
date created: Wednesday, July 12th 2023, 12:08:38 pm
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[🧠 Psychology]]
> links:  [[Unlinked]]
> source: [[The subtle art of not giving a fuck]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> If you want people to trust you, you need to be honest with them.

If you want to build trust with other people, here is only one rule: be honest with them. How can someone trust you, if they know that you lie to them? 
Honesty is more important than making other people happy.

It might be hard to tell the truth sometimes, but it is the right way.

Only if you tell people how you think, you can give them the option to deal with this opinion. This might sometimes result in conflict, and that is okay.

Without conflict, there can be no trust.