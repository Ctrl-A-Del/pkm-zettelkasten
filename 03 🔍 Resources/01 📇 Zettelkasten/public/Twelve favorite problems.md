---
date created: Wednesday, July 12th 2023, 11:28:48 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
tags: [permanent-note, published]
sr-due: 2023-07-23
sr-interval: 4
sr-ease: 273
---
| Topic                                | Source |
| ------------------------------------ | ------ |
| [[📚 Personal Knowledge Management]] | [[Building a Second Brain]]       |

---

> Create a list of 12 problems that you want to solve in your life.

## What is it
Richard Phillips Feynman was one of the most important scientists of the 20th century. He achieved this by always writing down his twelve favorite problems. The big questions he wanted to answer. Every note he wrote down should answer one of those questions.

You can do the same thing for your life.

Create a set of twelve thought-provoking or introspective questions. These questions can vary from person to person and can cover a wide range of topics, including personal values, goals, beliefs, experiences, relationships, and aspirations.

The questions should be open so that you have several options to answer them.

## Inspiration
Here is an inspirational list of potential questions, taken from [Tiago Fortes Favorite Favorite Problems](https://fortelabs.com/blog/my-favorite-favorite-problems).
### **Self-improvement**

- How can I build a good reading habit?
- What can yoga teach me and move me towards?
- How do I improve the functionality of my brain including cognitive processing and memory?
- How can I improve every day by 1%?
- How can I structure my goals so I increase my chances of achieving them?
- What are the classics? What are they trying to teach us? And how is it relevant?
- How can I consistently achieve the goals I set for myself?
- What facets of my life can I automate?
- How can I become friends with people that I want to be like?
- What does systems thinking have to teach us about healing and personal growth?

### **Hobbies**

- What are ways I can be a more engaged, connected, and playful dancer?
- What is the most efficient way to learn how to draw?
- How can I improve my skateboarding skill?
- How can I use knitting as a means of bringing healing to the world?
- What kind of music do I want to create?
- How can I reduce friction to ensure practicing the keyboard is a part of my daily routine?

### **Self-care**

- How can I live joyously with menopause and care for the aging female brain?
- What can I do to switch off and relax from time to time?
- How can I eat healthy food that is delicious, pescetarian, and gluten-free?
- How can I better strengthen and soothe the aching muscles in my feet, neck, and jaw?
- How do I create a body that I am proud of and that serves me well?
- How can I balance my personal life with my professional life while still feeling productive and fulfilled?
- How can I achieve regular deep sleep?
- How can I leverage social media in healthy and productive ways that add value to my personal and professional life?
- What are the optimum behaviors, habits and tools to mitigate my cognitive challenges (ADD)?

### **Self-reflection**

- How am I my own biggest stumbling block?
- How do I overcome my personal bottlenecks to be authentic, vulnerable, and risk failure?
- How do you find your way after losing your faith?
- What does living in integrity with myself look like?
- What does celebrating my life, showing gratitude for my life, look like?
- What can I do to accept myself as I am?
- What in my past casts a shadow on my present?
- What are the most simple and universally effective practices to experience and sustain inner peace?
- What am I missing?
- What does addiction and recovery have to teach us?

### **Business/career**

- How can I become a more natural and effective collaborator?
- How can I make problem-solving fun and attractive?
- How can I generate income pursuing my interests?
- What are the key skills to prepare for the future of music?
- What other things should I learn/unlearn to be an effective Executive Assistant?
- What forms of marketing are ethical, profitable for the marketer, and useful for the audience?
- How do I build a career I don't want to retire from?
- How do I create residual streams of income that can buffer my productivity highs and lows?
- What is the best platform for me as a creator?
- What should my personal brand be, and how do I get it there?

### **Finances**

- What should I invest my money into so it can work harder for me?
- How do I catch up building a nest egg midlife?
- How can we balance investing strategically with spending money on what we love today?

### **Leadership**

- How can I improve my leadership skills and consistently support, empower, and challenge those around me?
- What processes and people should I put in place in order to replace myself in the current business so I can focus on the next phase of the business before IPO?
- How do we create messages that are unique and persuasive to rural voters in Maine?

### **Spouse/partner**

- How do I express myself clearly and more often to my wife to show her that I love her to the moon and beyond?
- How can we cultivate a romantic relationship of respect, understanding, support, care, affection, and passion as a couple, which will serve as an example to our children so that they grow up to become adults responsible for the reality they live in, respecting themselves and others?
- How can I curiously explore life together with my life partner?

### **Environment**

- How can we learn to share and manage collective resources?
- How can we use models from nature to solve our gnarliest problems?
- How can I make environmental issues and climate justice appealing to Christian congregations?
- How can we reduce pollution while improving quality of life?
- How can I travel sustainably without harming the planet or other people?
- How do I determine the best intervention point in a dynamic but failing system?

### **Values**

- How can I not lose track of my core values and live up to them?
- How can I foster greater understanding, equality, and unity without disenfranchising any in the landscape of gender diversity?
- How do I live a fulfilled life with no regrets in all aspects of my life?
- How can I deepen my self-awareness and sharpen the values that are most personal to me, so that they permeate all areas of my life?
- What can I do to get clearer on my purpose?
- How can people utilize technology in a way that corresponds to their values?

- - -
## Links
1. [[CODE Framework second brain]]
2. [[When to capture something into your second brain]]
