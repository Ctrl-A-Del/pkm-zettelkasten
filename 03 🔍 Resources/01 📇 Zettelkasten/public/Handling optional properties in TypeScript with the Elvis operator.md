---
sr-due: 2023-08-08
sr-interval: 33
sr-ease: 250
date created: Monday, July 10th 2023, 8:41:01 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🦒 TypeScript]]
> links:  [[Interfaces in TypeScript]]
> source: TypeScript course by the native web
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

When handling optional parameters in TypeScript, you have to handle cases, where this parameter is not set. You can do this with the *Elvis operator* `?.`

```ts
console.log(donuldDuck.middleName?.toLocaleUpperCase());
```

The code to the right of the `?` is only executed, if the property before it is defined.