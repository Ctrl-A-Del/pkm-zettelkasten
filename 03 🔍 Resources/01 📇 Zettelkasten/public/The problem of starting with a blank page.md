---
date created: Monday, July 10th 2023, 8:39:50 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---
> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  
> - [[What is a Zettelkasten]] 
> - [[The Habit Loop]] 
> - [[Why I write my notes in English]] 
> - [[Do Something Principle]]
> source: [[06 🗃️ Archiv/03 🗄️ References/How to take smart notes]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> The most difficult thing in writing is starting with a blank page, not knowing what to write about and where to look for information. Having a smart note-taking system can make this much easier.

#### The Problem
The most difficult thing in writing is the beginning. Having a blank page in front of you and no idea what to write. Maybe you know the topic, but perhaps you don't even know what to write about. 

There is a fundamental misunderstanding about how our brain works and how we come off with ideas. Writing becomes significantly easier the more text you have already written. This does not only include notes and references about the topic (or having a way to come up with a topic), but also if you have already written a few pages and know where things are going.


#### How smart notes can help
What if you already have different text excepts about your topic and only need to plug them together and write them into one single argument? This seems easy, doesn't it? 

What if you come up with new ideas and stumble upon facts you read a long time ago naturally, while you are reading through your ideas, notes and make connections between them? That would make it easier to come up with the next paragraph or chapter.

These are a few examples, why a good note-taking system can help you to make the whole process of writing and learning much easier. The better your system, the easier it becomes. Success is not the result of strong willpower and much work, but how frictionless you can organize your working environment.


#### Just start
If you are struggling to get started, just start. [[Do Something Principle|Do something]]. Set up a timer and start writing, event it if it is awful and you will scrap it later. Just get started and go from there. Having *something* can be enough to get you to break the ice and stop procrastination.