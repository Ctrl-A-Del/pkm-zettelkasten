---
sr-due: 2023-07-22
sr-interval: 16
sr-ease: 250
date created: Tuesday, July 11th 2023, 4:30:37 pm
date modified: Sunday, July 16th 2023, 9:11:04 pm
tags:
  - permanent-note
  - published
sr-due: 2023-11-09
sr-interval: 98
sr-ease: 270
---
| Topic       | Source |
| ----------- | ------ |
| [[🖥️ Tech]] |   [Rubber duck debugging - Wikipedia](https://en.wikipedia.org/wiki/Rubber_duck_debugging)      |

---

> Rubber duck programming is a debugging technique where programmers explain their code to an inanimate object, often a rubber duck, in order to identify errors and find solutions.

Did you ever have a problem in coding and asked another developer to help you? Have you then started to explain the problem to him or her and started explaining what the code does, just to find the error all by yourself without the other dev really doing anything? 

You basically could've done the same with a rubber duck, because the other person didn't really do anything.

This is called Rubber duck programming. 

It is is a technique to debug code or solve problems by explaining the code line by line or step by step to an inanimate object, typically a rubber duck. 

The idea is that by verbalizing the problem and explaining it in simple terms, the programmer can often find the solution or identify any errors in their code. The rubber duck serves as a "listener" and helps you to think through their code logically and catch issues that may have been overlooked. 

- - -
## Links
1. [[You can place a breakpoint for debugging in JS with the debugger command]]
