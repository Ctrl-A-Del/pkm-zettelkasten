---
tags: [permanent-note, published]
sr-due: 2023-07-23
sr-interval: 4
sr-ease: 270
date created: Sunday, July 16th 2023, 6:09:22 pm
date modified: Wednesday, July 19th 2023, 10:37:28 am
sr-due: 2023-08-23
sr-interval: 27
sr-ease: 290
---
| Topic             | Source |
| ----------------- | ------ |
| [[🧹 Clean Code]] | [Clean Code - Udemy](https://www.udemy.com/course/writing-clean-code/learn/lecture/23111288#overview)       |

---

You can avoid deeply nested conditions by using guards. This means you invert the if-condition and return early in this if check. This guard *protects* the rest of the code, if the condition is not met and avoid it from running.

Guards make it much easier to read code by avoiding deeply nested structures.

```js
if (!email.includes('@')) {
	return;
}
```

- - -
## Links
1. [[Embrace refactoring - Clean code takes time]]
