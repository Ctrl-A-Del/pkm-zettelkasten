---
sr-due: 2023-08-10
sr-interval: 41
sr-ease: 290
date created: Monday, July 10th 2023, 8:41:41 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic:[[👪 Parenting]]
> links:  [[What a child wants is something different than what of needs]]
> source: https://pca.st/episode/7df530a4-f085-4d3b-ba6c-5596a9708481
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Communicate your boundaries clearly.

There is a modern way of parenting that aims to be nice to your child and avoid saying no to everything. While this is a great start, and it is great to stop saying no out of a principle, this doesn't mean boundaries are not allowed. **Boundaries are important.** But it's also important to have realistic constraints. They should be your personal no-gos. There is no general boundary, but only those that *you* can't deal with. For some parents, playing with food is a hard no, for others this is not a big deal. Always communicate your personal limits and explain them to your child. 

In addition, there obviously are some limits that are dangerous and are never allowed. Running to the street without watching and holding your hand. Not using the seatbelt etc.