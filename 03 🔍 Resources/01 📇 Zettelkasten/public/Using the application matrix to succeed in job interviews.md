---
date created: Monday, July 10th 2023, 8:39:27 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---
> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  
> - [[Failure leads to success]]
> - [[Reflecting on your work]]
> - [[The Myth of Talent]]
> - [[You can find new colleagues somewhere else]]
> source: Berufsoptimierer Podcast #045
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> The application matrix is a very detailed option to prepare for a job interview.

If you have trouble preparing for an important job interview, you can create an application matrix. This helps you to deep dive into the job description, your skills and prepare questions in advance.

The matrix has three columns and three rows.

|              | Job                                                    | How do I understand that? What do they mean?                                                          | How can I help with that?                         |
| ------------ | ------------------------------------------------------ | ----------------------------------------------------------------------------------------------------- | ------------------------------------------------- |
| Tasks        | Write down all tasks the employer mentions             | Write down, how you understand the task at hand, what does it mean? what questions do you still have? | Which skills, experiences and ideas do you bring? |
| Requirements | Write down all requirements, the employer is expecting |                                                                                                       |                                                   |
|              |                                                        |                                                                                                       |                                                   |

