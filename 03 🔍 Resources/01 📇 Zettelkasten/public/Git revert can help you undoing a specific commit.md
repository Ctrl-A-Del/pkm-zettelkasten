---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
---
| Topic        | Source                                                                                                                                            |
| ------------ | ------------------------------------------------------------------------------------------------------------------------------------------------- |
| [[🖥️ Tech]] | [How can I undo a specific old commit? - Learn Version Control with Git](https://www.git-tower.com/learn/git/faq/undo-revert-old-commit/)]        |
|              | [github - Reverting specific commits from git - Stack Overflow](https://stackoverflow.com/questions/20324638/reverting-specific-commits-from-git) |

---

> Use `git revert` to undo changes of a specific commit.

You can use `git revert` to specifically undo changes you made in a specific commit.

 Here’s how you can do it:

1. First, you need to find the hash of the commit you want to revert. You can do this by using the `git log` command and looking through your commit history.
    
2. Once you have the hash of the commit, you can use the `git revert` command with the `--no-commit` option. This will apply the changes necessary to revert the specified commit, but it won’t actually create a new commit with those changes.
    

Here’s what the command would look like:

```bash
git revert --no-commit <commit-hash>
```

After running this command, the changes will be staged but not committed, allowing you to review them before you decide to commit.

The `git revert --no-commit <commit-hash>` command will only revert the changes made in the specific commit identified by `<commit-hash>`. It will not affect any changes made in other commits between this commit and the most recent commit (HEAD).

- - -
## Links
1. [[Git clean has the power to undo all untracked changes to your repository]]
