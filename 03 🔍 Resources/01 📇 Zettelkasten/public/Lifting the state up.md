---
date created: Monday, July 10th 2023, 8:41:19 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
sr-due: 2023-07-22
sr-interval: 4
sr-ease: 273
tags:
  - permanent-note
  - published
sr-due: 2023-09-23
sr-interval: 53
sr-ease: 293
---
| Topic        | Source |
| ------------ | ------ |
| [[🧩 React]] |[Sharing State Between Components – React](https://react.dev/learn/sharing-state-between-components#lifting-state-up-by-example)        |

---

> Lifting the state up means to handle the state by a common parent component, that orchestrates the state for its children.

If you have two components and need to pass a value between both of them, e.g. because the first component has an event listener and the other components needs the value from the event, then you need to lift that state up. To achieve this, you have to create this state into the common parent component of the other two components. **The parent handles the state and is the single source of truth.** From the parent, you pass down the event handler function to the first component, so that the function in the parent is executed and updates the state directly in the parent. The parent can then pass down this value as props to the second component.

- - -
## Links
1. [[React state does not update immediately - use useEffect to work with the new value]]
2. [[SOLID Clean code in React]]
