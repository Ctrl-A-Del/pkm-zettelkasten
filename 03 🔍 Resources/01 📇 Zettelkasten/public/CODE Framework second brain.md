---
date created: Tuesday, July 11th 2023, 2:40:59 pm
date modified: Sunday, July 16th 2023, 9:11:05 pm
---
> [!INFO]- 
> topic: [[📚 Personal Knowledge Management]]
> links:  
> - [[Capture Everything in an Inbox]]
> - [[When to capture something into your second brain]]
> - [[Organize your second brain with PARA]]
> - [[Distill your knowledge with progressive summerization]]
>
> source: [[Building a Second Brain]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> The CODE Framework provides a systematic approach to organizing and leveraging information effectively.


The CODE Framework is a method for organizing and managing information in a [[What is a second brain|second brain]].

The framework consists of four components: 
- Capture
- Organize
- Distill
- Express

In the **Capture** phase, users collect information from various sources such as articles, books, websites, or personal experiences. This can be done using note-taking apps, bookmarking tools, or any other method that suits the individual.

In the **Organize** phase, the collected information is categorized and stored in a way that makes it easily accessible and searchable. This can involve creating tags or labels to group related content together, or using hierarchical structures like folders or nested categories.

The **Distill** phase focuses on extracting meaningful insights from the captured information. This involves reviewing and synthesizing the content to identify key ideas or patterns. It may also involve adding personal thoughts or annotations to further enhance understanding.

Finally, in the **Express** phase, the distilled knowledge is shared with others through various mediums such as writing articles, creating presentations, recording videos, or engaging in discussions. This step not only helps reinforce one's own understanding, but also allows for collaboration and contribution to broader communities.