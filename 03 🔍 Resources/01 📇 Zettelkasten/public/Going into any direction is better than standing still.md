---
date created: Monday, July 10th 2023, 8:39:23 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---
> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  
> - [[We don't need a map, we need a shovel]]
> - [[Constrains are a good thing]]
> - [[Rejection is a Life Skill]]
> - [[Failure-success paradox]] 
> - [[Mansons Law of Avoidance]]
> - [[Certainty effect - why we prefer a worse outcome as long as it is save]]
> - [[Getting better each day - the compounding effect of the one percent rule]]
>
> source: [[The Art of Work]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> You need to overcome the urge of only wanting to go into the right direction. This will freeze you in place because you don't know where to go. Just go into any direction. 

We tend to stay in our comfort zone and end up *standing still* in our lives. We can't move forward because we don't know in which direction to go. However, that is actually not that important. Just walking into any direction is better than standing still. Because standing still means you can't develop. If you continue to move forward, you either succeed, or you learn from your mistakes. Making these mistakes is an important part of the process. Don't be afraid to test out new things, don't fear failure. 
The path is about going into a direction and not the destination. 