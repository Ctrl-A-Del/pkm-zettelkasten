---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
---
| Topic       | Source |
| ----------- | ------ |
| [[🖥️ Tech]] |[git reset --hard HEAD leaves untracked files behind - Stack Overflow](https://stackoverflow.com/questions/4327708/git-reset-hard-head-leaves-untracked-files-behind)        |

---

> To reset your Git repository and remove untracked files, you can use the `git clean` command. 
 

Here’s how:

1. [First, you can use `git clean -n` to perform a “dry run” and see which files would be removed](https://stackoverflow.com/questions/4327708/git-reset-hard-head-leaves-untracked-files-behind)[1](https://stackoverflow.com/questions/4327708/git-reset-hard-head-leaves-untracked-files-behind).

```bash
git clean -n
```

2. [If you agree with the files to be removed, you can use `git clean -f` to remove untracked files](https://stackoverflow.com/questions/4327708/git-reset-hard-head-leaves-untracked-files-behind)[1](https://stackoverflow.com/questions/4327708/git-reset-hard-head-leaves-untracked-files-behind).

```bash
git clean -f
```

3. [If you want to also remove untracked directories, you can use `git clean -fd`](https://stackoverflow.com/questions/4327708/git-reset-hard-head-leaves-untracked-files-behind)[1](https://stackoverflow.com/questions/4327708/git-reset-hard-head-leaves-untracked-files-behind)[2](https://stackoverflow.com/questions/8200622/how-to-remove-untracked-files-in-git).

```bash
git clean -fd
```

4. [If you want to remove ignored files as well, you can use `git clean -fdx`](https://stackoverflow.com/questions/4327708/git-reset-hard-head-leaves-untracked-files-behind)[1](https://stackoverflow.com/questions/4327708/git-reset-hard-head-leaves-untracked-files-behind)[2](https://stackoverflow.com/questions/8200622/how-to-remove-untracked-files-in-git).

```bash
git clean -fdx
```

Please be careful when using these commands as they will permanently delete the untracked and/or ignored files. 

- - -
## Links
1. [[Git revert can help you undoing a specific commit]]
