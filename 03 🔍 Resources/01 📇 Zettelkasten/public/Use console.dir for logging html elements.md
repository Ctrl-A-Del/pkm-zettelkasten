---
date created: Monday, July 10th 2023, 8:39:33 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
tags: [permanent-note, published]
sr-due: 2024-02-01
sr-interval: 192
sr-ease: 315
---
| Topic        | Source |
| ------------ | ------ |
| [[🖥️  Tech]] | [Tips and Tricks for Debugging JavaScript - YouTube](https://youtu.be/_QtUGdaCb1c?t=158)        |

---

Don't use `console.log` to always log in JS. `console.dir` can actually handle the output of HTML elements better and give you more information

- - -
## Links
1. [[Use console.table to print out huge objects into the console]]
