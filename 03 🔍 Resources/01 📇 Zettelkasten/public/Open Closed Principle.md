---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
sr-due: 2023-08-01
sr-interval: 4
sr-ease: 279
---
| Topic             | Source |
| ----------------- | ------ |
| [[🧹 Clean Code]] |  [Clean Code - Udemy](https://www.udemy.com/course/writing-clean-code/learn/lecture/23111358#overview)       |

---

A class should be open for extension but closed for modification.

The goal is to avoid classes from growing each time a new functionality is added. 

This is a bad example of a printer class, that handles all different possibilities:

```js
class Printer {
	printPDF(data: any) {
	 // ...
	}
	
	printWebDocument(data: any) {
	 // ...
	}
	
	printPage(data: any) {
	 // ...
	}
	
	verifyData(data: any) {
	 // ...
	}
}
```

This is suboptimal because if something in the way printing works is changed, you have to change it in all those functions.

Instead, you should create specific classes that can be extended when needed:
```js
interface Printer {
  print(data: any);
}

class PrinterImplementation {
  verifyData(data: any) {}
}

class WebPrinter extends PrinterImplementation implements Printer {
  print(data: any) {
    // print web document
  }
}

class PDFPrinter extends PrinterImplementation implements Printer {
  print(data: any) {
    // print PDF document
  }
}

class PagePrinter extends PrinterImplementation implements Printer {
  print(data: any) {
    // print real page
  }
}
```

The base printer class can be closed for modification. Instead, you can extend it by different subclasses.

This ensures small classes instead of growing classes, keeping the code DRY.
- - -
## Links
1. [[Classes should be small and only have a single responsibility]]
2. [[SOLID Clean code in React]]
3. [[Don't repeat yourself - keep your code DRY]]
