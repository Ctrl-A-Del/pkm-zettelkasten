---
sr-due: 2023-09-01
sr-interval: 53
sr-ease: 292
date created: Monday, June 26th 2023, 10:42:20 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🧠 Psychology]]
> links:  
> - [[You against yourself]] 
> - [[Responsiblity-fault fallacy]] 
> - [[Mansons Law of Avoidance]]
>
> source: [[The subtle art of not giving a fuck]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Avoiding failure means we can't have success.

Failure is a painful and unpleasant process.

However, failure is needed for success.

You have to take chances to learn what works and what doesn't. You have to make mistakes to improve.

Avoiding failure means we can't have success.