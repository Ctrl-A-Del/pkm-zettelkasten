---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
---

| Topic     | Source                                                                                          |
| --------- | ----------------------------------------------------------------------------------------------- |
| [[React]] | [All 29 Next.js Mistakes Beginners Make - YouTube](https://www.youtube.com/watch?v=5QP0mvrJkiY) |

---

A component can be a client component, even if `"use client"` is not explicitly written at the top of it, when it is imported and used inside another client component (e.g. a button used inside some navigation bar, where the bar is a client component). However, it is best not to rely on this and make the button explicitly a client component if it needs client-side logic. This makes the code more maintainable and easier to understand


- - -
## Links
1. [[Don't create Next.js client components to high in your component tree]]
