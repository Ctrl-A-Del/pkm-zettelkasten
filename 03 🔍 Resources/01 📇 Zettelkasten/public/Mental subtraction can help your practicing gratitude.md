---
date created: Monday, July 10th 2023, 8:41:05 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
tags: [permanent-note, published]
sr-due: 2023-10-05
sr-interval: 79
sr-ease: 290
---

| Topic             | Source |
| ----------------- | ------ |
| [[🧠 Psychology]] |   [6 Minute Podcast - Mental Substraction](https://audio.podigee-cdn.net/900290-m-49223714d16a2015dfb486641d86a334.mp3?source=feed)     |

---

> By imagining that something is missing, you might realize how important it really is to you.

Mental subtraction is an exercise to practice gratitude. We often don't see, what we currently have and only realize what is missing. To change this way of thinking, you can use this exercise. 

Think about something or someone ordinary in your life. It should be something that seems normal for your gut that you use or talk too frequently. Now imagine, this person or thing would suddenly be gone. How would your life change? What would be missing? 

This can help you see the value in something or someone for you. 

- - -

