---
sr-due: 2023-09-10
sr-interval: 60
sr-ease: 314
date created: Monday, July 10th 2023, 8:40:39 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[🖥️ Tech]]
> links:  [[Hosting your own image gallery]] [[Installing PhotoPrism on your Pi]]
> source: 
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> You should add an external drive to your [[Hosting your own image gallery|PhotoPrism]] instance because otherwise it will put all data on the SD card.

I only use a pretty small 32 GB SD card for my PhotoPrism instance and save all other data to an external drive. Since SD cards have a limited life span, this allows my data to be more secure.

A SSD is recommended for a better speed at indexing.

I suggest doing this directly after PhotoPrism is installed on your Raspberry[^1].

To properly use an external SSD drive for your Photoprism, a few steps are needed:

1. [[RaspberryPi mount external drive]]
2. [[RaspberryPi automount external drive]]
3. [[Configure PhotoPrism to use an external drive]]

[^1]: [[Installing PhotoPrism on your Pi]]