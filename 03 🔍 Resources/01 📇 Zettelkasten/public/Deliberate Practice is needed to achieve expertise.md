---
date created: Monday, July 10th 2023, 8:41:23 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---
> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  [[Grit is more important than talent]]
> source: [[So good they can't ignore you]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---


To develop expertise and improve in a certain field, it is not enough to put in the hours. You also have to explicitly work on your weaknesses and practice above your comfort zone. You have to push for the uncomfortable. 

Working on the easy stuff AL the time will make you hit a plateau at an intermediate level. 