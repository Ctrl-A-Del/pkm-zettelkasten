---
date created: Monday, July 10th 2023, 8:39:22 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
tags: [permanent-note, published]
sr-due: 2023-09-15
sr-interval: 51
sr-ease: 255
---
| Topic               | Source |
| ------------------- | ------ |
| [[⚙️ Productivity]] | [[Willpower]]       |

---

> Define goals as specific as possible and try to avoid conflicting goals.

When defining goals, you should try to be as specific as possible. The clearer your outcome, the more energy you put into it. If your goals are too vague, there is additional resistance because you neither know what to aim for, nor what to do about it. Consider splitting your goals into smaller chunks, that keep you motivated, instead of goals that you will never reach. But while doing that, you can also aim to go a little above what you think is doable. This can be an additional motivator to achieve this result, that is just a it better.

You also need to be cautious that your goals do not conflict with each other. This will always result in failure and frustration because it will be impossible to succeed in all of them.

- - -
## Links
1. [[Five steps of project planning]]
2. [[Defining goals is difficult]]
