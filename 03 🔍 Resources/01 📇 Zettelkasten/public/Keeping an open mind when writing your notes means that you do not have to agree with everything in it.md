---
sr-due: 2023-07-23
sr-interval: 24
sr-ease: 273
date created: Monday, July 10th 2023, 8:40:30 am
date modified: Thursday, July 27th 2023, 8:46:44 am
---

> [!INFO]-  
> topic: [[⚙️ Productivity]]  
> links:  
> - [[Confirmation Bias - We like ideas that support our point of view]] 
> - [[What is a Zettelkasten]] 
> - [[The problem of starting with a blank page]]
> - [[Prepare to throw your work away]]
> 
> source: [[06 🗃️ Archiv/03 🗄️ References/How to take smart notes]]  
> tags: #permanent-note #published  
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Your permanent notes should not have a tendency to support your ideals. Notes should be added, if they add something to the argumentation, even if you do not agree with them.

Since we are prone to confirmation bias[^1], the problem that we prefer arguments that support our ideas rather than disagreeing perspectives, we have to remind ourselves to consider arguments against our own view. 

To avoid falling into this trap while writing, you should write your text *bottom-up* instead of *top-down*[^2]. The traditional way of first deciding on a topic and then seeking arguments for this idea and writing them down results in a strong tendency to support your thesis. However, finding arguments against it results in a much more interesting discussion.

Writing bottom-up means starting with your notes without judgement. Avoid having a pro and cons list in your mind. Arguments are not for or against your idea. They are only relevant or irrelevant.

To support this, your Zettelkasten[^3] should also not judge ideas by their tendency. The permanent notes[^4] should not relate to your personal projects or ideas. A note should be added, if it adds something to the discussion. In this regard, disagreeing information makes superb connections and makes it easy to create a discussion.

---
[^1]: [[Confirmation Bias - We like ideas that support our point of view]]
[^2]: [[The problem of starting with a blank page]]
[^3]: [[What is a Zettelkasten]]
[^4]: [[What types of notes belong to a Zettelkasten]]
