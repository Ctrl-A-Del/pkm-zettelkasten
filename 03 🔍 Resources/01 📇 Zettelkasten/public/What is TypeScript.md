---
date created: Monday, July 10th 2023, 8:41:05 am
date modified: Wednesday, July 19th 2023, 1:20:37 pm
tags: [permanent-note, published]
sr-due: 2023-08-22
sr-interval: 27
sr-ease: 299
---
| Topic             | Source |
| ----------------- | ------ |
| [[🦒 TypeScript]] | TypeScript course by the native web       |

---

> TypeScript is an addition to JavaScript that adds a type system to it.

TypeScript introduces a static type system to JavaScript and uses a pre-compiler that handles the backwards compatibility and compiles the code into vanilla JavaScript code for the browser.

It is open-source and has a huge community, making it the new industry standard.

TypeScript is backwards compatible, meaning any valid JavaScript code as also valid TypeScript code. This makes the transition in existing projects straightforward because you don't have to rework everything but can do this bit by bit.

- - -
## Links
1. [[Pros and cons of React]]
