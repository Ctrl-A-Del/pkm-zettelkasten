---
sr-due: 2023-10-03
sr-interval: 83
sr-ease: 290
date created: Monday, July 10th 2023, 8:41:45 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[🖥️ Tech]]
> links: 
> source: https://devconnected.com/cron-jobs-and-crontab-on-linux-explained/
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---



Cron is the short form for *command run on notice*. It is a task which is run automatically at a specific time, at a specified interval. This can range from simple shell commands to complex scripts.

Here is a short guide how to work with cronjobs:

edit cronjobs
```bash
$ crontab -e
```

list cronjobs (printing cron file)
```bash
$ crontab -l
```

edit cronjobs for specific user (in this case user www-data)
```bash
crontab -e -u www-data
```

Syntax for cronjobs:
```bash
# update all packages
0 3 * * * sudo apt upgrade && sudo apt -y upgrade
```

Structure:
```bash
.---------------- minute (0 - 59)
| .-------------- hour (0 - 23)
| | .------------ day of month (1 - 31)
| | | .---------- month (1 - 12) OR jan,feb,mar ...
| | | | .-------- day of week (0 - 6) (Sunday=0 or 7)
| | | | |
 * * * * * command to be executed
```