---
sr-due: 2023-09-30
sr-interval: 81
sr-ease: 310
date created: Monday, July 10th 2023, 8:39:57 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  [[What is a Zettelkasten]]
> source: [How To Take Smart Notes (3 methods no one's talking about) - YouTube](https://www.youtube.com/watch?v=5O46Rqh5zHE)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p>

---

> When connecting the notes of your [[What is a Zettelkasten|Zettelkasten]], you can use this compass as a guideline, to find connections.

When connecting the notes of your [[What is a Zettelkasten|Zettelkasten]], you can use this compass as a guideline, to find connections
![[Screenshot 2023-04-24 at 10.18.23.png]]
