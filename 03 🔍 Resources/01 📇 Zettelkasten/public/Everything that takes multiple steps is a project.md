---
date created: Monday, July 10th 2023, 8:40:31 am
date modified: Wednesday, July 19th 2023, 12:35:34 pm
tags: [permanent-note, published]
sr-due: 2023-08-29
sr-interval: 32
sr-ease: 276
---
| Topic               | Source |
| ------------------- | ------ |
| [[⚙️ Productivity]] | [[Getting things done]]       |

---

> Defining everything that requires multiple actions as a project helps to organize all the things you need to do, what outcome you expect and what actions to take.

You should treat everything that has multiple steps to be fulfilled as a project. This means that many things are project, that we usually wouldn't define this way. But this definition allows us to create a list of loose ends and define a desired outcome and next steps that need to be taken.

Separating the terms *project* and *task* allows us to identify all steps that have to be taken for a project to be fulfilled and organize those tasks in their needed context.

*Change broken light bulb* can be a project with the following actions:
1. check type of bulb
2. check if you still have this type of bulb in the storage
3. buy new bulbs
4. install new bulb


Defining this project allows you to always think about the next action, put it in the correct task list and regularly check in for the next action to do.

- - -
## Links
1. [[Five steps to deal with work]]
