---
sr-due: 2023-07-26
sr-interval: 27
sr-ease: 250
date created: Monday, July 10th 2023, 8:40:35 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
tags:
  - permanent-note
  - published
sr-due: 2023-12-04
sr-interval: 123
sr-ease: 270
---
| Topic               | Source |
| ------------------- | ------ |
| [[⚙️ Productivity]] | [[Essentialism - The Disciplined Pursuit of Less]]        |

---

> If you don't set aside time for your priority, this time will fill up with other tasks.

It is your job to define which task you have to do. It is your responsibility to make room for the things, that matter most to you. If you don't make room for your most important projects, you will never have the time. Other people will fill up your time easily with other tasks. Defend the time you need for your priority.

- - -
## Links
1. [[There is no plural for priority]]
2. [[Responsibility of choice]]
