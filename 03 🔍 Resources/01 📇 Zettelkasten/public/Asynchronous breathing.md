---
date created: Monday, July 10th 2023, 8:41:45 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
tags: [permanent-note, published]
sr-due: 2023-08-08
sr-interval: 21
sr-ease: 296
---
| Topic         | Source |
| ------------- | ------ |
| [[🩺 Health]] | [[Atmen - heilt - enntspannt - zentriert]]       |

---


By emphasizing the exhalation you have a more relaxing way of breathing. For example breathe 4 seconds in and 5 seconds out. 

- - -
## Links
1. [[Breathing in the resonance frequency]]
