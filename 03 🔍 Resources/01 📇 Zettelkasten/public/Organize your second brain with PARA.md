---
date created: Monday, July 10th 2023, 8:40:55 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---
> [!INFO]- 
> topic: [[📚 Personal Knowledge Management]]
> links:  
> - [[CODE Framework second brain]]
> - [[Everything that takes multiple steps is a project]]
>
> source: [[Building a Second Brain]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> The PARA system is a framework for organizing and managing your **digital information**. You have a hierarchy of projects, areas, resources, and archives.


The PARA system, developed by Tiago Forte, is a framework to help organize and manage your second brain effectively. Your second brain refers to the collection of digital and analog tools you use to store and manage information, ideas, and resources.

PARA stands for **Projects, Areas, Resources, and Archives** - four categories that encompass different types of information and tasks. Here's a breakdown of each category:

1. **Projects**: This category includes any active or ongoing endeavors that have multiple steps or actions involved. It could be work-related projects, personal goals, or anything that requires planning and execution. Each project should have clear objectives and tasks associated with it. A project should always have a clear beginning and an end.

2. **Areas**: Areas represent broader aspects of your life or work that require ongoing attention, but don't have specific end goals or defined outcomes. For example, areas may include areas such as health, finances, education, career development, etc. These are the major focus areas where you want to maintain progress over time. In contrast to projects, there is no clear end time.

3. **Resources**: Resources refer to reference materials or information that you collect for future use. It can include articles, books, research papers, podcasts, videos, websites – anything that provides valuable insights or knowledge in your field of interest. These resources can be organized in a way that makes them easily accessible when needed. This is basically a fallback for everything that doesn't fit into the first two categories.

4. **Archives**: Archives consist of completed projects or areas that no longer require active attention, but still hold valuable information for future reference. Archived items may include completed reports, past presentations, old client projects – anything you want to keep for historical purposes but don't need immediate access to.

The PARA system emphasizes maintaining clear boundaries between these categories and keeping them separate from each other. By doing so:

1. You can focus on specific projects without getting overwhelmed by unrelated materials.
2. You can easily track progress within different areas of your life.
3. You can quickly retrieve relevant resources when needed.
4. You can declutter your active workspace by archiving completed projects.