---
sr-due: 2023-07-22
sr-interval: 24
sr-ease: 254
date created: Monday, July 10th 2023, 8:41:12 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[🖥️ Tech]]
> links:  
> - [[Dynamic Website]]
> - [[Single Page Application]]
> source: 
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> On a static website, the server sends one view to the client per request. There are no interactive elements on the website.

On a static website, the server sends one view to the client per request. There are no interactive elements on the website.
This is the classic approach of web development and is still viable right now for simple websites, that focuses to present information. The server renders every page.