---
date created: Tuesday, July 11th 2023, 3:05:23 pm
date modified: Tuesday, July 18th 2023, 11:14:57 am
tags: [permanent-note, published]
sr-due: 2023-10-15
sr-interval: 89
sr-ease: 310
---
| Topic               | Source |
| ------------------- | ------ |
| [[⚙️ Productivity]] | [[Atmen - heilt - enntspannt - zentriert]]       |

---

> To feel more energetic, take a pause after the inhalation. 

By turning the breathing triangle around, you can feel more awake and energized.  
To achieve this, you take the pause after the inhalation instead of the exhalation. 

This means to breathe in a rhythm of 3 - 3 - 3 or 4 - 4 - 4 in this way:

Inhale 4 seconds, pause 4 seconds, exhale 4 seconds. 

In the beginning you should only do this practice for about 2 minutes. 

- - - 
## Links
1. [[Breathing triangle]]
