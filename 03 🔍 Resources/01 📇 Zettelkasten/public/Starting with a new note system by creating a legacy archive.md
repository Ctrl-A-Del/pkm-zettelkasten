---
date created: Wednesday, July 12th 2023, 8:59:23 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
tags: [permanent-note, published]
sr-due: 2023-08-13
sr-interval: 26
sr-ease: 290
---
| Topic                                | Source |
| ------------------------------------ | ------ |
| [[📚 Personal Knowledge Management]] | [[Building a Second Brain]]       |

---

> When a ton of old notes is keeping you away from starting with PARA, move everything into a legacy folder and start fresh.

If you have trouble starting PARA and have a huge chunk of notes that you have to organize beforehand, just create a legacy folder. Name it with the current date and move everything in there. Put it in the archive, and that's basically it. Start fresh from day one. 

> [!TIP]
> You can do the same with your E-mail when trying to creating a clean inbox.

- - -
## Links
1. [[Organize your second brain with PARA]]
