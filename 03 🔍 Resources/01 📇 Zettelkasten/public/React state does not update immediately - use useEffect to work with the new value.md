---
date created: Monday, July 10th 2023, 8:41:39 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
tags: [permanent-note, published]
sr-due: 2023-09-26
sr-interval: 70
sr-ease: 270
---
| Topic        | Source |
| ------------ | ------ |
| [[🧩 React]] | [Top 6 React Hook Mistakes Beginners Make - YouTube](https://www.youtube.com/watch?v=GGo3MVBFr1A&t=160s)       |

---

> If you want to work with a state variable after it is updated, use `useEffect`.

If you want to do something after a state variable changes, you can't do immediately afterwards. `useState` happens asynchronous, so you can't use the updated value in the next line. 

Instead, you need to use `useEffect` to wait for the re-render.

You can pass in a dependency into the `useEffect` to tell React, what state variables it should watch. 

```js
useEffect(() => {
	console.log(count);
}, [count]); // <-- this is an array of dependencies for useEffect
```

- - -
## Links
1. [[Use the function version of react hooks if your new state depends on the previous state]]
