---
sr-due: 2023-10-08
sr-interval: 89
sr-ease: 310
date created: Monday, July 10th 2023, 8:41:01 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic:[[⚔️ Role-playing]]
> links:  
> - [[Keep your scene descriptions short]]
> - [[You only have control over a game at the beginning of a session]]
> - [[Keeping an open mind when writing your notes means that you do not have to agree with everything in it]]
>
> source: [[Return of the Lazy Dungeon Master]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> If something you prepared doesn't fit into the game anymore, be okay with that.

Go with the flow of the game. If something you prepared doesn't fit into the game anymore, be okay with that. The key to this is to not over prepare and get too attached to an idea. None of the scenes you prepared actually happen, until you actually use them. 

Let your players take the stage. 