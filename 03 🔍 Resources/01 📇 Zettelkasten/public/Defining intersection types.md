---
sr-due: 2023-08-23
sr-interval: 43
sr-ease: 290
date created: Monday, July 10th 2023, 8:41:29 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[🦒 TypeScript]]
> links:  [[Defining union types]]
> source: TypeScript course by the native web
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

Analog to union types, you can merge types together with `&`.

```ts
interface CanBreathe {
  breathe: () => void;
}

interface CanTalk {
  talk: () => void;
}

const foo = function (human: CanBreathe & CanTalk): void {
  human.breathe();
  human.talk();
};

export {};

```