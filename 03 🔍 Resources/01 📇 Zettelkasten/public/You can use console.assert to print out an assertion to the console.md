---
sr-due: 2023-09-15
sr-interval: 67
sr-ease: 270
date created: Thursday, July 6th 2023, 5:22:34 pm
date modified: Sunday, July 16th 2023, 9:11:03 pm
tags: [permanent-note, published]
sr-due: 2024-03-29
sr-interval: 253
sr-ease: 290
---
| Topic       | Source |
| ----------- | ------ |
| [[🖥️ Tech]] | [Tips and Tricks for Debugging JavaScript - YouTube](https://youtu.be/_QtUGdaCb1c?t=295)       |

---

Instead of using `console.log`, you can print out an assertion with `console.assert`. The first parameter is the assertion and the second one is the message you want to print as an error (similar to `console.error`, if the assertion failed.

```js
console.assert(value !== undefined, "This shouldn't be undefined");
```

- - -
## Links
1. [[Use console.table to print out huge objects into the console]]
2. [[Use console.dir for logging html elements]]
