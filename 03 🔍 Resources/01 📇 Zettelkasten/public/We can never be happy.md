---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
---

| Topic             | Source                         |
| ----------------- | ------------------------------ |
| [[🧠 Psychology]] | [[Manson-Everything Is Fcked]] |

---

> No matter how great we feel, we will never be satisfied.

If you ask people to rate their current live on a scale from 1 to 10, on average, they will rate it a 7. 
Even if something really great or terrible happens, it will be another number for a short period of time, but eventually it will go back to being a 7. So on average, we are *doing fine*. 

But this also means that you can never be fully happy. Your life will never be a 10, no matter what happens. This is just how our brain works. But we will try to get to the 10 no matter what. 

This can result in a never-ending cycle of unhappiness and leave you unsatisfied, always aiming for this next great thing. While this can be motivating, it is also a trap to always be unhappy.

Nevertheless there is also light: we can get back up even from the worst setbacks in the long term. Always remember that.

- - -
## Links
1. [[You need to gain twice as much as you gain]]
2. [[The Hand we are dealt]]
