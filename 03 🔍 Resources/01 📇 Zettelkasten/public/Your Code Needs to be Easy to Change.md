---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: TSesundayAug,st Jsuly 16th5 33242023, 9:10:39 pm
tags: [permanent-note, published]
sr-due: 2023-09-06
sr-interval: 36
sr-ease: 292
---
| Topic             | Source |
| ----------------- | ------ |
| [[🧹 Clean Code]] | [[The Pragmatic Programmer]]       |

---
There is one single core principle to Clean Code: **It needs to be easy to change**. No matter which other principle or pattern you come across, it generally aims to make your code easy to change. This is why the Single Responsibility Pattern is good, why encapsulation is great, and so on. . This is the most important thing when coding. 


- - -
## Links
1. [[SOLID Clean code in React]]
2. [[Don't repeat yourself - keep your code DRY]]
3. [[Keep Your Application Orthogonal]]
