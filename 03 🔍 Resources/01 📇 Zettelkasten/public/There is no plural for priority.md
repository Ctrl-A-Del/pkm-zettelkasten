---
sr-due: 2023-09-04
sr-interval: 60
sr-ease: 290
date created: Monday, July 10th 2023, 8:41:45 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  [[Organizing your tasks]]
> source: [[Essentialism - The Disciplined Pursuit of Less]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> You should only have on priority at a time.

Historically, the word priority didn't have a plural. The word was meant to define the single most important task. Only in the 19th century, society introduced the plural version. 

However, having multiple priorities just weakens its meaning. Having five top priorities actually means you are having none.