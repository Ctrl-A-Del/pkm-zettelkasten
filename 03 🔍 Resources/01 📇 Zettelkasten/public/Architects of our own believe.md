---
date created: Monday, July 10th 2023, 8:39:20 am
date modified: Tuesday, July 18th 2023, 1:44:56 pm
tags: [permanent-note, published]
sr-due: 2023-09-24
sr-interval: 54
sr-ease: 295
---
| Topic             | Source |
| ----------------- | ------ |
| [[🧠 Psychology]] | [[The subtle art of not giving a fuck]]       |

---

> Our beliefs are formed by our experiences. Since everybody makes different experiences, everybody has different beliefs. Your beliefs are not universal.

Our beliefs are formed by our own experiences. However, everybody has experienced different events and lived a different life. Because of this, everybody has different beliefs and expectations. 

However, we tend to think that our own perspective on something is universal, even though this is not the case. **Our brain is spectacular at finding meaning in something unimportant.**

If something bothers us, this doesn't mean that it bothers anybody or that they did this to you deliberately to anger you. This is only **your** perspective. The others probably have no idea how you feel.

We have to remind ourselves that our beliefs are not universal, but our own perspective on things.

- - -
## Links
1. [[Nobody is special]] 
2. [[The Hand we are dealt]]
