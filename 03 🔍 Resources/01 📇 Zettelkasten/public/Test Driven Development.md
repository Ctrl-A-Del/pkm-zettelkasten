---
date created: Monday, July 10th 2023, 8:41:01 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---
> [!INFO]- 
> topic: [[🖥️ Tech]]
> links:  
> - [[Advantages and disadvantages of Microservices]]
> - [[Continuous Integration]]
> source: [Test-Driven Development (TDD) // deutsch - YouTube](https://www.youtube.com/watch?v=71nLhdZuMk0)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> In TDD, you write the tests before writing the code.

In TDD, you write the test for some code before you even write the specific code. This means, that the test will definitely fail initially. The goal now is to write the code to make this tests successful.

This forces you to write and think in tests. You start by considering all possible outcomes first and then focus on fixing each of this issues one after another. The code you write this way will also be easily testable because you wrote it that way. If you first start coding and write the tests afterwards, it is often the case that it is much more difficult to write tests because the code isn't structured appropriately.