---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
---
| Topic       | Source |
| ----------- | ------ |
| [[🖥️ Tech]] | [Duplicati](https://www.duplicati.com/)        |

---

> [Duplicati](https://www.duplicati.com/) is a free and open-source tool to automatically create encrypted backups and save them to the cloud service of your choice.

It supports all common cloud storage providers and also a lot of manual protocols to handle e.g. your NAS. It can work on a schedule (or at the next available time when your computer was turned off).

You can configure, if the backups should last forever or if backups of a certain age should be deleted to save some storage.

It reminds me a bit of [Cryptomator](https://cryptomator.org/de/), which is an encryption service specifically designed for cloud services. But Duplicati also handles the automatic upload.

I am using it in combination with [Syncthing](https://syncthing.net/) to have nice FOSS solution for synchronization and backup.

- - -
## Links
1. [[Unlinked]]
