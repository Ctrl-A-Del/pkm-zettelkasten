---
date created: Monday, July 10th 2023, 8:40:18 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---
> [!INFO]- 
> topic: [[👪 Parenting]]
> links: 
> source: [ScratchJr - Home](https://www.scratchjr.org/)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p>

---

This is a tool for children to learn some basic programming concepts.
