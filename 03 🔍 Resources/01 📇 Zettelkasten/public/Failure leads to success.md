---
sr-due: 2023-07-22
sr-interval: 25
sr-ease: 272
date created: Monday, July 10th 2023, 8:40:52 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🧠 Psychology]]
> links:  
> - [[Going into any direction is better than standing still]] 
> - [[It's never too late to change your direction]]
> - [[Failure-success paradox]]
> - [[Prepare to throw your work away]]
>
> source: [[The Art of Work]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Failure isn't what prevents us from success, it leads us there. 

It may take you several tries to find your true passion. You might think, that you already found it, just to realize that you went into the wrong direction. If you fail at something, try to listen if this is the right direction. 

But don't see your failures as just a failure. Every step is there to prepare you for your true calling. 