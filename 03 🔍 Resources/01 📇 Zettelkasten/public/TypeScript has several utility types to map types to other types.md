---
date created: Monday, July 10th 2023, 8:41:28 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
tags: [permanent-note, published]
sr-due: 2023-09-28
sr-interval: 65
sr-ease: 250
---
| Topic             | Source |
| ----------------- | ------ |
| [[🦒 TypeScript]] | [TypeScript: Documentation - Utility Types](https://www.typescriptlang.org/docs/handbook/utility-types.html#recordkeys-type)        |

---

When you want to build a type that somehow depends on another, already existing, type, there are several utility types that can help you to build subsets with specific behaviors.

For example, you can use `Partial<Type>` to create a new type, where all properties of `Type` are optional, `Readonly<Type>` to make all properties `readonly`, `Record<Type, Keys>` to create a dictionary of the Type and define the desired keys, or `Pick<Type, Keys>` to create a subset of another type, only consisting of the given types.

There are many more, that can be looked up in the [TypeScript: Documentation - Utility Types](https://www.typescriptlang.org/docs/handbook/utility-types.html#recordkeys-type).

```ts
interface Person {
  firstName: string;
  lastName: string;
  age: number;
}

const getPerson = function (): Readonly<Person> {
  const person: Partial<Person> = {
    firstName: 'Jane',
    lastName: 'Doe'
  };

  person.age = 37;

  return person as Readonly<Person>;
};

const person = getPerson();

console.log(person.firstName);

type PersonCommandingRespect = Pick<Person, 'lastName' | 'age'>
const personCommandingRespect: PersonCommandingRespect = {
  lastName: 'Scully',
  age: 30
};



type PersonCommandingRespect2 = Omit<Person, 'firstName'>
const personCommandingRespect2: PersonCommandingRespect2 = {
  lastName: 'Scully',
  age: 30
};
```


- - -
## Links
1. [[Defining union types]]
