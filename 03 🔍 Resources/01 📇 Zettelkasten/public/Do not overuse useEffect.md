---
date created: Monday, July 10th 2023, 8:40:10 am
date modified: Friday, July 28th 2023, 9:00:13 am
tags:
  - permanent-note
  - published
sr-due: 2023-08-15
sr-interval: 18
sr-ease: 273
---
| Topic        | Source |
| ------------ | ------ |
| [[🧩 React]] |   [Top 6 React Hook Mistakes Beginners Make - YouTube](https://www.youtube.com/watch?v=GGo3MVBFr1A&t=160s)      |

---

> You don't always need a `useEffect` to update values after an update.

`useEffect` triggers a complete re-render of the component. Pay attention when you really needs the re-render, and when a simple `const` could also do the trick.

Take a look at this example:
```jsx
const [firstname, setFirstname] = useState("");
const [lastname, setLastname] = useState("");
const fullname, setFullname = useState("");

useEffect (() => {
	setFullname(`${firstname} ${lastname}`)
}, [firstname, lastname]);
```

This updates the full name every time, either the first, or the last name changes. So the update of the first name triggers a re-render, and then `useEffect` triggers another one. Since the first `useState` already causes a re-render, we can simply update a `const` instead 

```jsx
const [firstname, setFirstname] = useState("");
const [lastname, setLastname] = useState("");
const fullname = `${firstname} ${lastname}`
```

- - -
## Links
1. [[Pros and cons of React]]