---
date created: Monday, July 10th 2023, 8:40:10 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---
> [!INFO]- 
> topic: [[🖥️ Tech]]
> links:  [[Use an external SSD for your PhotoPrism instance]]
> source: 
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> This post explains how to configure your own PhotoPrism instance to use an external hard drive.

To use an external drive in [[Hosting your own image gallery|Photoprism]], you need to first [[RaspberryPi mount external drive|mount the drive]] and configure it to [[RaspberryPi automount external drive|mount automatically]].

Afterwards, you can configure PhotoPrism accordingly.

1. Make sure the drive is mounted correctly, by running
```shell
sudo lsblk -o UUID,NAME,FSTYPE,SIZE,MOUNTPOINT,LABEL,MODEL
```
- you should see the mount point
1. edit the PhotoPrism configuration:
```shell
sudo vi docker-compose.yml
```
2. under `volumes`, edit the originals and storage destination:
```yaml
  - "/mnt/photoprism/originals:/photoprism/originals"
  - "/mnt/photoprism/storage:/photoprism/storage"
```
3. you can also specify subfolders as mentioned in the comments of the file