---
sr-due: 2023-09-09
sr-interval: 65
sr-ease: 292
date created: Monday, July 10th 2023, 8:39:43 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  
> - [[The problem of starting with a blank page]] 
> - [[Getting better each day - the compounding effect of the one percent rule]]
> - [[Why I write my notes in English]]
>
> source: [[06 🗃️ Archiv/03 🗄️ References/How to take smart notes]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p>  

---


> Don't try to remember everything. It unnecessarily clouds your mind. Just write every idea, every to-do down. The knowledge that you can tend to it later can be as fulfilling as actually doing it. 

Write everything down, that seems important to you. 

Have an interesting idea that you want to remember later? Write it down. 

Asked yourself a question about how something works? Write it down, and research later. 

Have a to-do that you need to remember later? Write it down. 

Learned something interesting in a book, podcast or video? Write it down. 

How often did you learn something interesting, but forgot most of it a week later? 

Keeping stuff in our head occupies it with a lot of information. If we think about something, we either want to do it now, or we forget about it because there was too much other stuff that we wanted to remember. 

In general, you can remember about 7 +/- 2 things at a time. 

But if you write something down and get it out of your system, this has the same effect as actually doing it. You feel a sense of accomplishment because it creates a feeling of doing it later. 

You should make a habit out of this. It doesn't matter if the thing you write down is unimportant. You can review and maybe trash it later. But writing it down in the first place gives you the opportunity to do that and not forget about half of your ideas and to-dos. 

This is a small habit that boosts your productivity by a lot. 

In the Zettelkasten, a good place to write everything down are your [[What types of notes belong to a Zettelkasten#Fleeting notes|Fleeting notes]]. 

> Your mind is for having ideas, not storing them. 
> - - - David Allen, Getting things done