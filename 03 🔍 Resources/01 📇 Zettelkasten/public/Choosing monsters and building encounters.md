---
sr-due: 2023-07-20
sr-interval: 23
sr-ease: 299
date created: Monday, July 10th 2023, 8:40:16 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic:[[⚔️ Role-playing]]
> links:  [[Prepare to throw your work away]]
> source: [[Return of the Lazy Dungeon Master]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> When choosing monsters for an encounter, consider the environment and do some research on it. 

Think about the monsters you want to have in your next session. Consider which enemies make sense in the environment and at the locations your players will go. Don't bother too much about the difficulty of the encounter. You can use choose the number of monsters that make sense at the location. Take your time to read about the monsters you're planing to use. This allows you to play then better and make each encounter feel unique. 