---
date created: Monday, July 10th 2023, 8:41:11 am
date modified: Tuesday, July 18th 2023, 11:23:10 am
tags: [permanent-note, published]
sr-due: 2023-10-16
sr-interval: 90
sr-ease: 322
---
| Topic               | Source |
| ------------------- | ------ |
| [[⚔️ Role-playing]] | [[Return of the Lazy Dungeon Master]]       |

---

> Create a strong start for your session to grab the attention. 

You never know what your players are going to do. This is why it's not useful to over prepare. Most of your plans will be crossed by the players anyway. This is why you can focus on having a strong start. You still have the reins. Use them. 

- - -
## Links
1. [[Players want their characters to do awesome things]]
2. [[Prepare to throw your work away]]