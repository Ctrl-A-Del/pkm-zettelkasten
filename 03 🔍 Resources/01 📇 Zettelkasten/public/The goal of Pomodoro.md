---
date created: Monday, July 10th 2023, 8:39:26 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---
> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  [[Engaging on your next action]]
> source: [The proper way to use the Pomodoro Method (How to Increase Focus) - YouTube](https://www.youtube.com/watch?v=rnVFwPRlrsw) 
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> 25 minutes of real focused work, 5 minute break.

The Pomodoro technique describes a specific way to work and take breaks. The classic Pomodoro suggests 25 minutes of work and 5 minutes break. Sometimes with some longer break after a couple of sessions. 
The idea is to really get into the flow in these 25 minutes and avoid any distrection, because we need to get into a focus to really be productive.

To use this technique, don't be too strict with these time limits. If you are in the flow, it might be a good idea to do *overtime* and use this energy. You can add this break time to a later break. On the other hand, don't force yourself to the finish line if you lost focus. In that case start your break early and try to refocus later.

Also you should always start a session with a clear goal in mind and don't multitask. Set a task for this session and focus on that.

