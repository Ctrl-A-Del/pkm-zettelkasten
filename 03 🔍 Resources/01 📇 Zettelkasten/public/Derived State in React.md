---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
---
| Topic        | Source |
| ------------ | ------ |
| [[🧩 React]] | [React - Udemy](https://www.udemy.com/course/react-the-complete-guide-incl-redux/learn/lecture/38345450#overview)       |

---

If you have a variable that is dependent on another state variable, you don't need to create another state variable for this. It is sufficient, to create a normal local variable `let` or `const` for this.

The whole function component gets re-rendered anyway, when the state gets updated, so the local variable will get computed again as well.

- - -
## Links
1. [[Lifting the state up]]
2. [[React state does not update immediately - use useEffect to work with the new value]]
