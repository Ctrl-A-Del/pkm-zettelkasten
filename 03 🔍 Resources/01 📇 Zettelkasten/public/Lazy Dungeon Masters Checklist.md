---
sr-due: 2023-09-08
sr-interval: 64
sr-ease: 310
date created: Monday, July 10th 2023, 8:39:50 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[⚔️ Role-playing]]
> source: [[Return of the Lazy Dungeon Master]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

## 1. Review the Characters
[[Players want their characters to do awesome things]]

## 2. Create a Strong Start
[[You only have control over a game at the beginning of a session]]

## 3. Outline Potential Scenes
[[Keep your scene descriptions short]]

## 4. Define Secrets & Clues
[[Secrets and clues are the glue of your game]]

## 5. Develop Fantastic Locations
[[Preparing fantastic locations]]

## 6. Outline Important NPCs
[[Creating characters on the fly]]

## 7. Choose Relevant Monsters
[[Choosing monsters and building encounters]]

## 8. Select Magic Item Rewards
[[The Importance of Magic Items]]
