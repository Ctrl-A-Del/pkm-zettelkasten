---
sr-due: 2023-09-20
sr-interval: 73
sr-ease: 310
date created: Monday, July 10th 2023, 8:40:14 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  
> source: [Bildungsurlaub NRW — Weiterbildungsberatung NRW](https://www.weiterbildungsberatung.nrw/finanzierung/bildungsurlaub-nrw)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> You can take days off to further educate yourself.

In Germany, NRW if you belong more than 6 months to a company with more than 10 employees, you are allowed to take 5 days paid vacation each year to study. To achieve this, you have to tell this your employer 6 weeks in advance, and you have to prove the training you are doing.