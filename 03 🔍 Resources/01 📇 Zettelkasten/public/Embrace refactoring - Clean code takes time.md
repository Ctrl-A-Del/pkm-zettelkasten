---
date created: Monday, July 10th 2023, 8:40:59 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---
> [!INFO]- 
> topic: [[🧹 Clean Code]]
> links:  [[Clean code - write your code like an author writes a story]]
> source: [Clean Code | Udemy](https://www.udemy.com/course/writing-clean-code/learn/lecture/23111112#overview)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

When trying to write clean code, this is always a process. You write feature A, then you start feature B and suddenly realize that you can adjust something in A and so on. This is a totally normal process. Embrace Refactoring.

Writing clean code will help you eventually. While dirty code is written down quicker, debugging, maintaining and even understanding it will take much more time. So while clean code takes more time at first, this will pay off later down the line.

> Refactoring today saves you work tomorrow

![[Pasted image 20230621183325.png]]