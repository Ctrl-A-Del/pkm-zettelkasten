---
sr-due: 2023-08-21
sr-interval: 46
sr-ease: 270
date created: Monday, July 10th 2023, 8:39:43 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[🖥️ Tech]]
> links:  
> source: https://koalatea.io/how-to-update-all-your-npm-packages-at-once/ 
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Keeping your npm packages is very important, but can be a hassle. Here are some solution to approach this.

Keeping your [npm](https://www.npmjs.com/) packages up to date is a crucial task, since old packages can result in a security risk. However, updating them can be a hassle. There are several ways to get the latest version for your packages.

## The Safe way
The simplest way is to use the native way of npm:

```sh
$ npm update --save
$ npm update --save-dev
```

This will update all packages to the most recent *minor* version. This shouldn't result in any problems, since minor version updates do not include any breaking changes.

## The less Safe way
You can use another npm package to update your other packages. The package we are using is called [npm-check](https://www.npmjs.com/package/npm-check). It will allow you to interactively choose which packages to update.

To use it, you can install it globally:

```sh
$ npm install npm-check -g
```

Afterwards, you can use the `npm-check` command, which will give you the option to choose which packages to update.

## The Manual way
If you want to be extra careful, it might be a good idea to update your packages manually. In that case, you can use `npm outdated` to see which packages need updates. This also provides you information about the latest version and the latest possible minor version you can update to.

You can then manually update each package with `npm update [package-name] --save`. 

Another way to check which packages to update is `npm audit`. This will show you all issues and vulnerabilities you are exposed to from your npm packages and which version resolves the issue. You can try to fix them automatically by using `npm audit fix`. 
If you want to see only the *critical* issues, you can use this command to only print the critical issues: `npm audit | grep -B 3 -A 15 Critical`

### Why Should I want to Do All This Manually?
While it seems like a hassle to do this manually instead of using the automated ways above, this allows you to update the packages one by one. This way, you can test if the new version of a specific package introduces issues or bugs.

> It might be a good idea, to create a commit for each updated package.
If you find an issue later, it is easier to find out which packages are responsible via `git bisect`.

