---
sr-due: 2023-07-20
sr-interval: 22
sr-ease: 270
date created: Monday, July 10th 2023, 8:41:03 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
tags: [permanent-note, published]
---
| Topic       | Source |
| ----------- | ------ |
| [[🖥️ Tech]] |   [Top 25 Microservice Interview Questions Answered - Java Brains - YouTube](https://www.youtube.com/watch?v=o36vWQCRrp0)     |

---

> A monolith software is one huge software that gets deployed as one single piece

A monolith software is one huge software that gets deployed as one single package. Often monoliths grow over time and might even use different technologies.

- - -
## Links
1. [[Advantages and disadvantages of Microservices]] 
