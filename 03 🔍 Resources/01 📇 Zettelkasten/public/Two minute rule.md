---
date created: Monday, July 10th 2023, 8:41:44 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
tags: [permanent-note, published]
sr-due: 2023-11-10
sr-interval: 110
sr-ease: 313
---
| Topic               | Source |
| ------------------- | ------ |
| [[⚙️ Productivity]] | [[Getting things done]]       |
- - - 
> If a task takes less than two minutes, just do it immediately.

When you want to decide about the next action you want to tackle, take a moment and evaluate if it will take more or less than two minutes.

If it takes less, just do it **immediately**. Queuing every minor task bloats your list with minor tasks. By doing this little actions while you work through your [[Capture Everything in an Inbox|inbox]], you can already get a lot done and feel productive.

However, two minutes is not a lot. Try to get a feeling for how much you can do in two minutes. Some phone calls can be done in that time, but many calls take longer. Learning this estimate takes time and experience.

- - -
## Links
1. [[Clarifying your tasks]] 
2. [[Five steps to deal with work]]

