---
sr-due: 2023-08-09
sr-interval: 34
sr-ease: 250
date created: Monday, July 10th 2023, 8:39:35 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🦒 TypeScript]]
> links:  
> - [[Default parameters in TypeScript]]
> - [[Optional Parameters in TypeScript]]
>
> source: TypeScript course by the native web 
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

To scale functions properly, you can add additional properties as an array that you spread with `…`. In JavaScript, this structure is called the *rest parameters*.

```ts
const add = function (left: number, right: number, ...others: number[]): number {
  let sum = left + right;

  for (const other of others) {
    sum += other;
  }

  return sum;
};

console.log(add(2, 3));
console.log(add(2, 3, 4, 5, 6, 7));
```