---
sr-due: 2023-07-31
sr-interval: 20
sr-ease: 230
date created: Monday, July 10th 2023, 8:40:55 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
tags: [permanent-note, published]
---
| Topic       | Source                                                                                                                                                                          |
| ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [[🖥️ Tech]] | [What are Microservices? - YouTube](https://www.youtube.com/watch?v=j3XufmvEMiM) [Was ist ein Microservice? ( Deutsch ) - YouTube](https://www.youtube.com/watch?v=QUdELxSCVEY) |
|             | [What are microservices really all about? - Microservices Basics Tutorial - YouTube](https://www.youtube.com/watch?v=j1gU2oGFayY)                                               |
|             |  [Top 25 Microservice Interview Questions Answered - Java Brains - YouTube](https://www.youtube.com/watch?v=o36vWQCRrp0)                                                                                                                                                                                   |

---

> The goal of a microservice architecture is to create small services, that serve exactly one purpose.

The microservice architecture tries to move away from big monolithic software. The goal is to split your application into small services, that each serve exactly one purpose. This allows you to incrementally develop each service on its own without the need to change the rest. To achieve this, you deploy each of this services on its own with its own pipeline.

The services need to communicate with each other (e.g. via a [[REST API]] & JSON) and work together to achieve the whole application. These APIs serve as a contract between the different services.

## Advantages
- it allows you to scale specific services independently
	- this is important, if some parts of your application are requested much more often than others
- testing the software is much easier because you only have to test the small service (as long, as the API doesn't change) the change happened in and not the whole monolith because your change might have some effects on something seemingly unrelated
- you can deploy them more flexible
- different teams can work on different services without interfering with the other software
- you can easily mix different technologies for the different services

## Disadvantages
- you have to consider how to split your services
- the architecture and deployment gets more complex
- Routing: the services need to know about each other and their corresponding endpoints

- - -
## Links
1. [[Test Driven Development]] 
2. [[Monolith Software]]
