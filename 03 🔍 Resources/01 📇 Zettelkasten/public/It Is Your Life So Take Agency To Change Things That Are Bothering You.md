> [!INFO]- 
> topic: [[🏡 Life]]  
> links:   
> - [[Responsibility of choice]]
> - [[Don't Blame, Take Responsibility - Responsiblity-fault fallacy]]
>
> source: [[The Pragmatic Programmer]]  
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> It is your responsibility to change something if it is bothering you.

It is your Life.

You have agency. This means if you're unhappy with your job, don’t be stuck. Think about what you can do about it. If you struggle that your knowledge is outdated and your company doesn't educate you, don’t just complain. Find some time in your leisure time and invest in yourself. You might be angry that you have to do this on your own time, but since you're investing in yourself, it is still a great investment. 

The important thing is that you take the control you have. 

Always remember: if there's something that is bothering you at your company, then there are two options. You can **change** your organization, or you can change your **organization**. 
