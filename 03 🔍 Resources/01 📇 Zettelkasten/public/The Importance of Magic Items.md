---
sr-due: 2023-08-24
sr-interval: 43
sr-ease: 294
date created: Monday, July 10th 2023, 8:40:01 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[⚔️ Role-playing]]
> links:  [[Players want their characters to do awesome things]]
> source: [[Return of the Lazy Dungeon Master]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Magic items are important! Don't overlook them. Instead, tie them into your story 

Never underestimate the power of giving your players magic items. They play an important part to **==make your players feel powerful and awesome==**. Ask your players what types of items they prefer. 

In addition to boosting the characters, magic items can be used to create strong connections to the story or generate Quests in their own. 