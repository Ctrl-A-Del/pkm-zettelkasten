---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
---
| Topic            | Source |
| ---------------- | ------ |
| [[👪 Parenting]] | [[Unconditional Parenting]]       |

---
There is a big misconception about parenting and a child being well-behaved.

In general, a good and well-behaved kid is a kid who sits still, doesn't annoy you. It is quiet and does do what it is told to do. But are those really the values, that you want this kid to behave when it is an adult? Do you really want to raise an adult to be quiet, not ask questions, and only does what he or she is told to do?

Or do you rather prefer a person who is mindful, asks questions, knows what it wants and is caring and empathetic instead of a quiet robot?

The problem is that most common parenting techniques that involve punishment, rewards, love-withdrawal and timeouts and such the like produce the first kind of person and not the latter one. 

The goal to achieve that result is an unconditional parenting style, where you show your child that you love it, no matter what it does.

- - -
## Links
1. [[Plea for parental leave as a father]]
2. [[Constraints in attachment parenting]]
3. [[We don't need to be perfect parents]]
