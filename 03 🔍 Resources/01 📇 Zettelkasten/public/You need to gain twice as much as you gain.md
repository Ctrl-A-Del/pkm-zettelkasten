---
sr-due: 2023-08-24
sr-interval: 49
sr-ease: 270
date created: Monday, July 10th 2023, 8:39:34 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[🧠 Psychology]]
> links:  [[Risk aversion - if we don't get a big advantage by taking a risk, we avoid it]]
> source: [[Thinking fast and slow]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> To make up for a loss emotionally, you have to make a gain that is twice as big.

The human mind hates losing, while it loves to gain. But it hates losing much more, than it likes gaining. As a rule of thumb, losing hurts twice as much as gaining. 

To mentally compensate a 50€ loss, you have to gain 100€, just to *feel* equal again. 

This effect exists because bad things result in a stronger emotion, than good ones.

>  A cockroach will be awful in a bowl of cherries. But a cherry doesn't help a bowl of cockroaches.

The concept of loss aversion is closely related to [[Risk aversion - if we don't get a big advantage by taking a risk, we avoid it|risk version]]. The difference is that risk aversion focuses on the possible outcome of an event, while the loss aversion has an effect after the event already happened.

Take a look at the following example:

You have a 5% chance to win 1 million € or nothing for no cost at all.

Everybody would take this chance. But if we lose, we kind of feel like we lost something, even though nothing changed from before. It still leaves a negative feeling, even though you did not need to take any risk.