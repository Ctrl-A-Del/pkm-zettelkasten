---
date created: Wednesday, July 12th 2023, 10:31:26 am
date modified: Tuesday, July 18th 2023, 11:27:23 am
tags: [permanent-note, published]
sr-due: 2023-08-12
sr-interval: 25
sr-ease: 303
---

| Topic       | Source |
| ----------- | ------ |
| [[🖥️ Tech]] | [How I structure my Obsidian vault (Obsidian tour) - YouTube](https://www.youtube.com/watch?v=vAwS-js2iB0&t=930s)       |

---

> Make a habit of logging your problem-solving.

I often tried to figure out a solution, tinkering around with configs etc. and after finally fixing it, i wasn't able to tell what actually did the trick.

**When you are having a specific problem, and you're Googling around trying to find the right solution, create a log of what you have tried.** Copy the stuff you've done so that you can later track down what the solution actually was. 

This makes it easier to get a library of your solutions to common problems, which is a blessing if the same error happens twice.

- - -
## Links
1. [[Don't panic, if your service breaks over night]]
2. [[Writing every idea and every task down helps to declutter you mind]]
3. [[What is a second brain]]