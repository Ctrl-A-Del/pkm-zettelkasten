---
date created: Monday, July 10th 2023, 8:41:35 am
date modified: Thursday, July 27th 2023, 8:01:09 am
tags: [permanent-note, published]
sr-due: 2024-08-10
sr-interval: 380
sr-ease: 332
---
| Topic                                | Source |
| ------------------------------------ | ------ |
| [[📚 Personal Knowledge Management]] | me       |

---

> Writing in English improves my ability in the language and I can reuse the notes easier in my Digital Garden

Writing notes in English can help to improve my understanding of the language and makes it easier to start thinking in English.

It is also easier to reuse these notes later for my [Public Second Brain](https://notes.sebastianauner.com/).

However, since I already started in German, the transition is difficult. I should write my new notes in English. But what about topics and indices? Those will probably have a mixed mode. Still, they should probably be written in English. 

Writing my notes in English from German sources also forces me to rephrase it on my own. But most sources will be English anyway. For these, it will just be easier to write in English anyway. 

This rule is only important for my permanent notes. Other notes are not *that* important, and it is therefore not that important.

- - -
## Links
1. [[Getting better each day - the compounding effect of the one percent rule]]
2. [[What is a Zettelkasten]]
3. [[Writing every idea and every task down helps to declutter you mind]]
4. [[Write down your achievements]]
