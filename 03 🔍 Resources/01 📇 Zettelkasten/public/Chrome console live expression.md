---
date created: Jul. Th, 2023 - 16:58
date modified: Jul. Tu, 2023 - 19:25
tags: [permanent-note, published]
sr-due: 2023-08-05
sr-interval: 18
sr-ease: 294
---
| Topic | Source |
| ----- | ------ |
|  [[🖥️ Tech]]     | [Watch JavaScript values in real-time with Live Expressions - Chrome Developers](https://developer.chrome.com/docs/devtools/console/live-expressions/)       |

---

You can monitor a variable in chrome console with the console feature `Create Live Expression`. You can access it via the command palette `Cmd + Shift + P`in the developer console.

- - - 
## Links
1. [[Use console.dir for logging html elements]]
