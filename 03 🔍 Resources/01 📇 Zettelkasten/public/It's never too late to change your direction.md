---
date created: Monday, July 10th 2023, 8:41:15 am
date modified: Wednesday, July 19th 2023, 11:16:49 am
tags: [permanent-note, published]
sr-due: 2023-09-21
sr-interval: 51
sr-ease: 295
---
| Topic                                 | Source |
| ------------------------------------- | ------ |
| [[⚙️ Productivity]] [[🧠 Psychology]] | [[The Art of Work]]       |

---

> You don't have to commit to a path just because you started it. Go with your passion, even if you realize it is something, than what you did in your past. 

Finding your passion is a difficult process. You might walk into the wrong direction, realizing after years that something else is your true meaning. In that case, don't be afraid to do the leap. Commit to your new, true passion and go into that direction. **You should never feel forced to continue a path, that you don't stand behind**, just because you started it. Go where life takes you. 

- - -
## Links
1. [[Going into any direction is better than standing still]]
2. [[Failure leads to success]]
