---
date created: Monday, July 10th 2023, 8:41:21 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
tags: [permanent-note, published]
sr-due: 2023-10-14
sr-interval: 88
sr-ease: 310
---
| Topic       | Source |
| ----------- | ------ |
| [[🖥️ Tech]] | Me       |

---

> I had some issues, especially with PhotoPrism and therefore decided to try a Synology NAS.

In the past, I have been using [Nextcloud](https://nextcloud.com/) and [PhotoPrism](https://photoprism.app/) to self host my own cloud service and manage my images. However, both apps sometimes [[Don't panic, if your service breaks over night|crashed]] overnight and created a ton of work to figure out the issue and get the services back to work again. In addition, PhotoPrism didn't really work as intended. When scrolling through the image gallery, it only loaded 50 images each scroll, making it difficult to find older photos. This was probably some error on my side, but I decided to go an easier, less custom approach and got a Synology NAS. 
It works out of the box, has easy data loss protection via RAID systems and managing my photos works flawlessly with [Synology Photos](https://www.synology.com/de-de/dsm/feature/photos).

- - -
## Links
1. [[Hosting your own image gallery]]