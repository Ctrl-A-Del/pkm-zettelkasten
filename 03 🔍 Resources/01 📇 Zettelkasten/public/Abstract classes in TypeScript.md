---
date created: Monday, July 10th 2023, 8:40:39 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
tags: [permanent-note, published]
sr-due: 2023-07-23
sr-interval: 4
sr-ease: 270
---
| Topic             | Source |
| ----------------- | ------ |
| [[🦒 TypeScript]] | TypeScript course by the native web       |

---

Abstract classes are like interfaces with superpowers. The keyword `abstract` makes sure, that you can not instantiate this class. But you can give some default implementations.


```ts
abstract class Animal {
  breathe (): void { //default implementation
    console.log('In and out, and in and out, ...');
  }

  abstract hunt (): void; // no specific implementation
}

class Dog extends Animal {
  hunt (): void {
    console.log('Woof, grrr, woof!');
  }
}

class Cat extends Animal {
  hunt (): void {
    console.log('...');
  }
}

class Horse extends Animal {
  hunt (): void {
    throw new Error('Where is my grass?');
  }
}

// Compiler error, because Animal is an abstract class
// const anonymous = new Animal();

const dark = new Horse();
dark.breathe();
dark.hunt();

export {}

```

## Links
- - - 
 1. [[Interfaces in TypeScript]]
2.  [[Creating classes that implement an interface in TypeScript]]