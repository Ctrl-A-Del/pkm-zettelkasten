---
date created: Monday, July 10th 2023, 8:40:09 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---
> [!INFO]- 
> topic: [[[🦒 TypeScript]]]
> links:  [[What is TypeScript]]
> source: TypeScript course by the native web
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---
Defining dictionaries in TypeScript can be a bit complicated because the type system gets in the way. Here are some examples of how *not* to do it. 
Imagine a dictionary of country populations.

```ts
// Doesn't work, because empty object does not allow accessing properties.
let countries = {};
countries.germany = 80_000_000;

// Doesn't work, because object does not allow accessing properties.
let countries: object = {};
countries.germany = 80_000_000;

// Doesn't work, because Object does not allow accessing properties.
let countries: Object = {};
countries.germany = 80_000_000;

// Works, but is not very well extensible.
interface Countries {
  germany?: number;
  france?: number;
}
let countries: Countries = {};
countries.germany = 80_000_000;
countries.france = 66_000_000;

// Works, but accessing a property leads to always-existent values, no matter whether they have been defined or not.
interface Countries {
  [ name: string ]: number;
}
let countries: Countries = {};
countries.germany = 80_000_000;
countries.france = 66_000_000;
countries.italy = 60_000_000;
```

```ts
// so this is the way
let countries: Record<string, unknown> = {};
countries.germany = 80_000_000;
countries.france = 66_000_000;
```
