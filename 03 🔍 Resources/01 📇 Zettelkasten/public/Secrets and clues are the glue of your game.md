---
date created: Monday, July 10th 2023, 8:39:43 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
tags: [permanent-note, published]
sr-due: 2023-10-13
sr-interval: 86
sr-ease: 299
---
| Topic               | Source |
| ------------------- | ------ |
| [[⚔️ Role-playing]] | [[Return of the Lazy Dungeon Master]]       |

---

The secrets of your adventure are what engages your players. But you should write them as neutral as possible, to be able to toss them in wherever you see fit. Don't limit a secret to one specific event, make it broadly available.

- - -
## Links
1. [[Prepare to throw your work away]]
2. [[Keep your scene descriptions short]]
