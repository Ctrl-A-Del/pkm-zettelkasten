---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
---

| Topic             | Source                   |
| ----------------- | ------------------------ |
| [[🧠 Psychology]] | [[The How of Happiness]] |

---

> Make gratitude a flexible, weekly habit by adjusting its timing, focus, or format to keep it meaningful and engaging.



Frequency:
Expressing gratitude doesn’t have to be a daily practice. Weekly gratitude can be more sustainable—it avoids feeling overwhelming and doesn’t turn into a chore.

Content:
Each time, identify 3-5 things you’re genuinely grateful for.

Staying Engaged:
If the habit starts to feel boring or routine, switch it up:

Change the timing (e.g., after specific events or triggers).

Adjust the focus (e.g., different types of gratitude).

Vary the format (e.g., writing in a journal, using a phone app).



Keep experimenting to keep the practice meaningful and engaging.



- - -
## Links
1. [[Make gratitude a habit]]
2. [[Mental subtraction can help your practicing gratitude]]