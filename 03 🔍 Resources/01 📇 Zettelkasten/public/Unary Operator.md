---
date created: Sunday, July 9th 2023, 8:20:50 pm
date modified: Thursday, July 27th 2023, 8:07:04 am
tags: [permanent-note, published]
sr-due: 2024-01-17
sr-interval: 174
sr-ease: 290
---
| Topic             | Source |
| ----------------- | ------ |
| [[🧹 Clean Code]] |  [Cast to Number in Javascript using the Unary (+) Operator | by Nikhil John | Medium](https://medium.com/@nikjohn/cast-to-number-in-javascript-using-the-unary-operator-f4ca67c792ce)       |

---

The unary + operator converts its operand to Number type.

```js
let positiveOne = +1
```

You don't need to `parseInt` or `parseFloat`. 

So, to convert a string or a Boolean, or a Numeric String, you can just use `+`.

```js
+false  // 0  
+‘123’  // 123  
+0xBABE // 47806 (Hexadecimal)  
+null   // 0  
+function(val) {return val } // NaN
```


The Unary (+) can convert string representations of **integers** and **floats**, as well as the non-string values **true**, **false**, and **null**. Integers in both **decimal** and **hexadecimal** formats are supported. **Negative** numbers are supported (though not for hex). If it cannot parse a particular value, it will evaluate to [NaN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/NaN).

However, bear in mind that the Unary (+) *does not* perform well in certain cases. For example, it doesn't work as expected on empty strings, alphanumeric strings, empty objects etc.

```js
+''     // NaN  
+'123a' // NaN  
+{}     // NaN
```

- - -
## Links
1. [[Nullish Coalesning operator]] 
