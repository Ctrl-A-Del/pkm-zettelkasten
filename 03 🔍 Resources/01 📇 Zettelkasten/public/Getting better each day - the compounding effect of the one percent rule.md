---
sr-due: 2023-10-09
sr-interval: 90
sr-ease: 310
date created: Tuesday, July 11th 2023, 5:25:08 pm
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  
> - [[The Habit Loop]] 
> - [[Why I write my notes in English]]
> source: [[Atomic Habits]] [Marginal Gains: This Coach Improved Every Tiny Thing by 1 Percent](https://jamesclear.com/marginal-gains)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Don't underestimate small changes. If you try to improve by one percent each day, this will accumulate to a 37% improvement over one year. 

Don't underestimate the importance of tiny improvements. Even the smallest improvements are always worth it. The goal is to compound all of these tiny improvements into a big one. If you just try to be one percent better every day, this will accumulate to 37% in a year. 

Initially, it doesn't make a difference if you follow your plan or don't because the improvement is not visible. But over time, the difference between getting one percent better (or worse if you don't follow your intention) will compound to a huge gap. 

While 1% doesn't sound like much. It can be one small change in your routine. You read for 10 minutes before going to bed, you take a walk each day. You meditate for 3 minutes. Likewise, you eat an apple each day. These are only minimal things to do and are pretty achievable. But over time, they will form strong habits and make you the person you want to be. 

Start small, really small. And just stick with it. Try to do a bit every day. It is not essential how little it is. You just want to compound the effect. This is much more effective than doing a lot on one day, feeling exhausted and falling into old habits afterwards. 

This effect is called the **one percent rule**.


> *"Well-being is realized by small steps, but is truly no small thing"*
> -- Socrates