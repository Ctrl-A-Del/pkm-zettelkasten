---
date created: Monday, July 10th 2023, 8:40:53 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
tags: [permanent-note published]
sr-due: 2023-08-10
sr-interval: 23
sr-ease: 290
---

| Topic               | Source                                                                                                                                                                                                                                  |
| ------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [[⚙️ Productivity]] | [Deep Work Vs/Or/And The Pomodoro Technique: Which To Choose When You Want to Work Smarter?](https://valeriejegomarsh.medium.com/deep-work-vs-or-and-the-pomodoro-technique-which-to-choose-when-you-want-to-work-smarter-8d6a0df59130) |

---

> Pomodoro and deep work do not contradict. You need to choose when to use which.

The idea of deep work can be counterintuitive to the pomodoro technique. Deep work suggests getting into a flow of working and stay focused for longer periods of time, while pomodoro tells you to work for 25 minutes and take a 5-minute break. It can feel like the pomodoro timer kicks you out of the zone. To avoid this, it is important to use the right tool for the job. You do not have to work with the pomodoro technique all the time. You can still plan to do a longer focus session of 2 hours of uninterrupted work without any timers. **If a task requires this longer period of dedication, go for it**. Use pomodoro if you need a nudge to get started, to work on easier tasks (shallow work), or just to remind yourself that you *could* take a break, but are also free to go for the next 25-minute cycle without taking a break. Use the tools how you need them. But don't force the rules on yourself when they don't make sense.

Finally, **deep work is more than working for longer periods of time. It also means to create a distraction-free zone**!

- - -
## Links
1.  [[Deep work]]
2. [[The goal of Pomodoro]]