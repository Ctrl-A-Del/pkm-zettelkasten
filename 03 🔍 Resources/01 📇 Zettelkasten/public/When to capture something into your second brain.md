---
date created: Monday, July 10th 2023, 8:41:15 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
tags: [permanent-note, published]
sr-due: 2023-07-23
sr-interval: 4
sr-ease: 278
---
| Topic                                | Source |
| ------------------------------------ | ------ |
| [[📚 Personal Knowledge Management]] | [[Building a Second Brain]]       |

---

> Capture what resonates with you.

Deciding what to capture in your second brain can be difficult. You don't want to capture everything. It should always have a meaning. Also, you don't want to just copy and paste everything. Only capture the highlight of something. Make it easy to digest for your future self.

Here are some guidelines, if something is worth capturing:

- Is it inspiring?
	- Having a folder of things that motivate and inspire you, can give you the nudge when in doubt
- Is it useful?
	- Some resource that might be useful in the future
- Is it personal?
	- Some insight you had
	- some heartwarming moment you want to remember
- Is it surprising?
	- If something didn't surprise you, you basically already knew it
	- capture the unintuitive

Be careful not to capture everything. There is an abundance of information out there, but if you capture everything just to capture it, you will have trouble finding relevant information.

- - -
## Links
1. [[CODE Framework second brain]]
2. [[Writing every idea and every task down helps to declutter you mind]]
3. [[Highlight Question Answer Framework]]
4. [[Keeping an open mind when writing your notes means that you do not have to agree with everything in it]]  
