---
date created: Monday, July 10th 2023, 8:40:40 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---
> [!INFO]- 
> topic: [[🦒 TypeScript]]
> links:  [[Common types in TypeScript]]
> source: TypeScript course by the native web
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> An interface is a way to define the structure of an object in TypeScript.

Because objects behave different in JavaScript than in other object-oriented languages, there are also differences between an interface in TypeScript and e.g. C-family languages.
The name of an interface should always start with an uppercase letter.

You can also define functions in interfaces.
Optional parameters are marked with a `?`.

```ts
interface Person {
	firstName: string;
	middleName?: string; // this is an optional parameter
	lastName: string;
	age: number;
	fullName: () => string; // returns string
};

const person: Person = {
	firstName: 'Tom',
	lastName: 'Smith',
	age: 44,
	fullName(): string {
		return `${this.firstName} ${this.lastName}`;
	}
};
```


