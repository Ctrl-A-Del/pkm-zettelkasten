---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
sr-due: 2023-08-07
sr-interval: 4
sr-ease: 274
---
| Topic | Source |
| ----- | ------ |
|   [[🧩 React]]    |  [React - Udemy](https://www.udemy.com/course/react-the-complete-guide-incl-redux/learn/lecture/25597300#overview)      |

---

CSS Modules are a common alternative to styled-components.
They have the benefit of keeping your styling and your code separate, by utilizing CSS files.

To use CSS Modules, your projects need to be configured to use them. Create-React-App does this by default.

To use CSS Modules, you name your CSS file with a `module` extension, e.g. `Button.module.css`.

You then import your CSS file like this

```js
import styles from './Button.module.css';
```

The `styles` import represents an object with all your styles as properties, so you can append those styles like this:

```js
<button className={styles.button}>
```

CSS Modules then creates unique class names for each time you use it.

- - -
## Links
1. [[Using styled-components to create scoped styling]]
