---
sr-due: 2023-10-02
sr-interval: 83
sr-ease: 290
date created: Monday, July 10th 2023, 8:41:35 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[🖥️ Tech]]
> links:  [[Scaling a database]] 
> source: [Apache Solr - Wikipedia](https://en.wikipedia.org/wiki/Apache_Solr)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Solr is an open-source database platform.

**Solr** (pronounced "solar") is an [open-source](https://en.wikipedia.org/wiki/Open-source_software "Open-source software") [enterprise-search](https://en.wikipedia.org/wiki/Enterprise_search "Enterprise search") platform, written in [Java](https://en.wikipedia.org/wiki/Java_(programming_language) "Java (programming language)"). Its major features include [full-text search](https://en.wikipedia.org/wiki/Full_text_search "Full text search"), hit highlighting, [faceted search](https://en.wikipedia.org/wiki/Faceted_search "Faceted search"), real-time indexing, dynamic clustering, database integration, [NoSQL](https://en.wikipedia.org/wiki/NoSQL "NoSQL") features and rich document (e.g., Word, PDF) handling.