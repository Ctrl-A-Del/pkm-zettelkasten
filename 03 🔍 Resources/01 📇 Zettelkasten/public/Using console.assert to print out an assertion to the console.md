---
sr-due: 2023-09-03
sr-interval: 53
sr-ease: 270
date created: Monday, July 10th 2023, 8:39:25 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[🖥️ Tech]]
> links:   
> - [[Use console.table to print out huge objects into the console]]
> - [[Use console.dir for logging html elements]]
>
> source: [Tips and Tricks for Debugging JavaScript - YouTube](https://youtu.be/_QtUGdaCb1c?t=295)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

Instead of using `console.log`, you can print out an assertion with `console.assert`. The first parameter is the assertion and the second one is the message you want to print as an error (similar to `console.error`, if the assertion failed.

```js
console.assert(value !== undefined, "This shouldn't be undefined");
```