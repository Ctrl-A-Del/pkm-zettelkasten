---
sr-due: 2023-09-02
sr-interval: 53
sr-ease: 270
date created: Monday, July 10th 2023, 8:40:56 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[🖥️ Tech]]
> links:  [[What's a REST API]]
> source: [What is a REST API? - YouTube](https://www.youtube.com/watch?v=lsMQRaeKNDk)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---


**C**reate
**R**ead
**U**pdate
**D**elete

| CRUD   | HTTP method |
| ------ | ----------- |
| Create | Post        |
| Read   | Get         |
| Update | Put         |
| Delete | Delete      |
