---
sr-due: 2023-09-21
sr-interval: 73
sr-ease: 290
date created: Wednesday, June 21st 2023, 12:25:03 pm
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  
> - [[Organizing your tasks]] 
> - [[Clarifying your tasks]]
> source: [[Willpower ]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> If you plan your tasks daily, you will probably fail because life gets in your way. Make weekly or monthly plans, so that missing a single day does not mean you didn't reach your goals.

When you consider using to-do lists to manage your tasks and goals, there is evidence, that a daily list is not the best way to go. If you specifically schedule every task for a specific day, changes are very high, that live will get in your way, and you will not be able to fulfill them because you cannot plan out every moment and situation. This can feel like a failure and be demoralizing. This reduces the chances of you continuing using lists at all, event though they can be a mighty tool to increase your productivity.

Instead of daily lists, consider using a weekly or monthly plan. This way, if you fail on one day, it is no big deal. You can easily recover from this fail and do more the next day. This means that you can manage spontaneous situations more easily and still achieve the success of checking of all your goals for that period of time.