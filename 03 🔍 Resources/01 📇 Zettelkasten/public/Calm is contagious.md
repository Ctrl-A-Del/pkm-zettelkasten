---
date created: Monday, July 10th 2023, 8:41:39 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---
> [!INFO]- 
> topic: [[🧠 Psychology]]
> links:  [[Don't panic, if your service breaks over night]]
> source: The Daily Stoic
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Calm is contagious, but so is panic. 

There is an old saying in the Navy SEALs: calm is contagious. This is especially important for the person in charge. 

If the person having the command is calm, this will help everyone else to stay calm as well. 

Always try not to let panic get the best of you. 