---
sr-due: 2023-07-24
sr-interval: 17
sr-ease: 254
date created: Monday, July 10th 2023, 8:41:01 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[🧠 Psychology]]
> links:  
> - [[Responsibility of choice]] 
> - [[Bad measurements of happiness]]
>
> source: [[The subtle art of not giving a fuck]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Don't mix fault and responsibility. They are different things. And you will have to take responsibility for things that are not your fault to learn how to make better choices.

Fault and responsibility are two different things.

A fault lies in the past, and it is not always something, that you did.

Responsibility is in the present, and it defines your current and future choices.

You are responsible not only for your own faults, but you can also be responsible for things that are not your fault.

If something bad happens to you, it is your responsibility to deal with your feelings.


If you are having trouble getting on with a fault, you might need to define better values. You should always try to make something good out of bad events. Acknowledge them and learn from them. 

If it was your fault, try to learn how to make better choices in the future.

If it wasn't your fault, first really think about if it really wasn't your fault. But if it wasn't, maybe you can still learn something from it?

Taking responsibility for your own choices is personal growth.
