---
date created: Monday, July 10th 2023, 8:40:35 am
date modified: Thursday, July 27th 2023, 12:28:55 pm
tags: [permanent-note, published]
sr-due: 2023-08-24
sr-interval: 28
sr-ease: 286
---
| Topic               | Source |
| ------------------- | ------ |
| [[⚙️ Productivity]] | [[Getting things done]]       |

---

> Capture every idea, document or task in an inbox. This is a place to dump everything that comes to your mind and keep your mind clear.

Many tasks, documents and ideas pile up rapidly on your desk or your mind. You are constantly bombarded with stuff to do. This results in a messy desk and a full head with countless tasks on your mind. Especially the stuff on your mind is a burden because either those ideas constantly occupy your mind, or you tend to forget them.

> If something is on your mind, your mind isn't clear.

To avoid this, you should [[Writing every idea and every task down helps to declutter you mind|write everything down]] as soon as it comes to your mind or the task is visible. 

You should create an inbox for all the stuff that needs to be written down. This can be a digital or physical inbox in the form of a letter tray. Write everything down, no matter how unimportant it is. Give it its own note. If you want to use a digital inbox, you should additionally use a physical one for letters and documents that you get physically.

In this step, only capturing is needed. We will organize the stuff later.

- - -
## Links
1. [[Five steps to deal with work]] 
2. [[Writing every idea and every task down helps to declutter you mind]]
3. [[When to capture something into your second brain]]
