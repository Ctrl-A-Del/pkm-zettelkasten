---
sr-due: 2023-09-04
sr-interval: 54
sr-ease: 270
date created: Monday, July 10th 2023, 8:40:23 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[🖥️ Tech]]
> links:  [[Continuous Integration]]
> source: [Continuous Integration vs Continuous Delivery vs Continuous Deployment in a Easy Way - YouTube](https://www.youtube.com/watch?v=bVRdiA6LVaE)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

Continuous deployment automatically builds your code as soon as it is committed and, if the tests and build process work without any issues, pushes it to production automatically.