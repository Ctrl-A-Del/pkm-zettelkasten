---
date created: Monday, July 10th 2023, 8:40:40 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
tags: [permanent-note, published]
sr-due: 2023-08-18
sr-interval: 21
sr-ease: 287
---
| Topic             | Source |
| ----------------- | ------ |
| [[🧠 Psychology]] | [[The subtle art of not giving a fuck]]       | 

---

> If you struggle how to tackle a task, just do something. This will inspire you to find new solutions

If you are clueless or afraid to engage with some problem, avoiding it won't help you. 

**Just do something**

It doesn't matter how much you achieve by this start. Just by beginning somehow, solutions might occur to you.

The same applies to a lack of motivation. If you don't feel like being productive, but you know you should do something. Just start with anything. It can (and probably should) be the tiniest task you find. Your action will trigger new motivation because accomplishing stuff actually feels good.

Even small, meaningless actions can inspire greater actions.

- - -
## Links
 1. [[Engaging on your next action]] 
 2. [[Getting better each day - the compounding effect of the one percent rule]]
 3. [[Two Minute Rule]]
