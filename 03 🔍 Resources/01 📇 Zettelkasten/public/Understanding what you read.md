---
date created: Monday, July 10th 2023, 8:40:42 am
date modified: Wednesday, July 19th 2023, 1:11:08 pm
tags: [permanent-note, published]
sr-due: 2023-08-26
sr-interval: 30
sr-ease: 288
---
| Topic               | Source |
| ------------------- | ------ |
| [[⚙️ Productivity]] | [[How to take smart notes]]       |

---

> To remember what you read, you need to work with the text. You should write the information down in your own words and work with the idea by linking it to other ideas.

The problem when reading a text is to really understand it and be able to remember the essence later. More often than not, we tend not to be able to actually recall the core ideas of it.  
There are a few things that can help you to understand a text you are reading. 

**The most important thing in reading is to not only read, but to write it down in your own words**. Underlining and rereading a text is not enough to certainly understand it. You should be able to explain the gist of the argument to someone else. By writing it down, the other person is your future you. By rephrasing it in your own mind, you make sure, that you can remember and explain it.

> If you can't say it clearly. You don't understand it yourself. --- John Searle

Working with a text is more important, than only reading it. Even if you read a text multiple times, you tend to forget it pretty quickly. But by working with the information, writing it down and linking it to your [[What is a Zettelkasten|Zettelkasten]], you tend to understand and remember it much better. 

- - -
## Links
1. [[Confirmation Bias - We like ideas that support our point of view]]
2. [[Keeping an open mind when writing your notes means that you do not have to agree with everything in it]]
3. [[Getting better each day - the compounding effect of the one percent rule]]
4. [[The problem of starting with a blank page]]
5. [[Illusion of competence]]
