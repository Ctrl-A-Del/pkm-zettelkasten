---
sr-due: 2023-07-20
sr-interval: 4
sr-ease: 270
---

> [!INFO]- 
> topic: [[🧹 Clean Code]]  
> links:  [[Embrace refactoring - Clean code takes time]]  
> source: [[The Pragmatic Programmer]]  
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Maintain a high standard to you code, because bad quality spreads quickly.

## Broken Window Theory
The broken window theory suggests that when a building has one broken window left unrepaired, it creates an impression of neglect, attracting more vandalism and deterioration. Similarly, in software development, when small issues or bugs are left unaddressed, they can lead to a perception of poor quality, which may encourage further neglect and degradation of the codebase.

## How to Prevent Software Rot

To combat software rot and prevent it from spreading like broken windows, developers should adopt several practices:

1. Maintain a clean codebase: Regularly refactor and clean up code to remove duplication, improve readability, and ensure adherence to best practices. This reduces complexity and makes future maintenance easier.

2. Continuously test: Implement comprehensive automated tests to identify issues early on and prevent regressions. This ensures that changes made to fix bugs or add new features do not inadvertently introduce further problems.

3. Embrace automation: Use tools for continuous integration (CI) and deployment (CD) to automate build processes, testing, and deployment. This minimizes manual errors while speeding up development cycles.

4. Update dependencies: Keep libraries, frameworks, and other dependencies up-to-date to leverage bug fixes, security updates, and performance improvements provided by their maintainers.

5. Encourage collaboration: Foster a culture where developers communicate effectively with each other through code reviews, pair programming sessions, or knowledge sharing sessions. This helps identify potential issues early on before they become ingrained in the codebase.


