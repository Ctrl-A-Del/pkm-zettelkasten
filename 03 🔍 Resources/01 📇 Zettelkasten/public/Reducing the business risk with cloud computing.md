---
sr-due: 2023-09-03
sr-interval: 59
sr-ease: 290
date created: Monday, July 10th 2023, 8:39:21 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🖥️ Tech]]
> links:  [[Basic definition of Cloud Computing]]
> source: [Key Considerations for Cloud Computing - Overview of Cloud Computing | Coursera](https://www.coursera.org/learn/introduction-to-cloud/lecture/jXd6b/key-considerations-for-cloud-computing)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> The easy scalability of cloud computing reduces your business risk by avoiding huge investments up front.

By using the pay-as-you-go monetization of most cloud computing services, you can start a new project with minimal initial cost. You avoid having a huge investment up front before even knowing if your project will attract any customers. Instead, you can start the service really slow and scale up according to your needs.

Externalizing deployment can reduce your time to create a [[Minimum viable product]] because you don't have to create your own infrastructure.