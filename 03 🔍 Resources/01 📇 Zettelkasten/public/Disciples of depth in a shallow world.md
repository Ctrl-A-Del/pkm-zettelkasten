---
date created: Monday, July 10th 2023, 8:41:12 am
date modified: Wednesday, July 19th 2023, 10:50:59 am
tags: [permanent-note, published]
sr-due: 2023-08-27
sr-interval: 31
sr-ease: 294
---
| Topic               | Source |
| ------------------- | ------ |
| [[⚙️ Productivity]] | [[Deep Work]]       |

---

> Deep work is a rare asset in our modern world.

Since the rise of the internet, deep work has become a rare asset. Social media, emails and instant messengers are constantly taking up our attention. **There is always a *ding* that tries to destroy our focus**. There is always that nagging feeling that we might miss something important. In some companies, there is a rule to react to a message as fast as possible or even to maintain a Twitter account while working. 

**While answering emails and attending meetings might seem productive, it actually is just keeping us busy**. However, it is scattering our focus and stops us from being really productive. If you want to take away some time to do some deep work, you might even be called rude. 

While all of this makes it difficult to achieve a state of focused work, but it also makes it a rare skill, giving you the edge if you manage to reap the benefits.

- - -
## Links
1. [[Pomodoro vs Deep Work]]
