---
sr-due: 2023-07-21
sr-interval: 24
sr-ease: 270
date created: Monday, July 10th 2023, 8:41:01 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[👪 Parenting]]
> links: 
> source: [[Erziehen ohne Schimpfen]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> You can express your stress level like a traffic light. The issue is, we often don't realize how often we are close to the red zone.

Imagine a traffic light of your stress during the day:

**Green** means that you are still in contact with your child 
**Red** means, we are out of our mind, in rage and frighten our children 
**Yellow** is the middle between these two. It's the moment when we leave the green zone and are on our way to red.

It can be very difficult to realize when you are in the **yellow** part and are only inches away of getting **red**. We often get angry because of some random, tiny event, that usually wouldn't bother us. But since we are so close to the outburst, this drop is enough to trigger us.
We need to learn to feel, when we are in the yellow area and find ways to calm ourselves *before* we trip into the red part. We can also ask others to tell us, when we are close to the edge. E.g. you can tell your kids to say "green", when you are getting angry to remind you, that you seem to be in the yellow or red zone.