---
sr-due: 2023-09-17
sr-interval: 67
sr-ease: 270
date created: Monday, July 10th 2023, 8:41:36 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[🧠 Psychology]] [[⚙️ Productivity]]
> links:  
> - [[Going into any direction is better than standing still]]
> - [[Don't follow your passion - why working right is more important than finding the right work]]
> source: [[The Art of Work]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> To find our passion, we need to look back at our life and see what it tells us. 

We often wonder how to find the passion of our life. We imagine it to some when appear, and we will recognize it as soon as we see it. However, we don't need to look in front of us, to find it. We need to look behind us. We have to see at all the events of our life and figure out, what they tell us. **Our past life is already telling us what our passion is**. Therefore, we don't need a map, we need a shovel.