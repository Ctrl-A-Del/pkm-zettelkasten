---
sr-due: 2023-08-15
sr-interval: 39
sr-ease: 305
date created: Monday, July 10th 2023, 8:41:42 am
date modified: Sunday, July 16th 2023, 9:10:14 pm
---

> [!INFO]-  [[🩺 Health]] #permanent-note #published
> source: [[Atmen - heilt - enntspannt - zentriert]]
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p>

---

The breathing triangle means to have a breathing rhythm with a pause. A good rhythm is 3 - 3 - 3 or 4 - 4 - 4.

So, you breathe in 4 seconds, breath out 4 seconds and pause 4 seconds. 

This has a positive effect on your nerve system and relaxes. 

## Links
- - - 
1.   [[Breathing in the resonance frequency]] 


