---
date created: Monday, July 10th 2023, 8:39:33 am
date modified: Thursday, August 3rd 2023, 3:25:26 pm
tags:
  - permanent-note
  - published
sr-due: 2023-08-07
sr-interval: 4
sr-ease: 275
---
| Topic             | Source |
| ----------------- | ------ |
| [[🧠 Psychology]] | [[The subtle art of not giving a fuck]]       |

---

> We always have a choice. Even if something bad happens to us, it is our choice what we make out of this event.

If we don't have a choice, it makes us feel trapped. If you are unhappy with a situation, it is often because you don't feel like you have a choice.

Luckily, ==**we always have a choice==.** 

**We are responsible for everything that happens in our life**. 

**Even if something bad happens to you, it is your responsibility to define the meaning of that event for yourself**.

This may sound hard for some bad events, but even the worst scenarios can give you strength, if you are willing to see it. 

You have to acknowledge the bad things for what they are. You don't have to like them, but you can define how you deal with them.

We are always choosing.

- - -
## Links
1. [[Bad measurements of happiness]]
2. [[Responsiblity-fault fallacy]]
3. [[Rejection is a Life Skill]]