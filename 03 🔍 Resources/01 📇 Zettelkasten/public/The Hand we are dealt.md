---
date created: Monday, July 10th 2023, 8:40:40 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---
> [!INFO]- 
> topic: [[🧠 Psychology]]
> links:  
> - [[Nobody is special]] 
> - [[Responsibility of choice]] 
> - [[Responsiblity-fault fallacy]]
>
> source: [[The subtle art of not giving a fuck]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> We are born with our hand of cards and have to play them as good as we can. It is your own responsibility to live with and work on your issues.

We are all born with our issues. They may differ from person to person, but we can't change them. Even if you encounter some specific problem or tick that you are confronted with, you have to live with it the best you can.

It is your choice how you deal with those problems. You can run away from them or face them and try to live with them or even fight against them.

Nobody can simply take them away from you. It is your own responsibility to try to do something about them. Every tiny step can make a huge difference.

Your goal should be to be as good as **you** can, not as anyone else can be.

We all have our own hand of cards and need to play them as good as we can.
