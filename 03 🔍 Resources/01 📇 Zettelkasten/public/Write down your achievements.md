---
date created: Monday, July 10th 2023, 8:40:40 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
tags: [permanent-note, published]
sr-due: 2023-09-30
sr-interval: 74
sr-ease: 270
---
| Topic                                 | Source |
| ------------------------------------- | ------ |
| [[⚙️ Productivity]] [[🧠 Psychology]] | my wife       |

---

> Write down you achievements to bring them up when you need them. 

Whenever you achieve something, specifically in your professional life, write that down. 
Write down nailing that amazing deal with the customer. 
Write down finishing the new project. 
Write down that extra course that you learned. 
Write down that difficult problem you solved. 

Write down that you did it, what the problem was and how you solved it. 

This will not only create a libabry of amazing solutions, but also your personal achievements. It can help you being more confident. 

But the most important thing: you can bring those stories up when needed. Explain this achievements in a new application or in the next feedback round. Having this list makes it much easier to sell yourself. 

- - -
## Links
1. [[Engaging on your next action]] 
2. [[Getting better each day - the compounding effect of the one percent rule]]
3. [[Two Minute Rule]]