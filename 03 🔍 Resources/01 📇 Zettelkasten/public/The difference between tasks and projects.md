---
date created: Monday, July 10th 2023, 8:40:15 am
date modified: Wednesday, July 19th 2023, 1:15:00 pm
tags: [permanent-note, published]
sr-due: 2023-07-22
sr-interval: 3
sr-ease: 253
---
| Topic               | Source |
| ------------------- | ------ |
| [[⚙️ Productivity]] | [[Getting things done]]       |

---

> A task is only one action. Everything that requires multiple steps is a project.

You should make a clear distinction between a task and a project. 

**A task describes exactly one action to do. 

A project is usually a collection of tasks. As a rule of thumb, everything that needs more than one step to complete can be considered a project.

This differs from our common understanding of projects being big undertakings with complex goals, requiring a lot of management etc.

With this new definition, something easy as returning a parcel can be considered a project. You have to contact the seller, pack the package, get a shipping label and bring it to the office. There are many steps to take, so it is a project. But most people generally would not have considered this a project.

This definition allows us to simply group our tasks and always focus on the next action to bring the project forward.

- - -
## Links
1. [[Five steps to deal with work]]
