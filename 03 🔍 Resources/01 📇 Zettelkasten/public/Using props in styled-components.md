---
date created: Monday, July 10th 2023, 8:39:49 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---
> [!INFO]- 
> topic: [[🧩 React]]
> links:  [[Using styled-components to create scoped styling]]
> source: https://www.udemy.com/course/react-the-complete-guide-incl-redux/learn/lecture/25597278#overview
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> You can pass props into styled-components to influence the styling.

You can pass props into styled-components in the same way that you pass props into other react components.

```jsx
...
<FormControl invalid={!isValid}>
...
```

You handle the props like this:

```jsx
const FormControl = styled.div`
	border: 1px solid ${props => props.invalid ? 'red' : '#ccc'}
`
```
You use the default syntax to use JavaScript inside template literals and pass the props into the function to access its properties.