---
date created: Monday, July 10th 2023, 8:41:33 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---
> [!INFO]- 
> topic: [[🦒 TypeScript]]
> links:  [[Generic type parameters make your components reusable over a variety of types]]
> source: TypeScript course by the native web
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p>  

---

If you want to pass a generic object and the corresponding key properties into a function, you can extend the generic key to be a key of the corresponding object `<TObject, TKey extends keyof TObject>`.

```ts
const person = {
  firstName: 'Dana',
  lastName: 'Scully',
  age: 30,
  department: 'X-Files'
};

const getProperty = function <TObject, TKey extends keyof TObject> (obj: TObject, key: TKey) {
  return obj[key];
};

const firstName = getProperty(person, 'firstName');

export {};
```