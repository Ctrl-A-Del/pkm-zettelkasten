---
date created: Monday, July 10th 2023, 8:40:10 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
tags: [permanent-note published]
sr-due: 2023-08-11
sr-interval: 24
sr-ease: 308
---
| Topic                 | Source |
| --------------------- | ------ |
| [[[⚔️ Role-playing]]] | [[Return of the Lazy Dungeon Master]]       |

---

> Keep descriptions of locations brief and make them special by adding unique aspects. 

You should create a list of locations that your party might walk into. While describing them, try to be brief. Give them a name that sticks and a couple of aspects that make this place unique and fantastic. The aspects should describe something, that the players can interact with and that makes this location special. Be careful not to go too much into detail. Always be prepared [[Prepare to throw your work AWAY|to throw them away]], if they don't occur or fit in. Each location should be used for one specific scene, not the whole location.

- - - 
## Links
1. [[Prepare to throw your work away]]