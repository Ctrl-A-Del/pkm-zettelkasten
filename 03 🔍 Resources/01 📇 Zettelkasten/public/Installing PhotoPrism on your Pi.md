---
sr-due: 2023-09-22
sr-interval: 70
sr-ease: 310
date created: Monday, July 10th 2023, 8:39:44 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🖥️ Tech]]
> links:  [[Hosting your own image gallery]]
> source: 
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> This is a complete step-by-step guide to install PhotoPrism on a Raspberry Pi 4.

The installation of PhotoPrism is pretty straightforward.

However, a Pi 3 or 4 is recommended to be able to use 64 bit mode and achieve a better indexing performance.

I used a MacBook for my setup. The steps to flash the SD card vary a bit on a Windows machine.

1. Flash [RaspberryOS Lite](https://www.raspberrypi.org/software/operating-systems/) via [Balena Etcher](https://www.balena.io/etcher/) (Mac)
2. Reinsert SD card to mount it on the Mac
3. Add `ssh` file to enable ssh on the device 
```shell
cd /Volumes/boot
touch ssh
````
4. (Create a `wpa_supplicant.conf` to enable Wi-Fi if needed)
```shell
cd /Volues/boot
vim wpa_supplicant.conf
```

```
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=<Country Code>

network={
     ssid="<SSID>"
     psk="<PASSWORD>"
     scan_ssid=1
}

```
4. Otherwise, set up a connection via LAN (you can also set up Wi-Fi later via `raspi-config`)
5. Insert SD card into raspberry
  - a Raspberry Pi 3 / 4 is recommended to enable 64bit mode, see https://docs.photoprism.org/getting-started/raspberry-pi/#system-requirements
1. Connect to the device via ssh, 
- user: pi
- password: raspberry
```shell
ssh pi@<your-ip>
````
1. Change your password
```shell
passwd
````
2. raspi-config` and change time zone and update raspi-config
3. Update the system via `sudo apt update && sudo apt upgrade`
4. Enable 64 bit mode in `config.txt`
```shell
sudo vi /boot/config.txt
```
  - Find the section for your Pi, either `[pi4]` or `[pi3]` and add the following:
```
arm_64bit=1
```
  - `sudo reboot`
  - ssh back in and check if it worked
 ```shell
 uname -a
 ````
 - your output should look like this:
```shell
Linux raspberrypi 5.10.17-v8+ #1414 SMP PREEMPT Fri Apr 30 13:23:25 BST 2021 aarch64 GNU/Linuxe
```
- we are looking for the `arch64` part
1. Install docker and docker-compose
```shell
sudo apt install docker docker-compose
```
2. Download the PhotoPrism docker file
```shell
wget https://dl.photoprism.org/docker/arm64/docker-compose.yml
```
3. Configure the `docker-compose.yml` to your liking, I recommend the following:
```
PHOTOPRISM_ADMIN_PASSWORD: "betterPassword"
HOTOPRISM_DATABASE_PASSWORD: "betterPassword"
MYSQL_ROOT_PASSWORD: betterPassword
MYSQL_PASSWORD: betterPassword
```
4. Start the docker image
```shell
sudo docker-compose up -d
```
- the download may take some time
- test if your PhotoPrism instance is running by visiting the local IP of your Pi on port 2342
![[Bildschirmfoto 2021-05-10 um 22.38.09 1.png]]

