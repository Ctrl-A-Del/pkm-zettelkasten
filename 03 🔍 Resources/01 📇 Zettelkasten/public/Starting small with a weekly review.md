> [!INFO]- 
> topic: [[⚙️ Productivity]]  
> links:  
> - [[Organizing your tasks]]
> - [[Organize your second brain with PARA]]
> - [[Two Minute Rule]]
> - [[Reflecting on your work]]
>
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p>  

---


Having a weekly review is a great habit to keep your tasks, notes, appointments and goals aligned. But it can be scary to get it right, or even get in the habit of doing it. 

Start small.

Make it simple in the beginning. Start by only looking at your calendar and see what meetings you have the upcoming week and if you have to prepare something. Continue by clearing one of your inboxes and go bigger as you feel comfortable. 

The goal is to make it a frictionless habit.