---
sr-due: 2023-07-20
sr-interval: 23
sr-ease: 268
date created: Monday, July 10th 2023, 8:40:52 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[🧠 Psychology]]
> links:  
> - [[Being optimistic makes you a bad planner]] 
> - [[Keeping an open mind when writing your notes means that you do not have to agree with everything in it]]
>
> source: [[06 🗃️ Archiv/03 🗄️ References/How to take smart notes]]
> tags: #permanent-note  #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Confirmation bias describes the tendency to prefer arguments and information that support your own point of view rather than opposing arguments. 

The human mind prefers arguments it agrees with because we like to strengthen our own point of view. 

This is why we tend to live in our own *filter bubble*, in which our own viewpoint is echoed all the time. This strengthens the idea, that this view is the only true one because we never see the other side. 

However, only we search for opposing arguments to broaden our mind and get new insights. 

It is important to confront yourself with other ideas, and therefore to specifically look and analyze arguments that differ from your point of view. 

The problem of confirmation bias is, that it happens to everybody, all the time. You have to remember yourself every time to consider other arguments. Charles Darwin wrote down the biggest arguments against his ideas to put himself into this position.