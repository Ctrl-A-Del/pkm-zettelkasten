---
date created: Monday, July 10th 2023, 8:40:00 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
tags: [permanent-note, published]
sr-due: 2023-08-05
sr-interval: 13
sr-ease: 292
---
| Topic       | Source |
| ----------- | ------ |
| [[🖥️ Tech]] | [console: time() method - Web APIs - MDN](https://developer.mozilla.org/en-US/docs/Web/API/Console/time)       |

---

If you want to figure out which part of your code is taking so long or have another reason to check the computing time of some of your code, you can use `console.time()` to start a timer. Use `console.timeEnd()` to stop the timer, and the time passed will be printed to the console.

- - -
## Links
1. [[Using console.assert to print out an assertion to the console]]
