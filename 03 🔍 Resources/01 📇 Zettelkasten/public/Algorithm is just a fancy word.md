---
sr-due: 2023-09-22
sr-interval: 73
sr-ease: 290
date created: Monday, July 10th 2023, 8:41:02 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
tags: [permanent-note, published]
---
| Topic       | Source |
| ----------- | ------ |
| [[🖥️ Tech]] | [[Cricking the Coding Interview]]       |

---

> An algorithm is just a program that solves a problem.

The term algorithm strikes fear into many people's eyes. They think that algorithms are complex and difficult to understand. But actually, an algorithm is simply a fancy word to describe a solution to a problem.

- - -
## Links
1. [[Big O Notation]]
