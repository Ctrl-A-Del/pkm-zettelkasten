---
date created: Monday, July 10th 2023, 8:39:43 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
tags: [permanent-note, published]
sr-due: 2023-07-23
sr-interval: 4
sr-ease: 270
---
| Topic | Source |
| ----- | ------ |
| [[👪 Parenting]]      | [Warum Kindern vorlesen so wichtig ist - Das gewünschteste Wunschkind](https://pca.st/episode/b19e0cdf-77d7-49c6-aaf8-49340e7ef7f7)       |

---

> When reading with your child, it's not about finishing the story, it's about having a nice time with your child and a book.

When reading out load, make it exciting. Interact with your child. Don't just be stuck with reading the story. Your child will probably interrupt you anyway. It will show you something on the picture, or it will tell you something that comes to mind while listening. You might think *"yeah, that's great, but I actually wanted to read that story"*, but this is not what you want to tell your child. Reading is about a great interaction with the book. If they interrupt you, this is not disrespectful, but it means they are invested in the story. Also, you can *make reading interactive*. Let you child make the noises that are described. Ask it what it would've done in that situation. Have a great time with your child and don't force yourself to finish that story. That part is not essential. 

- - -
## Links
1. [[It's okay if your child jumps around if you are reading for it]]
