---
date created: Monday, July 10th 2023, 8:41:14 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---
> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  
> - [[The pain of being great]] 
> - [[Failure-success paradox]]
>
> source: 
> - [[Grit - The Power of Passion and Perseverance]] 
> - [[The Art of Work]]
>
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Talent is not the cause of excellence, practice is. 
> --- K. Anders Ericsson

Being exceptional at something doesn't come down to a natural-born talent, but to continuous practice and perseverance. You need to push yourself beyond your limits. You're capable more than you realize, but you have to stick with it.