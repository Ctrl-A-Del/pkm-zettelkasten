---
sr-due: 2023-10-05
sr-interval: 85
sr-ease: 310
date created: Monday, July 10th 2023, 8:39:36 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🖥️ Tech]]
> links:  
> - [[Creating a SSL Certificate for Gandi with Nginx Proxy Manager]] 
> - [[Add HTTPS proxy for a service via Nginx Proxy Manager]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p>  

---

> Nginx is a DevOps tool to create a HTTPS proxy for a web service that you want to expose to the internet. This is a guide how to get it working on a RaspberryPi 4

We use the [Nginx Proxy Manager](https://nginxproxymanager.com/) to get nginx running via Docker easily and without the need to configure it that much.

These instructions are taken from the [Quick Setup](https://nginxproxymanager.com/#quick-setup)

==**There is an issue with the database and Pi4 in the setup from the link. The yml in this document is working! Use it**==
The problem was the specific database image.

1. Install Docker and Docker-Compose

- [Docker Install documentation](https://docs.docker.com/install/)
- [Docker-Compose Install documentation](https://docs.docker.com/compose/install/)

1. Create a docker-compose.yml file similar to this:

```yml

version: '3'
services:
  app:
    container_name: nginx
    image: 'jc21/nginx-proxy-manager:latest'
    restart: always
    ports:
      - '80:80'
      - '81:81'
      - '443:443'
    environment:
      DB_MYSQL_HOST: 'db'
      DB_MYSQL_PORT: 3306
      DB_MYSQL_USER: 'npm'
      DB_MYSQL_PASSWORD: 'npm'
      DB_MYSQL_NAME: 'npm'
    volumes:
      - ./data:/data
      - ./letsencrypt:/etc/letsencrypt
    depends_on:
      - db
  db:
    container_name: maria_for_nginx
    image: 'yobasystems/alpine-mariadb:10.4.17-arm32v7'
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: 'npm'
      MYSQL_DATABASE: 'npm'
      MYSQL_USER: 'npm'
      MYSQL_PASSWORD: 'npm'
    volumes:
      - ./data/mysql:/var/lib/mysql
```

1. Bring up your stack

```
docker-compose up -d
```

1. Log in to the Admin UI

When your docker container is running, connect to it on port `81` for the admin interface. Sometimes this can take a bit because of the entropy of keys.

[http://127.0.0.1:81](http://127.0.0.1:81/)

Default Admin User:

```
Email:    admin@example.com
Password: changeme
```

Immediately after logging in with this default user, you will be asked to modify your details and change your password.

1. Upgrading to new versions

```
docker-compose pull
docker-compose up -d
```

This project will automatically update any databases or other requirements, so you don't have to follow any crazy instructions. These steps above will pull the latest updates and recreate the docker containers.

