---
sr-due: 2023-09-07
sr-interval: 63
sr-ease: 308
date created: Monday, July 10th 2023, 8:41:39 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[🧠 Psychology]]
> links:  
> - [[Risk aversion - if we don't get a big advantage by taking a risk, we avoid it]] 
> - [[You need to gain twice as much as you gain]]
>
> source: [[Thinking fast and slow]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> We prefer certain outcomes to uncertain ones. We are willing to expect a lesser expected outcome to get an amount for sure.

We prefer certain outcomes to uncertain ones. Consider the following choices:

1. a sure gain of $30
2. 80% chance to win $45 and 20% chance to win nothing

Even though the expected outcome of the second choice is higher ($36), most people prefer the first option.

We don't want to choose the second option and get nothing. This fear of regret pushes us towards the certain outcome because we are [[Risk aversion - if we don't get a big advantage by taking a risk, we avoid it|risk avers]]. The difference in the amount does not justify the risk of getting nothing.

This means, that we are willing to accept a worse outcome, if we can guarantee a success. This is why many companies make money by buying your riskier option (dept collection from a customer, money from a court process etc.) because in the big scheme they can still profit, even if some of them lose. You, on the other hand, are willing to get less money, just to make sure you get most of it.