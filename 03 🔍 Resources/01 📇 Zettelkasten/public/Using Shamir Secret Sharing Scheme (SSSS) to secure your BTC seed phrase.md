---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
---
| Topic                       | Source                                                                                                                                         |
| --------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------- |
| [[🖥️ Tech]] [[💵 Finance]] | [Store your secrets securely.](https://secretco.in/)                                                                                           |
|                             | [Reddit – Entdecke ohne Ende](https://www.reddit.com/r/BitcoinDiscussion/comments/l2r76r/convert_any_1224_seed_into_a_shamir_backup/)          |
|                             | [Ultimate guide to storing your bitcoin seed phrase backups - Unchained](https://unchained.com/blog/how-to-store-bitcoin-seed-phrase-backups/) |
|                             | [slips/slip-0039.md at master · satoshilabs/slips · GitHub](https://github.com/satoshilabs/slips/blob/master/slip-0039.md)                     |

---

> SSSS is a backup mechanism that splits your single seed phrase into multiple secrets. You can define the amount of secrets needed to regenerate the original.

#### What is this
Create a set of several secrets. You will need a specified threshold of secrets to get back the original secret.

Common are **2 of 3** or **3 of 5**.

This helps you to limit the risk of storing it. You can lose some of the secrets, but you can still regenerate the original. If some secrets are compromised. It is useless without enough other secrets 

#### How to do this
- You can use something like secretco.in to [store your secrets securely.](https://secretco.in/)
- ==Use the offline website== to avoid sneaking
- backup website code to be *independent*
clone repo


###### Ideas for Backup
You can use any way to store those phrases. Save some to the cloud, keep some offline at home in paper or at your parents house. Choose different and safe locations. Make sure, that you can still regenerate the key, even if some keys get lost. For example, have enough backup outside your house, to get the seed, even if your house burns down.

- - -
## Links
1. [[Unlinked]]
