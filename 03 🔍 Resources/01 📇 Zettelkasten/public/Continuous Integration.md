---
date created: Monday, July 10th 2023, 8:40:37 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
sr-due: 2023-08-28
sr-interval: 33
sr-ease: 294
tags: [permanent-note, published]
---
| Topic        | Source |
| ------------ | ------ |
| [[🖥️  Tech]] |  [What is Continuous Integration? - YouTube](https://www.youtube.com/watch?v=1er2cjUq1UI)       |

---

> Continuous Integration aims to commit small changes into the build instead of huge features to avoid big merge conflicts.

Continuous integration aims to avoid *merge hell*. Often, developers working on features separate from each other over a long time. If they merge their changes then, this can result in a lot of merge conflicts. **Continuous integration wants you to commit changes as small as possible to the current build**. This allows everybody to always work on the most recent state and makes merge conflicts much smaller. However, you also need to create an automatic build and testing environment to always ensure, that everything is still working when adding your change to the large code base

- - -
## Links
1. [[Continuous Deployment]]
