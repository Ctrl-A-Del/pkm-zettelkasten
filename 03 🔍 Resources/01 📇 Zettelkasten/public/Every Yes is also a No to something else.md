---
sr-due: 2023-08-04
sr-interval: 26
sr-ease: 250
date created: Monday, July 10th 2023, 8:41:01 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🧠 Psychology]] [[⚙️ Productivity]] [[👪 Parenting]]
> links:  [[Rejection is a Life Skill]]
> source: [[Erziehen ohne Schimpfen]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Every time you say yes to something, you automatically say no to something else.

If you say yes to something, it always means saying no to something else. If you agree to help your mother, do some extra tasks at work, you make some people happy. But this means that you say no to having a nice evening with your children or significant other. More work can also mean more stress., which is something we easily overlook.

Always consider what you are missing by saying yes to something. Make sure that you are happy with your decision.