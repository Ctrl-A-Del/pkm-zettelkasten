---
sr-due: 2023-09-20
sr-interval: 68
sr-ease: 290
date created: Monday, July 10th 2023, 8:40:11 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[👪 Parenting]] [[🧠 Psychology]]
> links:  [[Communicating boundaries with a yes]]
> source: [Erziehen ohne Schimpfen - mit Nicola Schmidt - Das gewünschteste Wunschkind](https://pca.st/episode/3a895dab-7801-45c7-8aaf-ba2d439330a7)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Our brain can't handle stress and physical sensations at the same time. Use this by sending some physical feeling to your brain and numb the stress. 

**If we get angry, our brain sends this signal for about eleven seconds**. If we stay angry for a longer period of time, it's because **we let ourselves be angry**. We continuously step into this feeling. 
But if we try to stop this feeling, we can break out of the rage and step back into our conscious mind. 

There is a great trick to do this: some physical sensation. **Our brain can't handle stress and physical sensations at the same time**. If we have to run away from a tiger, we won't even realize that we hurt ourselves on some branches. The physical sensation is cut off. On the other hand, you can trigger your brain to think about a physical sensation instead of thinking about your rage. Your brain can only do one of those things. If you concentrate on rubbing your hands or doing jumping Jacks, your brain will stop feeding your rage. 