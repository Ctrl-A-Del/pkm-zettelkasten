---
date created: Monday, July 10th 2023, 8:41:04 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
tags: [permanent-note, published]
sr-due: 2023-08-02
sr-interval: 13
sr-ease: 270
---
| Topic       | Source |
| ----------- | ------ |
| [[🖥️ Tech]] |[How to Flush DNS Cache on a Mac](https://www.lifewire.com/flush-dns-cache-on-a-mac-5209298)        |

---

> If you have a site can't be reached error, this is likely a DNS error. To fix it, flush the DNS cache.

I recently had the issue, that every webpage I open, shows me an error.

> [!warning] This site can't be reached.

After a short delay, the page loaded just fine.

This is actually a DNS issue, so flushing the DNS fixed it.

On Mac, this is the command.

```bash
sudo dscacheutil -flushcache; sudo killall -HUP mDNSResponder
```

- - -
## Links
1. [[Don't panic, if your service breaks over night]]
