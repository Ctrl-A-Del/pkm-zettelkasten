---
date created: Monday, July 10th 2023, 8:41:32 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---
> [!INFO]- 
> topic: [[🧩 React]]
> links:  
> - [[Test Driven Development]]
> - [[Use Request Handlers to mock data fetching tests]]
> source: [React Testing Library and Jest: The Complete Guide | Udemy](https://www.udemy.com/course/react-testing-library-and-jest/learn/lecture/35701758#overview)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Module mocking doesn't test the module you are replacing.

Module mocking describes the process of overwriting some module, that you would normally import into your test. For example, when you want to test an API, the real request might introduce undesired side effects to your test. Module mocking allows you to mock some dummy code, that will be used instead of the real code. While this makes testing easy, this means that the real code will never be tested, which is why this is not the ideal way.

Instead, use a [[Use Request Handlers to mock data fetching tests|Request Handler]].

Here is an example of module mock, but it is using a hook that does an API request, which is why it should be avoided.

```js
jest.mock('../hooks/useRepositories', () => { //instead of using the code inside this module, use this code
return () => {
	return {
		data: [
			{ name: 'react' },
			{ name: ' javascript'}
		]
	}
}

});
```