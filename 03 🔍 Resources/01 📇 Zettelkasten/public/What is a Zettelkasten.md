---
date created: Monday, July 10th 2023, 8:40:55 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
tags: [permanent-note, published]
sr-due: 2023-10-03
sr-interval: 77
sr-ease: 270
---
| Topic               | Source |
| ------------------- | ------ |
| [[⚙️ Productivity]] | [[06 🗃️ Archiv/03 🗄️ References/How to take smart notes]]       |

---

> The Zettelkasten method is a workflow to create notes that you can use to increase your long-term knowledge and find new insights. It prevents you from having a mess of notes and gives you the ability to find your ideas easily and make interesting connections between them.

A Zettelkasten is a system to organize your notes in a way, that makes it easy to work with your notes, make connections between different topics and stumble upon new ideas. The goal is to make learning and writing a frictionless process because your notes are doing the work.

It was invented by Niklas Luhmann. He was able to publish an outstanding number of papers and books because it was easy for him. He did not need to come up with topics and research about them. The ideas came to him naturally, and he had a lot of the text in a usable format in his notes.

The notes in a Zettelkasten are linked together so that you can make connections between and across different topics and stumble upon new ideas on the fly. 

However, the Zettelkasten is more than just a collection of notes. It is a workflow. You write your notes on the go whenever ideas come up, or you consume media. You then work flow these notes regularly to work the good ideas into permanent notes that help you to develop these ideas and be able to know what they were about easily.

 - - - 
## Links
1. [[What types of notes belong to a Zettelkasten]] 
2. [[The problem of starting with a blank page]] 
3. [[Why I write my notes in English]]
4. [[Compass of Zettelkasten]]
5. [[Understanding what you read]]
6. [[Using spaced repetition to review your notes is a great way to improve and deepen your understanding]]