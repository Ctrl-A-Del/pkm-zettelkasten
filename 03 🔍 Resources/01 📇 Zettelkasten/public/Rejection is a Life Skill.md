---
date created: Monday, July 10th 2023, 8:40:04 am
date modified: Wednesday, July 19th 2023, 12:27:50 pm
tags: [permanent-note, published]
sr-due: 2023-07-20
sr-interval: 1
sr-ease: 230
---
| Topic             | Source |
| ----------------- | ------ |
| [[🧠 Psychology]] | [[The subtle art of not giving a fuck]]       |

---

> Saying no to something isn't bad. It can help you to put a focus on the things that truly matter.

You can't just say yes to everything in your life. You will have to focus on some things and by doing this reject other things.

If you start a relationship with a person, you usually have to reject having sex with many changing partners. If you decide to go with a job that gives you more freedom, it might mean less income etc. 

You shouldn't consider these rejections as a limitation. Only by saying no to some things, will you be able to experience the results from these decisions. A relationship can open you to a whole new world of feelings.

Try to learn your priorities and to what things in life you want to say yes and to what kind of things you intend to say no.

- - -
## Links
1. [[Going into any direction is better than standing still]]
