---
date created: Monday, July 10th 2023, 8:40:59 am
date modified: Wednesday, July 19th 2023, 11:14:41 am
tags: [permanent-note, published]
sr-due: 2023-08-21
sr-interval: 20
sr-ease: 273
---
| Topic        | Source |
| ------------ | ------ |
| [[🧩 React]] |  [React Testing Library and Jest: The Complete Guide | Udemy](https://www.udemy.com/course/react-testing-library-and-jest/learn/lecture/35701764#overview)       |

---

> How to set up msw and use it to mock request handlers.

[msw (Mock Service Worker)](https://mswjs.io/docs/getting-started/install) is a framework to create request handlers to mock data fetching tests.

It intercepts the configured requests and returns the response you give it without sending out the real request.


## Manual Setup
To achieve this, you have to define the handlers and set up a server.
```js
import { rest } from 'msw';
import { setupServer } from 'msw/node';

const handlers = [
  rest.get('/getPerson', (req, res, ctx) => { // define the type of request (GET, POST,...) and the route, that you want to intercept
    return res (
      ctx.json({ //define the object to return
        "id": 2,
        "name": "John"
      })
    );
  })
];
```

You will need to set up a testing server to handle the requests. You can use Jests lifecycle for this.
```js
beforeAll(() => {
  server.listen();
});

afterEach(() => {
  server.resetHandlers();
});

afterAll(() => {
  server.close();
});
```

msw works asynchronously, which means that you have to wait for the response before continuing testing. To achieve this, you can use one of React Testing Libary's functions that uses `act()` under the hood:

```js
screen.findBy...
screen.findAllBy...
waitFor
user.keyboard
user.click
```


## Setup with server.js
It is generally a good approach, to put all of this setup into its' own file, by creating a server setup file:

`server.js`
```js
import { setupServer } from 'msw/node';
import { rest } from 'msw';

export function createServer(handlerConfig) {
  const handlers = handlerConfig.map((config) => {
    return rest[config.method || 'get'](config.path, (req, res, ctx) => {
      return res(ctx.json(config.res(req, res, ctx)));
    });
  });
  const server = setupServer(...handlers);

  beforeAll(() => {
    server.listen();
  });
  afterEach(() => {
    server.resetHandlers();
  });
  afterAll(() => {
    server.close();
  });
}
```

It can then be used like this:

```js
import { createServer } from './test/server';

createServer([
  {
    path: `/getPerson`,
    method: "post",
    res: (req, res, ctx) => {
      return {
        "id": 2,
        "name": "John"
      }
    },
  }
]);
```

- - -
## Links
1. [[Set up Jest environment]]
2. [[Use Request Handlers to mock data fetching tests]]
