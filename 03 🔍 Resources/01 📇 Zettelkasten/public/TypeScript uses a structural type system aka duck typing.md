---
date created: Monday, July 10th 2023, 8:39:57 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
tags: [permanent-note, published]
sr-due: 2024-02-19
sr-interval: 211
sr-ease: 290
---
| Topic             | Source |
| ----------------- | ------ |
| [[🦒 TypeScript]] |  TypeScript course by the native web      |

---

TypeScript is not a *nominal type system*, so **you don't need to inherit the interfaces all the way** to make your code work. It is important that your class implements the correct interface to be recognized as the same type.

Instead, TypeScript uses *duck typing* or structural type system.
The term comes from the saying *"If it looks like a duck, walks like a duck and quacks like a duck, it probably is a duck."*. 
**So ==even if the class doesn't implement the interface, but has the same properties, those types are still compatible==**.

By the way, Go also uses a structural type system.

```ts5
const person = {
  firstName: 'Brendan',
  lastName: 'Eich'
};

// Duck typing (aka "structural type system")
const bar = function (p) {
  console.log(p.firstName);
};

bar(person);
```

- - -
## Links
1. [[Interfaces in TypeScript]]
2. [[Excess properties when casting in TypeScript]]
