---
sr-due: 2023-07-23
sr-interval: 26
sr-ease: 290
date created: Monday, July 10th 2023, 8:39:23 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[🖥️ Tech]]
> links:  [[Installing GrapheneOS]]
> source: 
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> After installing GrapheneOS you can go on without Google or install Google Services in a sandboxed manner. 

While it is easy to install GrapheneOS on your device, the big question is, what you will do without it afterwards. You basically get a pretty bare-bones phone with very few apps preinstalled. Depending on your opinion, this is great because you have no bloatware preinstalled. However, it leaves you the responsibility to get anything installed at all.

## The Freedom to Do whatever You want

### A Life without Google
The great thing is, that you can choose to go as far or as little into Google as you personally want. The OS comes with no Google Services installed at all. You can keep it that way and use stores and apps that don't require Google and enjoy the feeling of a *deegoogled* phone. In this case, you can use [F-Droid](https://f-froid.org) (or any other frontend, e.g. Droidify) to access many remarkable open-source apps. In addition, [Aurora Store](https://auroraoss.com/) is a great app that enables you to install all apps that are available in the PlayStore without having the latter installed on your system.

### The Traditional Phone
If you want to have a more traditional experience, just go into the Apps from GrapheneOS and install Google Play Services and Google Play Store. GrapheneOS has a [sandboxed](https://grapheneos.org/features#sandboxed-google-play) way to install Play Services which doesn't give them elevated rights, but instead they are installed like any other app. This allows you to toggle the access rights of the app, which you can't do in vanilla Android.

