---
sr-due: 2023-08-19
sr-interval: 40
sr-ease: 274
date created: Monday, July 10th 2023, 8:41:22 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  [[Everything that takes multiple steps is a project]]
> source: [[Getting things done]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> If you are stuck with a project, follow these 5 steps to get it going.

There are five steps to get a project going and keep it on its tracks:

## 1. Defining Purpose and Principles
At first, you have to decide why you are doing something. This is important to frame your decisions and gives you a motivation. It also helps you to define your goals.

## 2. Envisioning
Envision yourself having succeeded. What is your desired outcome? Again, this creates goals and motivation.
## 3. Brainstorming
Write down all your ideas and tasks as they come to mind. Be completely judgement free to allow ideas to just come to your mind.
## 4. Organizing
Structure the result of your brainstorming. Trash useless ideas and sort useful ones.
## 5. Define the next Action
Define the next physical action, that is required to move the project forward. Be as precise as possible. *Call meeting* is not precise. Who is part of the meeting? When to schedule it?


However, not all projects need to be written out. For most projects, you can do these steps automatically in your head. Maybe 5% of projects are big enough that you really need to write out these steps.


