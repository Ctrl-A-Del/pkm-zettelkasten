---
sr-due: 2023-09-05
sr-interval: 56
sr-ease: 270
date created: Wednesday, July 12th 2023, 8:59:20 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[🧠 Psychology]]
> links:  
> - [[I am wrong much more often than I am right - and why this is a good thing]] 
> - [[Responsibility of choice]]
>
> source: [[The subtle art of not giving a fuck]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Feeling certain has the danger of disappointment. Feeling uncertain helps us to fight for the desired outcome and helps us to improve.

Certainty about something is a dangerous thing. There are few things in life that are certain. There is always something that can change the outcome and get into your way.

Feeling certain about something kindles expectations and can result in disappointment. If we feel certain that we deserved a specific outcome, we often tend to blame others. 

Blaming others is never a suitable solution. Instead, we should think if we are responsible for this outcome.

Uncertainty removes the burden of disappointment and gives us the opportunity to make mistakes, be wrong about something and improve.

Feeling uncertain can also help us to really try. If we are not certain, that we gain something, we enable ourselves to truly work hard to get it, instead of just waiting for it. It helps us to be the best version of ourselves.