---
date created: Monday, July 10th 2023, 8:39:46 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---
> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  
> - [[What is a Zettelkasten]]
> source: [Obsidian: inbox review with spaced repetition - YouTube](https://www.youtube.com/watch?v=zG5r7QIY_TM&themeRefresh=1) [Spaced repetition may be a helpful tool to incrementally develop inklings](https://notes.andymatuschak.org/z7iCjRziX6V6unNWL81yc2dJicpRw2Cpp9MfQ)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Looking at your notes at specific intervals will keep you engaged with your Zettelkasten.

While it is one thing to write down everything you learn and it definitely helps you to understand the new things, it is still easy to forget about everything you put so much effort into writing. To help you take another look at your [[PKMS]], spaced repetition can be the solution.

If you have ever learned with flashcards, you basically did spaced repetition. It means taking a look at something over a specific interval. If the information was easy, you take another look at a later interval, for example a week. If the information was new, or you didn't know the answer, look at it sooner. Depending on the importance of learning, this can be 5 minutes or tomorrow. This system will help you to regularly engage with the content you created.

While this is helpful for vocabulary, it can also be used to review your notes. Read them, improve them, remember them. It can help you to find those lost gems in your Zettelkasten again.

I am using a plugin for Obsidian, called [obsidian-spaced-repetition](https://github.com/st3v3nmw/obsidian-spaced-repetition/) to review my notes.