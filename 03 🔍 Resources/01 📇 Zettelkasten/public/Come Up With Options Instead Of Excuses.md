> [!INFO]- 
> topic: [[🏡 Life]] [[🖥️ Tech]]  
> links:  
> - [[Stop Blaming Others - How to Take Responsibility for Your Actions]]
> - [[Don't Blame, Take Responsibility - Responsiblity-fault fallacy]]
>
> source: [[The Pragmatic Programmer]]  
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

Take responsibility for your errors. You can't just tell your boss that the cat ate your source code. Instead of lame excuses, deliver options. Be honest and direct and admit your errors.

Start thinking in solutions. What can you do to resolve the issue? What can be done to avoid this from happening in the future? Learn from your mistakes.