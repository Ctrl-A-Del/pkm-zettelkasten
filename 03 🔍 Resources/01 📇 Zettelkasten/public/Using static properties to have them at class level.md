---
date created: Monday, July 10th 2023, 8:40:01 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---
> [!INFO]- 
> topic: [[🦒 TypeScript]]
> links:  
> - [[Interfaces in TypeScript]]
> - [[Common types in TypeScript]]
>
> source: TypeScript course by the native web
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

To access properties or functions from class level, you have to define them as `static`.


```ts
class Dog {
  public readonly name: string;

  public static numberOfDogs: number = 0;

  private constructor (name: string) {
    Dog.numberOfDogs += 1;
    this.name = name;
  }

  public static bringToLife (name: string): Dog {
    return new Dog(name);
  }

  public bark (): void {
    console.log('Woof woof!');
  }
}

const alice = Dog.bringToLife('Alice');
console.log(alice.name);

const malin = Dog.bringToLife('Malin');
console.log(malin.name);

console.log(Dog.numberOfDogs);
```