---
sr-due: 2023-08-26
sr-interval: 46
sr-ease: 250
date created: Monday, July 10th 2023, 8:40:26 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🖥️ Tech]]
> links:  [[How to scale a web server]]
> source: [Was ist ein Load Balancer? Definition für Load Balancing - Citrix Germany](https://www.citrix.com/de-de/solutions/app-delivery-and-security/load-balancing/what-is-load-balancing.html)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> A load balancer splits up server traffic to several different devices.

A load balancer is a piece of hard- or software that you place in front of your web server to handle huge amounts of requests and avoid an overload. To achieve this, they balance out the requests across multiple servers and decide which client is handled by which server. They can use different techniques, starting by [[Round Robin]] or using a more complex mechanism that analyzes the response time, process load, or bandwidth and picks the server by one or several of these criteria.