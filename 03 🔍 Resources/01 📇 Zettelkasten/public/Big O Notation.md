---
sr-due: 2023-07-23
sr-interval: 12
sr-ease: 256
date created: Monday, July 10th 2023, 8:40:53 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
tags: [permanent-note, published]
sr-due: 2023-10-25
sr-interval: 83
sr-ease: 276
---
| Topic       | Source |
| ----------- | ------ |
| [[🖥️ Tech]] | [[Cracking the Coding Interview]]       |

---

> The Big O notation is used to express the space or time complexity of an algorithm. 

Imagine you have to send some data to a friend across the country. What would be the fastest way to send it over there? A digital file transfer? For small amounts of data this is certainly true, but for dozens of terabytes this might take days. In that case, driving or flying there and giving your friend a hard drive might be quicker. This is because digital file transfer takes longer the more data you send. Flying over there always takes the same amount of time. The time you need is linear. 

The big O notation is the same. It gives you am idea how an algorithm will perform. Is the time needed linear, exponential or logarithmic? To express this, we write O(N) or O(N²), where N is the number of inputs, e.g. The length of an array. 

- - -
## Links
1. [[Drop the constants and non dominant terms in Big O]]
2. [[Adding and multiplying in Big O]]
3. [[Recursive Runtimes]]
