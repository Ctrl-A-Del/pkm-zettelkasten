---
sr-due: 2023-09-02
sr-interval: 50
sr-ease: 280
date created: Monday, July 10th 2023, 8:40:25 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  
> - [[The problem of starting with a blank page]] 
> - [[What is a Zettelkasten]] 
> - [[Why I write my notes in English]] 
> - [[Getting better each day - the compounding effect of the one percent rule]] 
> - [[Creating a workflow with Todos]] 
> - [[Writing every idea and every task down helps to declutter you mind]] 
> - [[The Habit Loop]]
>
> source: [[06 🗃️ Archiv/03 🗄️ References/How to take smart notes]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> The Zettelkasten workflow uses different types of notes for distinct purposes. The key is to not mix these notes together because this creates a mess.

The [Zettelkasten](Zettelkasten.md) consists of different notes, that you write in different styles to finally achieve one goal: build a knowledge management system.

**The goal is to make writing a habit and learn to write your ideas down as they come to you** and write down notes while you do your research. You later transfer your notes into **permanent notes** in your Zettelkasten.

Working with the Zettelkasten means doing your thinking while you write. You should [[The Habit Loop|make a habit]] to [[Writing every idea and every task down helps to declutter you mind|writing everything down]], that might be important.

Generally, you can break down your notes into 5 different types of notes.

## Fleeting Notes
**On fleeting notes, you write all your thoughts down on the go**. It doesn't matter where you write them. A sheet of paper is as good as a random file on your smartphone. These notes don't need to be formulated nicely, as long as you know what is meant. They contain any idea or concept that comes to your mind during the day. Creating these notes should create no frictions in your workflow. Ideally, you review these notes once a day. You should review your fleeting notes and either transfer them into permanent notes or trash the notes that don't lead anywhere.


## Literature Notes
Whenever you consume media to learn (reading, videos, podcasts) do it with the ability to take notes. Write down the most important parts. Be extremely selective what to write down and ==**write it in your own words**==. This is essential to really understand the essence. Later transfer your insights into permanent notes. Archive the literature notes in your reference system afterward.

## Project Notes
These notes are only relevant to a specific project and go into a project-specific folder. If you can generalize the ideas, you can develop them into permanent notes. But if they are too specific, they just get trashed or archived once the project is finished.

## Permanent Notes
These are the notes that go into your Zettelkasten. Use your fleeting and literature notes to create them. Each note should only contain one idea, and it should be written in a way, that no other context is needed to understand them. Write them like for someone else. Use full sentences and be as precise as possible. It should fit on one *card*. This should mean no shorter than a tweet (180 characters) and not longer than a short essay (200 - 500 words).

## Index Notes
These work as an **entry point into a topic**. You should always link your permanent notes to an index. This way you have an overview of the ideas you already had about a topic. However, an index note should not consist of more than about 25 notes. If the index gets too big, consider separating it into different topics. You should also describe the topic in a few words.

## Reference System
This is not a type of note, but a place in your workflow. Literature notes go into your reference system after you worked through them. This way you have an entry point for further reading about a topic and can quickly check where you got the information from.