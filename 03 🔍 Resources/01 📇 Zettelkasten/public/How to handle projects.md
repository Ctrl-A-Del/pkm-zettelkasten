---
date created: Monday, July 10th 2023, 8:40:06 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
tags: [permanent-note, published]
sr-due: 2023-10-18
sr-interval: 84
sr-ease: 293
---
| Topic               | Source |
| ------------------- | ------ |
| [[⚙️ Productivity]] | [[Getting things done]]       |

---

> If you struggle to get on top of a project, there are five steps that can help you.

Handling big projects can be quite daunting and there are many things that can go wrong. Having a useful structure to deal with projects can help to get even big projects under control.

There are five natural steps of project planning:

1. Defining purpose and principles
    - ask why you have to do something 
    - the purpose gives the frame for decisions, resources and goals
2. Envisioning 
    - imagine the desired outcome 
    - you get an image what to aim for 
3. Brain storming
- get ideas about what to do without judgement 
1. Organizing
- structure the ideas from before 
- what's the plan 
1. Define the next action 
- define the next action in the plan? 


However, most projects don't need this steps to be written out. For easy tasks, you can do it in your head till you know what your next step is. Maybe 5% of your projects really need each step down deliberately.

If you continue to fail getting the hang of a project, take a few steps back and start from step one. Ask yourself *what is the purpose here*.

- - -
## Links
1. [[Five steps to deal with work]]