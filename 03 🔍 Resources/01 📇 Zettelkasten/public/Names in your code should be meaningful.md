---
date created: Monday, July 10th 2023, 8:40:30 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---
> [!INFO]- 
> topic: [[🧹 Clean Code]]
> links:  [[Clean code - write your code like an author writes a story]]
> source: [Clean Code | Udemy](https://www.udemy.com/course/writing-clean-code/learn/lecture/23111116#overview)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

Well named code allow readers to understand your code without going through it in detail.

Avoid having too generic or short names. Single character names are always a bad sign. 

Also, avoid redundant and unnecessary information. `userWithNameAndAge` might be a bit too much.

Slang (e.g. `user.facePalm()` instead of `user.sendError()`) or unclear abbreviations (e.g. `ymdt` instead of `dateWithTimezone`) can also be misleading. 

Use distinctive names, to differentiate your functions from one another.

Here are some rules of thumbs to name different parts of your code:

## Variables
- usually data containers
- use nouns or short phrases with adjectives (for booleans)
- `const userData`
- `const isValid`

## Functions
- commands
- use verbs or short phrases with adjectives (for booleans)
- `sendData()`
- `inputIsValid`

## Classes
- create *things*
- nouns or short phrases with nouns
- `class User`
