---
date created: Monday, July 10th 2023, 8:40:23 am
date modified: Wednesday, July 19th 2023, 12:36:38 pm
tags: [permanent-note, published]
sr-due: 2023-07-23
sr-interval: 4
sr-ease: 272
---
| Topic        | Source |
| ------------ | ------ |
| [[🧩 React]] |   [React Testing Library and Jest: The Complete Guide - Udemy](https://www.udemy.com/course/react-testing-library-and-jest/learn/lecture/35701738#overview)     |

---

Don't use `act()` manually to fix warnings that tell you to when using React Testing Library. Instead, use one of those functions of RTL because it uses `act()` under the hood.

```js
screen.findBy... 
screen.findAllBy... 
waitFor
user.keyboard
user.click
```

These function asynchronously, so you probably have to use `async` and `await` to make them work properly.

- - -
## Links
1. [[Testing requests in Jest with msw]]

