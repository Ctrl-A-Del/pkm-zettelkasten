---
sr-due: 2023-08-07
sr-interval: 38
sr-ease: 290
date created: Monday, July 10th 2023, 8:40:56 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[👪 Parenting]] [[🧠 Psychology]]
> links: 
> source: Erziehen ohne schimpfen
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Practice taking responsibility for your mistakes playfully. 

If we want to improve the error culture in our family and society, we need to teach our children, that it's okay to make mistakes and take responsibility for them. 

To practice this, we can create a *blame game*. Each day, there is a specific person who is to blame for all things that happen today, *and* they take responsibility. They happily take responsibility for that and excuse themselves, no matter who actually did it. This way you set an example that this is not a bad thing. 