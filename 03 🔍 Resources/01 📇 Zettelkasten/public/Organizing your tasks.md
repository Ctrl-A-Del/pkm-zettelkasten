---
date created: Monday, July 10th 2023, 8:40:11 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---
> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  
> - [[The difference between tasks and projects]]
> - [[Five steps to deal with work]]
>
> source: [[Getting things done]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> You should organize your tasks in fitting lists to avoid forgetting about them. Having a few lists gives you the needed order to stay on top of everything.

After [[Clarifying your tasks]] you should organize them in a way, that makes it easy for you to always be aware of your to-dos.

To organize all items, you should make lists of your projects, timed events etc.

[[Getting things done]] recommends seven categories:

## 1. Projects List
This is a list of all your projects. This is meant to serve as an overview for all your projects that you are working on and prevents you from forgetting any projects over time.

## 2. Project Support Material
This is information (reference files) that you will probably need during a project.

## 3. Calendar Actions
This is your calendar. Write only time crucial items in your calendar. Don't note anything on there that you would like to be done at that date, but that isn't necessary.

## 4. Next Action List
This is a list of every next action that is required in every project. It might be useful, to sort them by context to allow you to see what action you can now work on. 
Useful contexts are:
- on computer 
- errands 
- partner/person x
- calls 
- at office 
- at home
- anywhere 
- read/review

### 5. Waiting for List
This is a list of tasks and projects that you are currently waiting for others to act. Review these regularly and check in with the other person.

## 6. Reference Material
Your document system with all items that only contain information and require no action.

## 7. Some Day Maybe List
A list of things and ideas that you might like to do in the future. Let your imagination run wild. This can be anything.

