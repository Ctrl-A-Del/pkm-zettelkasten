---
sr-due: 2023-07-30
sr-interval: 32
sr-ease: 273
date created: Monday, July 10th 2023, 8:41:16 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[🖥️ Tech]]
> links:  
	> - [[Pros and cons of React]]
	> - [[Vue]]
	> - [[Svelte]]
>
> source: 
> - [Vite | Next Generation Frontend Tooling](https://vitejs.dev/) 
> - [Vite 2.0 Crash Course | Super Fast Build Tool for JavaScript, React, Vue, Svelte, & Lit (2021) - YouTube](https://www.youtube.com/watch?v=LQQ3CR2JTX8)
>
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

Vite is a building tool to create new projects in react, vue or svelte.
