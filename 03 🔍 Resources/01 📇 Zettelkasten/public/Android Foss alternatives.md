---
date created: Monday, July 10th 2023, 8:40:24 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
tags: [permanent-note, published]
sr-due: Invalid date
sr-interval: NaN
sr-ease: NaN
---
| Topic       | Source |
| ----------- | ------ |
| [[🖥️ Tech]] | me       |

---

> This is a list of open source alternatives to some of my apps on Android.


| Current       | FOSS                                                                                                                                                                 |
| ------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| PocketCasts   | AntennaPod                                                                                                                                                           |
| Spotify       | Spotube, ViMusic, [Music](https://github.com/z-huang/music)                                                                                                          |
| WhatsApp      | WhatsApp Web Go                                                                                                                                                      |
| aCalendar     | Simple Calendar Pro, Etar                                                                                                                                            |
| Datei Manager | Amaze                                                                                                                                              |
| Wetter        | Geometric Weather                                                                                                                                                    |
| Reddit        | Infinity                                                                                                                                                             |
| Maps          | Organic Maps, Magic Earth                                                                                                                                            |
| YouTube       | ReVanced, NewPipe, [LibreTube](https://github.com/libre-tube/LibreTube)                                                                                              |
| PDF Viewer    | MJ PDF Reader                                                                                                                                                        |
| Camera        | [Google Camera with Gcam Services](https://www.youtube.com/watch?v=Pi8RwvnJiRw) or in [Sandboxed Google Play](https://grapheneos.org/features#sandboxed-google-play) |
| Gallery       | Simple Gallery, Aves                                                                                                                                                 |
| Netflix       | [CloudStream](https://github.com/recloudstream/cloudstream)                                                                                                          |

- - -
## Links
1. [[Installing GrapheneOS]] 
2. [[What to do after installing GrapheneOS]]
