---
date created: Monday, July 10th 2023, 8:39:24 am
date modified: Tuesday, July 18th 2023, 10:47:33 am
tags: [permanent-note, published]
sr-due: 2023-08-06
sr-interval: 19
sr-ease: 292
---
| Topic             | Source        |
| ----------------- | ------------- |
| [[🧠 Psychology]] | [[Willpower]] |

---

> Your willpower behaves like your physical stamina and can be depleted. You need it for a ton of different acts of self-control, so choose your battles wisely.

**Willpower is the force to resist temptation** and do something you actually don't want to do. But **resisting temptation is exhausting**. 

==**Your willpower can be used up**==. It behaves the same way as physical stamina. 

If you go through a difficult time, for example exam time during studies, you need all your willpower to learn. Most students often stop other good habits during this time. They don't do the dishes, work out or eat healthy. They simply don't have enough willpower left to do deal with this.

You only have one pool of willpower, and you need it for all of your self-control in different contexts. You need it to avoid sweets, deal with your boss, motivate yourself to go exercising etc. 

Because of this, **you should not try to change multiple behaviors at the same time**. You will need plenty of self-control to change one thing, so there will be not much left to deal with the other stuff. And failing then will feel like a failure. To avoid this, go in small steps and gradually improve.

- - - 
## Links
1. [[Where does willpower come from]]


