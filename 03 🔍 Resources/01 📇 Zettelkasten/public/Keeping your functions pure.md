---
sr-due: 2023-08-03
sr-interval: 28
sr-ease: 270
date created: Monday, July 10th 2023, 8:40:55 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🧹 Clean Code]]
> links:  
> - [[Functions should be small and do one thing - they should have a single responsibility]]
> - [[What are side effects of a function]]
>
> source: [Clean Code | Udemy](https://www.udemy.com/course/writing-clean-code/learn/lecture/23111258#overview)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

A pure function always returns the same output for the same input. In addition, a pure function doesn't have any side effects. Pure functions make your code predictable.