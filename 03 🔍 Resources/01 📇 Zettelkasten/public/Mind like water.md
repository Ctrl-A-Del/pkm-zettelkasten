---
date created: Monday, July 10th 2023, 8:40:03 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---
> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  
> - [[Capture Everything in an Inbox]] 
> - [[Creating a workflow with Todos]]
> - [[Writing every idea and every task down helps to declutter you mind]]
>
> source: [[Willpower]] [[Getting things done]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> If you write everything important down, you don't clutter your mind with unnecessary things. 

Your mind doesn't like unfinished thoughts and tasks. It tries to remember all your tasks, resulting in a cluttered mind - and forgetting most of them. Because we are bad at remembering stuff anyway, it is best to just write everything down, that's important to you. If you do make a habit of writing everything down, you can relax because you don't need to remember anything by yourself. This way, your mind can be free to flow and think about other things. You can have a mind like water. 