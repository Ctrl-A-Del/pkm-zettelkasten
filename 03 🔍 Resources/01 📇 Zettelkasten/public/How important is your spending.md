---
sr-due: 2023-09-18
sr-interval: 69
sr-ease: 290
date created: Monday, July 10th 2023, 8:41:39 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[💵 Finance]]
> links:  [[Zero based Budget - put your money in an envelope]]
> source: [How to Double Your Savings - YouTube](https://youtu.be/HvesR8SA3G4)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> By rating the importance of your purchases, you can get new insights into which ones really matter.

If you try to get a hand over your spending habits, consider rating how important each purchase is. Here is an example rating:

1. Essential
   - What you physically require
2. Have to Have
   - A purchase you enjoy making and life would be ruined without it
3. Nice to Have
   - A purchase you enjoy making, but life wouldn't be ruined without it
4. Shouldn't Have
   - A purchase you regret

These categories allow you to give yourself some slack and realistically learn about your spending habits. It's okay to have some things that you really want, but be also realistic about which purchases are probably not that important.

Ideally, if you sort your spending by importance, most of your money should go into essential and have to have things, while the least amount should obviously be in the Shouldn't have category. If this is not the case, you should reconsider your spending and eliminate some latter two categories.