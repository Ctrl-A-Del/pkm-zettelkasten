---
date created: Monday, July 10th 2023, 8:39:25 am
date modified: Wednesday, July 19th 2023, 1:10:03 pm
tags: [permanent-note, published]
sr-due: 2023-08-06
sr-interval: 14
sr-ease: 296
---
| Topic               | Source |
| ------------------- | ------ |
| [[⚔️ Role-playing]] | [[Return of the Lazy Dungeon Master]]       |

---

> When developing your own campaign, design it from the inside out. 

Start by only thinking about the location where your players start and that location. From there, design the locations that the players are lost interested in. Flesh out the next city only if your characters are going to visit it. Don't bother with the huge lore of the whole continent, unless this is a major part of your story, obviously. 

- - -
## Links
1. [[Prepare to throw your work away]]
