---
date created: Monday, July 10th 2023, 8:40:56 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
tags: [permanent-note, published]
sr-due: 2023-10-10
sr-interval: 83
sr-ease: 290
---
| Topic       | Source |
| ----------- | ------ |
| [[🖥️ Tech]] | [Blockchain and Analytics in the Cloud - Overview of Cloud Computing | Coursera](https://www.coursera.org/learn/introduction-to-cloud/lecture/XXfhq/blockchain-and-analytics-in-the-cloud)       |

---

> Blockchains are connected networks of users and are happening in the cloud by nature.

Blockchain describes a secure distributed system to build transparent applications. It is an immutable, open network that relies on all users deciding on a consensus. Because of the connected nature of blockchains, they are cloud services innately.

- - -
## Links
1.  [[AI in the cloud]]
2. [[Basic definition of Cloud Computing]]