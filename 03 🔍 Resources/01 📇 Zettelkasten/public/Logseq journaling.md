---
sr-due: 2023-07-21
sr-interval: 24
sr-ease: 302
date created: Monday, July 10th 2023, 8:39:33 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  [[Logseq]] [[Todos in Logseq]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> At it's core, Logseq is designed to be used for daily note-taking. Because of this, it has a nice home view with a chronological order of the last couple of days.

As I already explained in my [[Logseq|introduction to Logseq]], I use it to write down my notes and to-dos for my working day. You can easily write down your tasks, ideas and other notes during the day in your daily note and link it to other notes when it makes sense.

The home view can be accessed by hitting `gh` when the cursor is inactive. This view merges all your daily notes together into one big note that you can scroll through.

On each new day, a new note for today is created and gives you a clean slate to write down all your thoughts, as well as link them to other notes or days. You can use this to also mention days in the future and create reminders for specific days, that will then be visible on this day. More on this in a coming note about the task utilities of Logseq.

So, why is this journaling feature so useful, even if you don't write any journal? It gives you a place to write down everything you might need and makes it easily accessible, searchable and gives you the option to link it to other ideas, by referencing other notes outside your journal, or just one block from a note. 
These links are a great way to connect different concepts to each other. 

Obsidian has many of those features and also has a great plugin for daily note-taking. However, this doesn't come out of the box and the home view is missing, so you don't have this nice view to scroll through. This doesn't mean Obsidian isn't great. It is a wonderful piece of software. But for some use cases, I prefer to use Logseq.