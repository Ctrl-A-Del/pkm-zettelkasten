---
date created: Monday, July 10th 2023, 8:41:48 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
tags: [permanent-note, published]
sr-due: 2023-07-23
sr-interval: 4
sr-ease: 277
---
| Topic             | Source |
| ----------------- | ------ |
| [[🧠 Psychology]] | [[The subtle art of not giving a fuck]]       |

---

> Always question yourself, imagine what it would mean if you are wrong and if this might create a better outcome for you or others.

Try to introduce uncertainty as a value in your life. It can help you to [[Uncertainty is better than certainty|grow personally]].

Here are a couple of steps to get into the habit of reminding yourself to be uncertain:

## 1. Create a Habit of Questioning Yourself if You Might Be Wrong.
Being mindful about your possible mistakes helps you to question your decisions, learn to make better ones.

## 2. Imagine what it Would Mean if You Are Wrong
Being wrong is often a painful step and admitting it takes effort. It can result in [[Mansons Law of Avoidance|cracking the image of yourself]]. It can be a step that results in questioning your decisions and yourself. But you should also imagine, how you can change this mistake and what this would mean. Can this improve something in your life?

## 3. Would Being Wrong Create a Better, or a Worse Problem for Me or other People?
Even if admitting being wrong is difficult, eventually, it can help you or someone close to you? If so, shouldn't you try to do your best to achieve this better outcome?

- - -
- ## Links
1. [[Uncertainty is better than certainty]] 
2. [[Mansons Law of Avoidance]] 
3. [[Certainty effect - why we prefer a worse outcome as long as it is save]]

