---
sr-due: 2023-08-15
sr-interval: 35
sr-ease: 272
date created: Monday, July 10th 2023, 8:39:34 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
tags: [permanent-note, published]
---
| Topic               | Source |
| ------------------- | ------ |
| [[⚙️ Productivity]] |   [[06 🗃️ Archiv/03 🗄️ References/How to take smart notes|How to take smart notes]]      |

---

> Fleeting notes are an integral part in the [[What is a Zettelkasten|Zettelkasten]] workflow. They are non formatted notes that should be written frictionless and are reviewed regularly.


## Current
I am currently considering to use [Notesnook](https://notesnook.com/) to quickly capture my fleeting notes on my phone, because Obsidian has a considerable startup time from five to ten seconds, which is too much friction. 
When I am on the go, I am currently testing [Otter.ai](https://otter.ai/) to quickly create a voice note. It is automatically transcribed, so that I can easily copy it over later, without having to listen to it again.

Both solutions have the side effect of me having to transfer those notes over to my note taking system in Obsidian.

When on my desktop, I can easily create a note in Obsidian to begin with.

The note initially goes into my inbox, but I try to move it into a relevant space in my [[Organize your second brain with PARA|PARA]] system. 

## History
 I used *daily notes* in [Obsidian](https://obsidian.md/) to create my [[What types of notes belong to a Zettelkasten#Fleeting notes|fleeting notes]]. 
 
 Each day has its own note with the ideas that I had that day. This allows the daily notes to create a backlog of my ideas and to-dos and review them later. Having them in a note with a specific date keeps me engaged to review them soon enough as I see the daily notes folder growing. 
 After I reviewed a note, I move them into an archive folder for my daily notes. This way, I can easily see how many daily notes I haven't reviewed yet in my daily notes folder.
 
 When working through a fleeting note, I go through every bullet point and do one of the following things:
 
### Transfer it into a Permanent Note
I create a permanent note in my inbox with the title of that idea. I write this note right now or just leave it as a reminder in my inbox and do more research on the topic when I have time. In this case, the new note serves as a reminder and the fleeting note can be trashed.

### Transfer it into a To-do
If it is an actionable idea, I write it in my to-do app and give the task a date to remind me later. It is then put into my [[Creating a workflow with Todos|to-do workflow]]

### Trash it
Sometimes an idea looked good in the first place, but turns out to not really go anywhere later. Don't be afraid to trash a fleeting note if it doesn't seem to lead anywhere.


My template for my daily notes is quite simple:
```markdown
[⬅️[[Yesterday]] ||| 📅 [[Today]] ||| [[Tomorrow]] ➡️
# Fleeting Notes
…

# Daily Dump
…](<⬅️[[2022-06-23|Yesterday]] ||| 📅 [[Daily|Today]] ||| [[2022-06-25|Tomorrow]] ➡️


# Fleeting Notes
- 

# I should...


# Just a thought
**Idea**:: 

# I am thankful for...
**Gratitude**::

# Inspiring words
**Quote**::
**Source**::
```

I use a header at the top to quickly switch between my daily notes. Then I start with my fleeting notes. I also have sections to write down tasks, ideas, quotes and things I'm grateful for. I query those with the Dataview plugin in Obsidian to collect them at one place. The goal is to have an easily accessible place to store those notes and have some place to find them quickly, so I don't forget them.

Creating this template is easily done with the [Templater plugin](https://silentvoid13.github.io/Templater/).

- - -
## Links
1. [[What types of notes belong to a Zettelkasten]] 
2. [[Organize your second brain with PARA]]
