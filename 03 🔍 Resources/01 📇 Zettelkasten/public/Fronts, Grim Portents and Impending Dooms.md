---
date created: Monday, July 10th 2023, 8:39:59 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---
> [!INFO]- 
> topic: [[⚔️ Role-playing]]
> links:  [[Spiral Campaign Development]]
> source: [[Return of the Lazy Dungeon Master]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Define opposing fronts and create a series of events that end in a horrible outcome. 

When thinking about your campaign, the concept of *fronts* from Dungeon World presents an easy and effective way to do so. 

## Fronts
A front defines a threat that your players must overcome. This can be the villain of your campaign, or some other looming event. 

## Grim Portents
A Grim Portent is a milestone for the front. Some bad event that brings the enemy closer to their goal. It helps you to define the villain's actions. 

## Impending Doom
This is the final goal of the front. It describes what will happen, if the villain succeeds. 

## How Many?
Generally, three fronts should be a good start for your campaign. Define three grim Portents for each. Then, wait and see, which threads your party follows. 