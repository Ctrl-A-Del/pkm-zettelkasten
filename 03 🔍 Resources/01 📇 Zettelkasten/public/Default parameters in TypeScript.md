---
sr-due: 2023-09-05
sr-interval: 55
sr-ease: 270
date created: Monday, July 10th 2023, 8:40:07 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[🦒 TypeScript]]
> links:  [[Optional Parameters in TypeScript]]
> source: TypeScript course by the native web
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

You can assign a default value to a function so that it is assigned if you don't explicitly assign anything.

```ts
// Parameters with default values
const getHost = function (hostname: string, port: number = 443): string {
  return `${hostname}:${port}`;
};

const localhost = getHost('localhost', 3000);
const yourDomain = getHost('yourdomain.com');

console.log(yourDomain);
```