---
sr-due: 2023-08-30
sr-interval: 50
sr-ease: 293
date created: Monday, July 10th 2023, 8:40:10 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links: 
> - [[Five steps to deal with work]]
> - [[Creating a workflow with Todos]]
>
> source: [[Getting things done]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Trust your intuition to decide what task to engage in. If you are not sure, decide by context time, energy and priority.

After structuring all your projects and having a list of next actions, the last part is to really engage and decide what to do at what given time. How do you work through your list? 

You probably have [[Organizing your tasks|organized them by context]], but you will most likely have numerous tasks at the given context. So, what to do? 

**Trust your intuition**

The more experienced you get, the easier it is for you to decide which task is the most relevant at the time and which one is doable right now. 

But to help you find the right task, here are 4 criteria to choose the best suiting task for the moment:

1. Context 
    - where are you?
    - do you have the tools/person at hand? 
2. Time available 
    - how much does each task take?
    - how much time do you have right now?
3. Energy available 
    - do you have the energy to tackle a huge task?
    - if you are exhausted, get an easy win by doing easy things that you still need to do anyway
4. Priority
    - which task is the most urgent and important one?


You should look at these criteria in this order. A task can be super important and have top priority, but if you don't have enough time or energy or simply are not at the correct place, you can't do it.
