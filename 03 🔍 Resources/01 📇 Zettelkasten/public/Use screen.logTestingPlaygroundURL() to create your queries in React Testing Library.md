---
date created: Monday, July 10th 2023, 8:40:11 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
tags: [permanent-note, published]
sr-due: 2023-08-08
sr-interval: 16
sr-ease: 290
---
| Topic        | Source |
| ------------ | ------ |
| [[🧩 React]] |    [React Testing Library and Jest: The Complete Guide | Udemy](https://www.udemy.com/course/react-testing-library-and-jest/learn/lecture/35701640#overview)     |

---

`screen.logTestingPlaygroundUrl()` creates an interactive playground of the current markup you are rendering and gives you perfect queries to get every element.

- - -
## Links
1. [[Set up Jest environment]]
