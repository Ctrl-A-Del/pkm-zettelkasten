---
date created: Monday, July 10th 2023, 8:41:39 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
tags: [permanent-note, published]
sr-due: 2023-07-23
sr-interval: 4
sr-ease: 295
---
| Topic       | Source |
| ----------- | ------ |
| [[🖥️ Tech]] | me       |

---

> This Guide explains how to generate an SSL Certificate to later use for an HTTPS Proxy.

1. Go to the Security Panel of your Account in Gandi.
2. Generate and copy the API key
3. Go into the admin panel of your Nginx Proxy Manager
4. Go to SSL Certificates
5. Enter the desired domain name (with the subdomain)
6. Enter your gandi.net email address
7. Toggle `Use DNS Challenge`
8. Choose Gandi as DNS Provider
9. Enter your API Key
10. Generate the Certificate

- - -
## Links
1. [[Install Nginx Proxy Manager on a Pi 4]] 
2. [[Add HTTPS proxy for a service via Nginx Proxy Manager]]
