---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
---
| Topic     | Source |
| --------- | ------ |
| [[React]] |  [React 16: The Complete Course (incl. React Router 4 & Redux) - Udemy](https://www.udemy.com/course/react-the-complete-guide-incl-redux/)      |

---

The useReducer hook in React is a powerful tool for managing state in a more controlled and predictable way, especially when dealing with complex state logic. It is an alternative to using the useState hook when you have state updates that depend on the previous state or when your state has a more complex structure.  

useReducer is used for managing state by specifying a reducer function that handles state transitions.
The reducer function is a pure JavaScript function that takes two arguments: the current state and an action object.  
The reducer function computes and returns the new state based on the current state and the action.  
  
To update the state, you dispatch actions. An action is simply an object with a type field that describes what kind of state change you want to perform.  
When an action is dispatched, React calls the reducer function, passing the current state and the action to compute the new state.  

  
```jsx  
import { useReducer } from 'react';  
  
// Reducer function  
const reducer = (state, action) => {  
  switch (action.type) {  
    case 'INCREMENT':  
      return { count: state.count + 1 };  
    case 'DECREMENT':  
      return { count: state.count - 1 };  
    default:  
      return state;  
  }  
};  
  
// Initial state  
const initialState = { count: 0 };  
  
// Using useReducer  
const [state, dispatch] = useReducer(reducer, initialState);  
  
// Dispatching actions to update state  
const increment = () => dispatch({ type: 'INCREMENT' });  
const decrement = () => dispatch({ type: 'DECREMENT' });  
````  
  

useReducer can make state transitions more predictable and manageable, especially in scenarios where state updates depend on the previous state or involve complex logic.  
It can be particularly useful when you need to manage state for components with multiple related values.  

Consider using useReducer when useState becomes too unwieldy for managing your component's state, or when you need to centralize state logic and actions.  

- - -
## Links
1. [[Derived State in React]]
2. [[React state does not update immediately - use useEffect to work with the new value]]
