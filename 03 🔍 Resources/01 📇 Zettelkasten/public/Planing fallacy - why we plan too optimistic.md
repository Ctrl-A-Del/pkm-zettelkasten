---
sr-due: 2023-09-10
sr-interval: 60
sr-ease: 288
date created: Monday, July 10th 2023, 8:40:10 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🧠 Psychology]]
> links:  [[Being optimistic makes you a bad planner]]
> source: [[Thinking fast and slow]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> The issue of planing too optimistic is called planing fallacy. It describes the error of ignoring negative possibilities of a decision and overestimating its success. 


**The planing fallacy is the mistake of estimating outcomes of plans in a far too optimistic way**. You are confident, that you will be able to finish a project, do it in a reasonable time and have success with it. 

Examples can be projects, which costed far too much money, took too long, or even small businesses that failed. Take the BER airport in Berlin as an example. 

We tend to underestimate risks and only focus on the optimistic side because we would prefer a positive outcome. 


To avoid this, you need to take an outside view of the project. You need to think like a person, that does not benefit from a positive outcome. You tend to fall into an optimistic bias. 