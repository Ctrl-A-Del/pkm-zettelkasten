---
sr-due: 2023-08-31
sr-interval: 51
sr-ease: 291
date created: Monday, July 10th 2023, 8:41:12 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🖥️ Tech]]
> links:  [[Basic definition of Cloud Computing]]
> source: [Internet of Things in the Cloud - Overview of Cloud Computing | Coursera](https://www.coursera.org/learn/introduction-to-cloud/lecture/0EOqS/internet-of-things-in-the-cloud)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> The cloud can be the central collection point for data from IoT devices.

The internet of things creates tremendous amounts of data. The cloud can be the central collection point for all this data. Processing it, storing it and responding to the devices.