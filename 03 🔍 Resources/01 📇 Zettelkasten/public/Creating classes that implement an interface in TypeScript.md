---
date created: Monday, July 10th 2023, 8:40:42 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---
> [!INFO]- 
> topic: [[🦒 TypeScript]]
> links:  [[Interfaces in TypeScript]]
> source: TypeScript Course by the native web
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

Interfaces can extend other existing interfaces.
Classes can implement interfaces.

```ts 
interface Animal {
  name: string;
  breathe: () => void;
}

interface AnimalWithLegs extends Animal {
  numberOfLegs: number;
}

class Dog implements AnimalWithLegs {
  name: string;
  numberOfLegs: number;

  constructor (name: string) {
    this.name = name;
    this.numberOfLegs = 4;
  }

  bark (): void {
    setTimeout(() => {
      console.log(`Woof woof, I'm ${this.name}!`);
    }, 1_000);
  }

  breathe (): void {
    console.log('In and out, and in and out, and in and out, ...');
  }
}

const alice = new Dog('Alice');

alice.bark();
```
