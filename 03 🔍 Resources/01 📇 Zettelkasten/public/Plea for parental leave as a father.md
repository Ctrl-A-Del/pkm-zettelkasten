---
sr-due: 2023-10-01
sr-interval: 82
sr-ease: 290
date created: Monday, July 10th 2023, 8:41:31 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[👪 Parenting]]
> links:  [[The best parents I know make thirty mistakes each day]]
> source: Me
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Don't miss the time to see your children growing up.

I don't know about your social environment, but in mine, it is very rare for the father to take time off to take care of your child. I only know of very few fathers who did take a parental leave. 

It is very common for the mother to stay at home, while the father is still working full time. But I have plenty of issues with this approach.

I took the opportunity and started working part-time right after the birth of my daughter. I highly encourage you to do the same! Working less to have time with my child was the second-best decision of my life, after having a child in the first place. 

Sharing the carework with your significant other helps both of you to enjoy seeing your child growing up. It also gives both of you perspective on how much of your energy a child can drain. You both understand, why the flat is looking desastrous, why dinner is not ready and why your artner looks so exhausted, even though they were at home the whole day. Carework can be much more exhausting than your day job. And still, it is great, to spend time with your child. 

Your child will never be in this phase. If you take the time to get to know your 3-year-old, you might have missed your chance to get to know the 2-year-old.