---
date created: Monday, July 10th 2023, 8:41:33 am
date modified: Wednesday, July 19th 2023, 1:14:06 pm
tags: [permanent-note, published]
sr-due: 2023-08-07
sr-interval: 15
sr-ease: 296
---
| Topic       | Source                                                                                                                                                                 |
| ----------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [[🖥️ Tech]] | [CMR vs SMR Hard Drives in Network Attached Storage (NAS) T Buffalo Americas](https://www.buffalotech.com/blog/cmr-vs-smr-hard-drives-in-network-attached-storage-nas) |
|             | [NAS Festplatten mit SMR oder CMR ? Ein Überblick im Jahr 2021](https://www.elefacts.de/test-160-nas_festplatten_mit_smr_oder_cmr_ein_ueberblick_im_jahr_2021)                                                                                                                                                                        |

---

> Use CMR drives for your NAS because it works better for a RAID configuration.

When deciding for hard drives for your NAS system, you might come across the terms SMR and CMR.  
Those are different types of recording technologies used by hard drives.

**C**onventional **M**agnetic **R**ecording (CMR) puts the data on the drive side-by-side, while **S**hingled **Ma**gnetic **R**ecording (SMR) uses the fact, that write tracks are wider than read tracks and writes data sequentially on the track, so that they partially overlap.

![[Pasted image 20220708094213.png]]

**For your NAS configuration, you should use CMR drives because they are not optimal if you go for a RAID setup.**

Since WD does a mediocre job at telling you which drives of the red series use CMR and which use SMR, here is a handy [overview](https://nascompares.com/answer/list-of-wd-cmr-and-smr-hard-drives-hdd/).

- - -
## Links
1. [[Why I got a Synology NAS]]
