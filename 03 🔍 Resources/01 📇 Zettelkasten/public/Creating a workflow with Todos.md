---
date created: Monday, July 10th 2023, 8:41:48 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---
> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  [[Writing every idea and every task down helps to declutter you mind]] [[Getting better each day - the compounding effect of the one percent rule]]
> source: 
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

<img src="https://images.unsplash.com/photo-1484480974693-6ca0a78fb36b?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1352&q=80" width="800px" />

> Writing your tasks down helps you to remember them. Choose a daily highlight to-do for the day. This need to be achieved today. You can do more to-dos if you'd like to. 


If you are not writing down your to-dos, you probably should. It helps to remember all the stuff you need to do. However, you should not overdo it. I do not write down every daily task. So stuff like *doing the dishes* or *cooking* is not found in my to-do list. But tasks that are either important daily to-dos, like taking my medicine, are. The other to-dos are one-time tasks that I need to do at a later time. 

To avoid having this huge, frightening list of to-dos that is difficult to fulfill, don't push yourself too hard.

Choose one *daily highlight*. This is the task you have to do today. This should obviously be the most urgent one. You can do more tasks if you feel like it. But as long as you achieve your daily highlight, you can go to bed with a feeling of accomplishment. 

You can manage your to-dos however you want, but I prefer a to-do app on my phone. Having a constant reminder on my home screen helps me to remember my tasks and makes it easy to add new ones. 

[Tasks.org](https://tasks.org) is an open-source Android app that fulfills all my needs. I can add and prioritize tasks, put them into different lists, make them repeatable, and it has an outstanding widget. 