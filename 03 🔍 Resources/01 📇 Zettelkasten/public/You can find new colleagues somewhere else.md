---
date created: Monday, July 10th 2023, 8:40:37 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
tags: [permanent-note, published]
sr-due: 2024-05-15
sr-interval: 296
sr-ease: 330
---
| Topic               | Source |
| ------------------- | ------ |
| [[⚙️ Productivity]] |  [Neues Jahr - mehr Geld! Interview mit Celine Münstermann (#248) - Berufsoptimierer - Erfolg in Bewerbung und Karriere](https://pca.st/episode/739ccbc8-e401-4dd2-a757-58a1693de7e0)       |

---

> Colleagues shouldn't be the reason to not take a new challenge. 

Having nice colleagues is a common excuse to not look for a new job. This can lead to you not taking that new opportunity. 

But you can find wonderful colleagues at your new job as well. Chances are, you will. Don't stop yourself. 

- - -
## Links
1. [[Mansons Law of Avoidance]]
