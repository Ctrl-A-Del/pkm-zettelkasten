---
sr-due: 2023-07-28
sr-interval: 28
sr-ease: 275
date created: Monday, July 10th 2023, 8:41:04 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[🖥️ Tech]]
> links:  [[Reducing the business risk with cloud computing]]
> source: [What is Cloud Computing? | Amazon Web Services - YouTube](https://www.youtube.com/watch?v=mxT233EdY5c)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Cloud computing uses scalable cloud infrastructures instead of your own servers.

Cloud computing describes the use of huge infrastructures like AWS or Azure to utilize on-demand computing resources, instead of deploying on your own servers. The goal is to create a scalable environment for technology, that doesn't require huge investments upfront. Instead, you usually have a pay-as-you-go model, that you can dynamically adjust to your needs. You can deploy the software globally in multiple locations, so you don't need to worry about latency.

Cloud computing can include infrastructure, platforms or services. Depending on that, it can mean renting computing power, platform tools to deploy software, or subscribing to a specific, already existing service.