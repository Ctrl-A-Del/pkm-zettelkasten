---
sr-due: 2023-08-27
sr-interval: 47
sr-ease: 276
date created: Monday, July 10th 2023, 8:40:55 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🖥️  Tech]]
> links:  
> - [[Vue]]
> - [[Riot]]
> - [[Svelte]]
> - [[Next.js]]
> - [[Gatsby.js]]
> - [[Single Page Application]]
> source: [Svelte vs. React vs. Vue | Best SPA Framework for 2022 - YouTube](https://www.youtube.com/watch?v=LSiV8A9Zqzw)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> React is a client-side JS library to build interactive web frontends and [[Single Page Application|Single Page Applications]].


React is a client-side JS library to build interactive web frontends and [[Single Page Application|Single Page Applications]].


It uses JSX to implement HTML code into your JS code. That JSX code is compiled to vanilla JS for the browser.


| Pros                                           | Cons                                                                                                                                                                        |
| ---------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| easy to manipulate state via its state         | doesn't do much for you, so you need several other libraries (e.g. routing)                                                                                                                |
| very light-weight without the other libraries | react uses a poll mechanism for updates, requiring it to check on every component if it needs to be re-rendered once the state changes, causing many unnecessary re-renders |
| huge community and libraries                   | often needs other libraries like Redux, react-router etc.                                                                                                                   |
| funded by Meta                                 | using JSX it is dependent on babel for compiling                                                                                                                            |
|                                                | loads a ton of JS for client-side rendering (without stuff like next.js)                                                                                                                                                                            |

