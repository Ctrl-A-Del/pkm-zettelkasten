---
sr-due: 2023-08-12
sr-interval: 36
sr-ease: 270
date created: Monday, July 10th 2023, 8:39:43 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[🧠 Psychology]]
> links:  
> - [[How to be less certain]] 
> - [[Uncertainty is better than certainty]] 
> - [[Responsibility of choice]] 
> - [[Don't Blame, Take Responsibility - Responsiblity-fault fallacy]]
> - [[I am wrong much more often than I am right - and why this is a good thing]]
>
> source: [[The subtle art of not giving a fuck]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> If it comes down to the question if you screwed up, or someone else did, it is most of the time you. 

Always remember: if it comes down to the question if you screwed up, or someone else did, it is most of the time you. 

Try to find the mistakes in your doing, not the one of others. You can't change them anyway. You might complain, but if they change is still up to them.

Instead, focus on the question of how you can improve. What was your mistake in this?

It's not you against the world. Very often, it is you against yourself. 

Blaming others is a common excuse to avoid admitting your own mistakes. But ultimately, it is yourself who you can change. So if something went wrong, how can **you** improve it?
