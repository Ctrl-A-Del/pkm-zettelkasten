---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
---
| Topic            | Source |
| ---------------- | ------ |
| [[👪 Parenting]] | [[Unconditional Parenting]]        |

---

Praise and positive reinforcement are common tactics to encourage good behavior.

But it actually doesn't work - at least not in the long run.

It usually works in the short term while you are offering a reward. But it reduces the intrinsic motivation to comply after the reward is given. So, if you pay a person to help another person, they will probably do it. But studies show that the chance that they'll do it without a reward will decrease. They'll learn that being helpful should pay off and there should not be any intrinsic motivation.
- - -
## Links
1. [[Love withdrawal is as bad as physical dominance to your child]]
