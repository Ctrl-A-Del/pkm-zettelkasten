---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
---

| Topic     | Source                                                                                          |
| --------- | ----------------------------------------------------------------------------------------------- |
| [[React]] | [All 29 Next.js Mistakes Beginners Make - YouTube](https://www.youtube.com/watch?v=5QP0mvrJkiY) |

---

> Adding the "use client" directive too high in the component tree can inadvertently convert server components into client components, losing the benefits of server-side rendering and data fetching.

Always consider when to create a client component. Do this only for the lowest necessary component, to avoid unnecessary client-side rendering. When using `"use client"` inside a component, all other modules imported into it, including child components, are considered part of the client bundle!

Because of this, you might need to do some refactoring when you realize that you need a client component. Create a new client component with only the client-side code instead of just writing `"use client"` at the top and be done with it.


- - -
## Links
1. [[Make client component explicit instead of implicit in Next.js]]
