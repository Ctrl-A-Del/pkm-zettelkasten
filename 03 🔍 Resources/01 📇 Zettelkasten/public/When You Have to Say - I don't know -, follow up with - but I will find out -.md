topic: [[🏡 Life]]
links: [[Getting better each day - the compounding effect of the one percent rule]]
source: [[The Pragmatic Programmer]]
tags: #permanent-note #published 

---


When you find yourself saying “*I don't know”*, follow up with “*But I’ll find out*”.