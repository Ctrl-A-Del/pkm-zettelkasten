---
date created: Monday, July 10th 2023, 8:41:03 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
tags: [permanent-note, published]
sr-due: 2023-08-13
sr-interval: 26
sr-ease: 290
---
| Topic                                 | Source |
| ------------------------------------- | ------ |
| [[⚙️ Productivity]] [[🧠 Psychology]] |  [How I Routinely Study With a Full Time Job when I'm TIRED - YouTube](https://www.youtube.com/watch?v=ifZWcPXDyFc)       |

---

> "Work expands so as to fill the time available for its completion."

There is a simple reason we procrastinate. *Because we can*. If you have a deadline next week, you're unlikely to finish your task today, but if the deadline is tomorrow, you will certainly finish it today *somehow*. 

Parkinson's law describes, that **"work expands so as to fill the time available for its completion"**.

- - -
## Links
1. [[Do Something Principle]]
