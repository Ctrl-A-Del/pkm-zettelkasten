---
date created: Monday, July 10th 2023, 8:40:16 am
date modified: Friday, July 21st 2023, 9:15:04 am
tags: [permanent-note, published]
sr-due: 2023-09-16
sr-interval: 57
sr-ease: 290
---
| Topic             | Source                              |
| ----------------- | ----------------------------------- |
| [[🦒 TypeScript]] | TypeScript course by the native web |

---

When casting an object into an interface that has fewer properties than the object, this actually works, but you lose the property to access those properties, even though they are still saved inside the object.

```ts
interface Point2D {
  x: number;
  y: number;
}

interface Point3D {
  x: number;
  y: number;
  z: number;
}

const firstPoint: Point3D = {
  x: 42,
  y: 23,
  z: 17
};

const secondPoint: Point2D = firstPoint; // this is valid

console.log(secondPoint);
// output: { x: 12, y: 23, z: 17 }

```

To access those properties, you have to cast the object into the other interface again:

```ts
console.log((secondPoint as Point3D).z);
```

- - -
## Links
1. [[Interfaces in TypeScript]] 
