---
sr-due: 2023-08-22
sr-interval: 41
sr-ease: 275
date created: Monday, July 10th 2023, 8:40:40 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  [[The Habit Loop]] 
> source: [Stop Bad Habits & Addictions - 100% Money-Back Guarantee - YouTube](https://www.youtube.com/watch?v=-IuEXr5WUp0&list=PLogFTIm1gC5LYSwnFJJF1XgCzI9iH2IVb&index=3)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> If you struggle to get rid of your bad habits, make them impossible

The most difficult part of getting rid of a bad habit is the beginning. The longer you don't break the chain of abstinence, the easier it gets. So, if you struggle to get rid of your bad habits, make them impossible. If you really want to get rid of some bad behavior, invest some time into systems that make it impossible for you. Block the usage of the app, lock away your phone, put your money into a kitchen safe to avoid buying new cigarettes. Go the extra mile to make it impossible for yourself, and you will see success.