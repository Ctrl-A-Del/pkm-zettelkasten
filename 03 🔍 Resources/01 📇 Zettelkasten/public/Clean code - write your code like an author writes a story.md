---
sr-due: 2023-08-14
sr-interval: 34
sr-ease: 292
date created: Monday, July 10th 2023, 8:39:54 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

**> [!INFO]- 
> topic: [[🧹 Clean Code]]
> links:  [[SOLID Clean code in React]]
> source: https://www.udemy.com/course/writing-clean-code/learn/lecture/23111090#overview
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Clean code is important to reduce the cognitive load to understand that code.

As a developer, you spend much time reading and understanding code. Because of this, it is essential to write code, that is easy to understand and not code that *just works*. Consider yourself an author, writing a story.

Your code should be readable, meaningful and precise, to reduce the cognitive load required by the reader to understand it. To achieve this, you should try to follow best practices and patterns, so that others can recognize these standards.

The common pain points for clean code are:
- naming
	- variables
	- functions
	- classes
- structure & code formatting
	- bad comments
- functions
	- length
	- parameters
- deep nesting in conditionals 
- missing error handling
- classes & data structures
	- missing distinction
	- bloated clases