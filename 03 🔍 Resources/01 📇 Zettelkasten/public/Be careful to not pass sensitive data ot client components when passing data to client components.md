---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
---

| Topic     | Source                                                             |
| --------- | ----------------------------------------------------------------------------------------------- |
| [[React]] | [All 29 Next.js Mistakes Beginners Make - YouTube](https://www.youtube.com/watch?v=5QP0mvrJkiY) |

---

When passing data from a server component to a client component, makes sure to not expose any data to the client.

A common example is user data and passwords.

- - -
## Links
1. [[Make client component explicit instead of implicit in Next.js]]
