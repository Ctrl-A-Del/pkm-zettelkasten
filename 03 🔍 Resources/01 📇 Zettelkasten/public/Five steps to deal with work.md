---
date created: Monday, July 10th 2023, 8:41:12 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
tags: [permanent-note, published]
sr-due: 2023-10-12
sr-interval: 78
sr-ease: 297
---
| Topic               | Source |
| ------------------- | ------ |
| [[⚙️ Productivity]] | [[Getting things done]]       |

---

> To stay on top of all your tasks, you need to capture, clarify, organize, reflect and engage with your tasks.

A lot of work accumulates quickly in your life. Keeping a system to stay on top of all tasks and projects is crucial. 

The following five steps can help you as a guide to stay on top of your tasks:

1. Capture 
    - collect all data, tasks, emails and ideas in one place
2. Clarify
    - Decide what to do about an item 
3. Organize
    - organize the items in useful lists
4. Reflect
    - review your lists regularly
5. Engage 
    - Do your next tasks

If you properly use these techniques to create a workflow and always have an idea about your tasks, this can help you to get much more productive. Even if it takes some time to engage with these five steps, it can pay off exponentially to always know the next task at hand.

- - -
## Links
1. [[Writing every idea and every task down helps to declutter you mind]]
2. [[Creating a workflow with Todos]]