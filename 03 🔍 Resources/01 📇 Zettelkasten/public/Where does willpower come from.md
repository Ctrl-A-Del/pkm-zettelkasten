---
date created: Monday, July 10th 2023, 8:39:52 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---
> [!INFO]- 
> topic: [[🧠 Psychology]]
> links:  [[Willpower is depletable]]
> source: [[Willpower]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> No glucose, no willpower.

Glucose is the fuel for our brain, and it is also the fuel for our willpower.
Your body transfers all food to glucose. While sweet food have glucose faster available, it doesn't regenerate as much willpower, as a healthy meal. Food with a low glycemic index works better than food with a high glycemic index. Fruit and vegetables are better than fast food.

Craving for food to regain willpower can be a struggle, if you actually want to use your willpower to lose weight. This is a specific case to watch out for healthy food.

In addition to glucose, enough sleep also helps your willpower because it gives you a bigger tank in the first place.