---
sr-due: 2023-08-23
sr-interval: 42
sr-ease: 290
date created: Monday, July 10th 2023, 8:41:03 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🧠 Psychology]]
> links:  
> - [[What is a Zettelkasten]]
> - [[Understanding what you read]]
>
> source: [Illusions of Competence - Chunking | Coursera](https://www.coursera.org/learn/learning-how-to-learn/lecture/BuFzf/illusions-of-competence)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

If you look at something you are eager to learn by reading the solution to a problem or following along a video tutorial, it is easy to follow along and understand the process of thinking. It makes you feel like you understand the subject and that you learned something. When in fact, you would struggle to explain the main points to another person. This is the *illusion of competence*.

**==To really learn something, you have to practice==**. You need to test yourself. You should consider writing your notes about the subject in your own words, or you should take some example tasks and test your understanding. 