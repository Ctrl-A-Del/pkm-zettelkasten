---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags: [permanent-note, to-publish]
sr-due: 2023-07-28
sr-interval: 4
sr-ease: 286
---
| Topic | Source |
| ----- | ------ |
| [[📚 Personal Knowledge Management]]      | [Stop Procrastinating With Note-Taking Apps Like Obsidian, Roam, Logseq - YouTube](https://www.youtube.com/watch?v=baKCC2uTbRc&t)       |

---

> Focus on creating, not note-taking.

Personal Knowledge Management can take a lot of time. And it can result in you not focusing on your real goals. Even if it makes you feel productive, it can still limit the time you really put into your work.

To avoid this, you have to accept, that PKM is not the work, that is bringing you forward. It matters what you do with that knowledge. This means that you shouldn’t take notes for the sake of taking notes, and that you probably should limit the time you are spending with it. 

What matters is what you do with those notes. Focus on doing, not on planning.

Perfectionism in your system can be a goal in your leisure time, but it is no work and won’t help you to do the work. Stick with good enough, instead of perfect.

- - -
## Links
1. [[Switch from consuming to creating]]
2. [[Choose your priority or someone else will]]
3. [[Defining goals is difficult]]
4. [[Do Something Principle]]
5. [[Engaging on your next action]]
6. [[Pareto principle - the 80-20 rule]]
