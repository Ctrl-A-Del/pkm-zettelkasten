---
date created: Monday, July 10th 2023, 8:39:27 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
tags: [permanent-note, published]
sr-due: 2023-09-19
sr-interval: 49
sr-ease: 291
---
| Topic       | Source |
| ----------- | ------ |
| [[🖥️ Tech]] | [[Cracking the Coding Interview]]       |

---

> Add runtimes, if your code does things after each other and multiply them, if you have nested loops. 

It is easy to confuse if you have to add or multiply a value to your Big O notation. However, there is a simple rule:

- If your algorithm is in the form of *"do this, then when you're done, do that"* then add the runtimes. 
- If your algorithm is in the form of *"do this for each time you do that"* then multiply the runtimes

Imagine the difference between two consecutive for loops and two nested for loops. 

- - -
## Links
1. [[Big O Notation]]
