---
sr-due: 2023-09-15
sr-interval: 64
sr-ease: 290
date created: Monday, July 10th 2023, 8:41:10 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---

> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  
> - [[Diffuse mode vs focus mode]] 
> - [[The goal of Pomodoro]]
>
> source: [Coursera - Learning how to learn](https://www.coursera.org/learn/learning-how-to-learn/home/module/1)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> To learn efficiently, you need to take a break.

The problem is, that **we don't establish new connections in our brain when in focused mode**, because we prefer our known patterns. So to learn something new, we need to get into the diffuse mode, to allow new connections.

To achieve this, our mind needs some space. So **to achieve these new connections in our brain, we need to relax**. Taking a walk, doing housework, something like that.

A great technique to achieve this is Pomodoro. This helps us to focus on something new and let it sink in during the break, when our mind is allowed to wander.