---
date created: Tuesday, June 21st 2022, 10:52:53 am
date modified: Sunday, July 16th 2023, 9:10:39 pm
tags:
  - permanent-note
  - published
---
| Topic            | Source |
| ---------------- | ------ |
| [[👪 Parenting]] | [[Unconditional Parenting]]       |

---

> You should care more about your child's well-being than about their accomplishments. 

Focusing on your child's accomplishments usually means rewarding success and often even love-withdrawal if success remains absent.

This will result in more stress and a worse relationship. It can even have a negative impact on the self-esteem of your child, going as far as depression. 

Focus on your child's well-being and your relationship. Children's natural curiosity flourishes in the absence of pressure.

- - -
## Links
1. [[Positive Reinforcements lowers the intrinsic motivation]]
2. [[Irregular Praise is worse than too much Praise]]
