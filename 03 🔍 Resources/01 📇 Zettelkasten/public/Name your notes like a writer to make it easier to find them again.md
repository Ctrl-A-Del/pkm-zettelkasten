---
sr-due: 2023-08-11
sr-interval: 42
sr-ease: 292
date created: Monday, July 10th 2023, 8:41:52 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  
> - [[What is a Zettelkasten]] 
> - [[The problem of starting with a blank page]] 
> - [[Why I write my notes in English]] 
> - [[Keeping an open mind when writing your notes means that you do not have to agree with everything in it]]
> source: [[06 🗃️ Archiv/03 🗄️ References/How to take smart notes]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p>  

---

> Don't name your notes by the most fitting keyword. Choose a title that lets you stumble upon your notes again accidentally, by choosing words that do not occur in your note and that relate to your projects and interests. 

There are two ways to design the titles and keywords related to your notes:

1. Like and archivist 
2. Like a writer 

The archivist asks what keywords fit most to the content and design a headline. 

The writer wonders, how he wants to stumble upon the note again accidentally and maybe considers an intriguing question as a title, that is answered in the note. 

Most people tend to go the archivists way, but for a [[What is a Zettelkasten|Zettelkasten]], the writers way is much better. 

In your Zettelkasten, you want to find your notes via links and while creating those links. So, you should choose your title with your projects and interests in mind, to increase the chances of finding them. If you simply choose the most fitting keywords, this keyword might be something that you rarely look for. 

As a rule of thumb, try to find a title consisting of words that do not occur in the note itself. 
