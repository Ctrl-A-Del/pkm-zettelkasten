---
date created: Monday, July 10th 2023, 8:41:38 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
tags:
  - permanent-note
  - published
sr-due: 2023-08-06
sr-interval: 4
sr-ease: 276
---
| Topic        | Source |
| ------------ | ------ |
| [[🖥️ Tech]] | [What is a REST API? - YouTube](https://www.youtube.com/watch?v=lsMQRaeKNDk)       |

---

> A REST API creates a simple and standardized approach to communicate data between client and server.

Rest stands for **Re**presental **S**tate **T**ransfer. Its focus is to create a simple and standardized approach to communicate data between client and server.
The API should be stateless and give your server a high performance due to caching.

A REST API uses the [[CRUD actions]].

- - -
## Links
1. [[CRUD actions]]


