---
date created: Monday, July 10th 2023, 8:40:38 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---
> [!INFO]- 
> topic: [[⚔️ Role-playing]]
> links:  [[Prepare to throw your work away]]
> source: [[Return of the Lazy Dungeon Master]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> Focus on designing the important characters that have a connection to your story. Give them the personality of some character in a book or show. 

When creating NPCs for your game, focus on the characters that play a role in your story. Improvise the other ones. To quickly characterize those people, tie them to a character you know. Think about a person from a book or TV show. When doing this, consider switching genders and go into other genres. 