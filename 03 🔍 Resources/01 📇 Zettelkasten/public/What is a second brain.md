---
sr-due: 2023-09-23
sr-interval: 75
sr-ease: 290
date created: Tuesday, June 20th 2023, 11:06:53 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[⚙️ Productivity]]
> links:  
> - [[Writing every idea and every task down helps to declutter you mind]]
> - [[What types of notes belong to a Zettelkasten]]
> - [[Capture Everything in an Inbox]]
> - [[Getting better each day - the compounding effect of the one percent rule]]
>
> source: [The Second Brain - A Life-Changing Productivity System - YouTube](https://www.youtube.com/watch?v=OP3dA2GcAh8)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> A second brain is a place to store your learnings and ideas for the future and make them accessible for you later. This helps you to tackle information overload and stop forgetting new ideas.

Have you ever read something in a book or heard some great advice in a podcast and thought *"This is spectacular, I need to remember that and apply that to my life"*, just to forget about it?

> Our brain is good at having ideas, not at remembering them
> --- David Allen

If we consume media, we are often confronted with [[Information Overload]]. To stop forgetting all these great things we hear and read about, we need to create a [[Personal Knowledge Management]] system. We can do this by building a second brain. The goal is to take notes on those things. Writing them down in one specific place can help you to later find that note again. 

The only problem remaining is, how to find that note again later. To accomplish this, the [[What is a Zettelkasten|Zettelkasten]] methodology can be a great way. The idea is to store the info on a small note that only holds one information and link it with other notes, that have some connection to the concept. These connections can help you to stumble upon those notes in the future.

Over time, your ideas can pile up into a *second brain*, that stores all your learnings and ideas in one place and shows you how they all fit together. Modern tools like [Obsidian](https://obsidian.md) can help you to even visualize those connections as a graph.

However, your second brain is not limited to being one app. You can use several applications for different use cases. You can have a read-it-later app, a task manager, a notes app etc. As long as you have a system you can use and trust and that helps you find your information.

But no matter how many apps you are using, building a second brain is a [[Getting better each day - the compounding effect of the one percent rule|habit]], not just an app. The goal is to consistently capture, organize and distill information to make it easier to find it later for your future self to engage with it.
