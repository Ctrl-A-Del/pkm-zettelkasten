---
date created: Monday, July 10th 2023, 8:40:32 am
date modified: Sunday, July 16th 2023, 9:11:05 pm
---
> [!INFO]- 
> topic: [[🏡 Life]]
> links: 
> source: [Die 8 wichtigsten Tipps für Gasgriller [Anfänger & Fortgeschrittene] - YouTube](https://www.youtube.com/watch?v=rA8bp7YI6y4)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p>

---

> Burn out your new grill by heating it to the maximum for 30 minutes.

When you have a new gas grill, it is important to burn production residues. You have to do this every time, you have a new barbecue grate, or after you clean the grill with chemicals.
To accomplish this, turn the heat up to the maximum and let the power on for 20 to 30 minutes.
