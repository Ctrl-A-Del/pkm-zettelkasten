---
sr-due: 2023-07-25
sr-interval: 28
sr-ease: 270
date created: Monday, July 10th 2023, 8:39:28 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---

> [!INFO]- 
> topic: [[🖥️ Tech]]
> links:  
> - [[Basic definition of Cloud Computing]]
> - [[Don't panic, if your service breaks over night]]
>
> source: [Definition and Essential Characteristics of Cloud Computing - Overview of Cloud Computing | Coursera](https://www.coursera.org/learn/introduction-to-cloud/lecture/2ev7i/definition-and-essential-characteristics-of-cloud-computing)
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> SaaS describes centrally hosted software that is licensed via a subscription.

Software as a Service (SaaS) is a business model that focuses on licensing software and applications that are centrally hosted. Usually, those services have a subscription model.