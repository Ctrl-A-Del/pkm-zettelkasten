---
sr-due: 2023-09-27
sr-interval: 77
sr-ease: 310
date created: Monday, July 10th 2023, 8:41:29 am
date modified: Sunday, July 16th 2023, 9:11:03 pm
---

> [!INFO]- 
> topic: [[⚔️ Role-playing]]
> links:  [[Players want their characters to do awesome things]]
> source: [[Return of the Lazy Dungeon Master]]
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> These truths can help you to be more relaxed as a dungeon master. 

- Everyone comes to the game to have fun (and not to laugh at you)
- Players don't care as much as you think
> - [[Players want their characters to do awesome things]]
- Be a fan of the characters 
- Players love breaking the game 
- Let the game evolve 
