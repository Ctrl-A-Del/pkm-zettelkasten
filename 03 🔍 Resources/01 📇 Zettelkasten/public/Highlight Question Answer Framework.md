---
date created: Monday, July 10th 2023, 8:40:51 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
---
> [!INFO]- 
> topic: [[📚 Personal Knowledge Management]]
> links:  
> - [[What is a second brain]]
> - [[What is a Zettelkasten]]
> - [[Use the Question Evidence Conclusion framework to find out what information to write into your permanent notes]]
>
> source: [Take Better Notes When Reading Non-Fiction With HQ&A](https://words.jamoe.org/highlight-question-and-answer) 
> tags: #permanent-note #published 
> <br/><p class="d-none" style="text-align: right;">`Last Modified:` <i>`=dateformat(this.file.mtime, "MMM. dd, yyyy - HH:mm")`</i></p> 

---

> **Highlight** = The original context for your reference.
> **Question** = An application of the new information to form an insight.
> **Answer** = A compressed version of the highlight's insight expressed in your own words

The Highlight, Question and Answer (HQ&A) framework is a concept to take better notes when reading non fiction books.

It tells you, that you should excerpt a highlight of a text that stands out to you by formulating a question. This requires some additional thinking and engages you to think about why this highlight is important. Then you answer this question in your own words.

