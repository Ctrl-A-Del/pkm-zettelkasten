---
date created: Monday, July 10th 2023, 8:41:14 am
date modified: Sunday, July 16th 2023, 9:11:04 pm
tags: [permanent-note, published]
sr-due: 2023-08-16
sr-interval: 22
sr-ease: 294
---
| Topic | Source |
| ----- | ------ |
| [[👪 Parenting]]      | [Warum Kindern vorlesen so wichtig ist - Das gewünschteste Wunschkind](https://pca.st/episode/b19e0cdf-77d7-49c6-aaf8-49340e7ef7f7)        |
 
---

> It's normal if your child doesn't sit calmly next to you when you read a story for it.

Reading can be very exciting for a child. However, often they jump around and climb on top of the bed and do other stuff while we are reading. This might seem disrespectful, but it isn't. Usually, your child is still listening. This means, your child is absorbed in the story and is excited. It feels the suspension and needs to counter this tension by movement. You will see that your child actually listened cautiously. 

- - -
## Links
1. [[Reading a story for you child is not about reading and not about the story]]

